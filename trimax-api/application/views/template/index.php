<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trimax App</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <style>
            /*
     * Base structure
     */

            /* Move down content because we have a fixed navbar that is 50px tall */
            body {
                padding-top: 50px;
            }


            /*
             * Global add-ons
             */

            .sub-header {
                padding-bottom: 10px;
                border-bottom: 1px solid #eee;
            }

            /*
             * Top navigation
             * Hide default border to remove 1px line.
             */
            .navbar-fixed-top {
                border: 0;
            }

            /*
             * Sidebar
             */

            /* Hide for mobile, show later */
            .sidebar {
                display: none;
            }
            @media (min-width: 768px) {
                .sidebar {
                    position: fixed;
                    top: 51px;
                    bottom: 0;
                    left: 0;
                    z-index: 1000;
                    display: block;
                    padding: 20px;
                    overflow-x: hidden;
                    overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
                    background-color: #f5f5f5;
                    border-right: 1px solid #eee;
                }
            }

            /* Sidebar navigation */
            .nav-sidebar {
                margin-right: -21px; /* 20px padding + 1px border */
                margin-bottom: 20px;
                margin-left: -20px;
            }
            .nav-sidebar > li > a {
                padding-right: 20px;
                padding-left: 20px;
            }
            .nav-sidebar > .active > a,
            .nav-sidebar > .active > a:hover,
            .nav-sidebar > .active > a:focus {
                color: #fff;
                background-color: #428bca;
            }


            /*
             * Main content
             */

            .main {
                padding: 20px;
            }
            @media (min-width: 768px) {
                .main {
                    padding-right: 40px;
                    padding-left: 40px;
                }
            }
            .main .page-header {
                margin-top: 0;
            }

            .page-header {
                color:#337ab7;
                margin-bottom: 0;
                margin-top: 20px;
            }
            .hideme {
                display: none;
            }
            .padding-no-top {
                padding-top: 0;
            }
        </style>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="<?= (!empty($_SESSION['login_from']) && $_SESSION['login_from'] == 'appbackend') ? 'padding-no-top' : null ?>">
        <nav class="navbar navbar-inverse navbar-fixed-top <?= (!empty($_SESSION['login_from']) && $_SESSION['login_from'] == 'appbackend') ? 'hideme' : null ?>">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/users" style="color: cadetblue">Trimax</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <!-- include('admin.menu') -->
                    <ul class="nav navbar-nav ">
                        <li class="<?= $view_name == 'pages/users' ? 'active' : '' ?>"><a href="<?= base_url('users') ?>" style="color: antiquewhite"><span class="glyphicon glyphicon-user"></span> Users</a></li>
                        <li class="<?= $view_name == 'pages/inactive_users' ? 'active' : '' ?>"><a href="<?= base_url('users/inactive') ?>" style="color: antiquewhite"><span class="glyphicon glyphicon-list-alt"></span> Registration Request</a></li>
                        <li class="<?= $view_name == 'pages/conact_persons/index' ? 'active' : '' ?>"><a href="<?= base_url('app/contactpersons') ?>" style="color: antiquewhite"><span class="glyphicon glyphicon-list-alt"></span> Contact Persons</a></li>
                        <li class="<?= $view_name == 'pages/welcome_text' ? 'active' : '' ?>"><a href="<?= base_url('app/welcome_text') ?>" style="color: antiquewhite"><span class="glyphicon glyphicon-list-alt"></span> Welcome Text</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?= base_url('login/logout') ?>" style="color: antiquewhite"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <?php $this->load->view($view_name); ?>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" ></script>
        <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js" ></script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable();
            });
            function readURL(input,selector) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $(selector).attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgInp").change(function () {
                readURL(this,'#blah');
            });
            $("#imgInp2").change(function () {
                readURL(this,'#blah2');
            });
        </script>
    </body>
</html>