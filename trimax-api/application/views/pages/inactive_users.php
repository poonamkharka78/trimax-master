<div class="page-header">
    <h1 class="">Registration Requests</h1>
</div>
<br>
<?php if (!empty($_SESSION['success'])): ?>
    <div class="alert alert-success" role="alert"><?= $_SESSION['success'] ?></div>
    <?php unset($_SESSION['success']) ?>
<?php endif; ?>
<div class="table-responsive">
    <table class="table table-striped table-bordered" id="example">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Company</th>
                <th>Remark</th>
                <th>Register Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($result as $value): ?>
                <tr>
                    <td><?= $value['first_name'] ?></td>
                    <td><?= $value['last_name'] ?></td>
                    <td><a href="mailto:<?= $value['email'] ?>"><?= $value['email'] ?></a></td>
                    <td><?= $value['phone'] ?></td>
                    <td><?= $value['company_name'] ?></td>
                    <td><?= $value['remark'] ?></td>
                    <td><?= $value['created_at']; ?></td>
                    <td>
                        <a class="btn btn-sm btn-success" href="<?= base_url('users/activate_user/' . $value['id']) ?>">Approve</a>
                        <a href="<?= base_url('users/delete/' . $value['id'].'/rejected') ?>" class="btn btn-sm btn-danger" onclick="return confirm('Reject Request?')">Reject</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>