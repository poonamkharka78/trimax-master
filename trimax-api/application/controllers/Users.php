<?php

class Users extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->setLogin();
    }

    function index()
    {
        $this->setLogin();
        if (empty($_SESSION['login'])) {
            redirect('login');
        }
        $this->db->order_by('first_name', 'DESC');
        $data = $this->db->where(['status' => 'active'])->get('users')->result_array();
        $this->load->view('template/index', ['result' => $data, 'view_name' => 'pages/users']);
    }

    function inactive()
    {
        if (empty($_SESSION['login'])) {
            redirect('login');
        }
        $this->db->order_by('first_name', 'DESC');
        $data = $this->db->where(['status' => 'inactive'])->get('users')->result_array();
        $this->load->view('template/index', ['result' => $data, 'view_name' => 'pages/inactive_users']);
    }

    public function activate_user($id)
    {
        $data = $this->db->where(['id' => $id])->get('users')->row_array();
        $this->db->update('users', array('status' => 'active'), array('id' => $id));
        $this->send_mail($data);
        $_SESSION['success'] = 'User Approved';
        redirect('users/inactive');
    }

    public function delete($id,$type = 'deleteuser') {
        $data = $this->db->where(['id' => $id])->get('users')->row_array();
        $this->db->delete('users', array('id' => $id));
        if($type == 'deleteuser'){
            $_SESSION['success'] = 'User Deleted';
            $this->send_mail($data, false);
        }else {
            $_SESSION['success'] = 'Request Rejected';
            $this->send_mail($data, true);
        }        
        redirect($_SERVER['HTTP_REFERER']);
    }

    private function send_mail($data, $isReject = false) {
        $config['protocol'] = 'mail';
        $config['mailtype'] = 'html';

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('app@trimaxmowers.co.uk', 'Trimax');
        $this->email->to($data['email']);
//        $this->email->to('maulik.php5626@gmail.com');
        $this->email->subject('Trimax Account');
        if ($isReject) {
            $this->email->message($this->load->view('emails/reject', $data, true));
        } else {
            $this->email->message($this->load->view('emails/activate', $data, true));
        }

        $this->email->send();
    }

    private function setLogin()
    {
       if (!empty($_GET['LOGIN_KEY']) && $_GET['LOGIN_KEY'] == LOGIN_KEY) {
            $_SESSION['login'] = true;
            $_SESSION['login_from'] = 'appbackend';
        }
    }

}
