<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {
        if (!empty($_SESSION['login'])) {
            redirect('users');
        }
        $this->load->view('login');
    }

    public function check() {
        if ($_POST['username'] == "appadmin" && $_POST['password'] == "d#2@Cr4#") {
            $_SESSION['login'] = true;
            redirect('users');
        } else {
            redirect('login');
        }
    }

    public function logout() {
        unset($_SESSION['login']);
        redirect('login');
    }

    public function activate_user() {
        if (empty($_GET['user'])) {
            show_404();
        } else {
            $id = base64_decode($_GET['user']);
            $data = $this->db->where(['id' => $id])->get('users')->row_array();
            if (!empty($data)) {
                $this->db->update('users', array('status' => 'active'), array('id' => $id));
                $this->send_mail($data);
                $this->load->view('pages/activate_user', $data);
            } else {
                show_404();
            }
        }
    }

    private function send_mail($data) {
        $config['protocol'] = 'mail';
        $config['mailtype'] = 'html';

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('app@trimaxmowers.co.uk', 'Trimax');
        $this->email->to($data['email']);
//        $this->email->to('maulik.php5626@gmail.com');
        $this->email->subject('Trimax Account');
        $this->email->message($this->load->view('emails/activate', $data, true));
        $this->email->send();
    }

}
