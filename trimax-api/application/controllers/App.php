<?php

class App extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->setLogin();
        if (empty($_SESSION['login'])) {
            redirect('login');
        }
    }

    function contactpersons()
    {
        $this->db->order_by('id', 'DESC');
        $data = $this->db->get('contact_persons')->result_array();
        $this->load->view('template/index', ['result' => $data, 'view_name' => 'pages/conact_persons/index']);
    }

    function contact_add()
    {
        if ($this->input->method() == 'post') {
            $insertData = [
                'name'        => $this->input->post('name', true),
                'description' => $this->input->post('description', true),
                'phone'       => $this->input->post('phone', true),
                'email'       => $this->input->post('email', true),
            ];
            if (!empty($_FILES['userfile']['name'])) {
                $result = upload('userfile', 'image', 'contact_person');
                if ($result['status'] == true) {
                    $insertData['image'] = $_POST['image'];
                }
            }
            table::insert('contact_persons', $insertData);
            $_SESSION['success'] = 'Added Contact Person';
            redirect('app/contactpersons');
        } else {
            $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash());
            $data['csrf'] = $csrf;
            $data['view_name'] = 'pages/conact_persons/add';
            $this->load->view('template/index', $data);
        }
    }

    function contact_edit($id)
    {
        if ($this->input->method() == 'post') {
            $updateData = [
                'name'        => $this->input->post('name', true),
                'description' => $this->input->post('description', true),
                'phone'       => $this->input->post('phone', true),
                'email'       => $this->input->post('email', true),
            ];
            if (!empty($_FILES['userfile']['name'])) {
                $result = upload('userfile', 'image', 'contact_person');
                if ($result['status'] == true) {
                    $updateData['image'] = $_POST['image'];
                }
            }
            table::update('contact_persons', $updateData, ['id' => $id]);
            $_SESSION['success'] = 'Update Contact Person';
            redirect('app/contactpersons');
        } else {
            $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash());
            $data['csrf'] = $csrf;
            $data['view_name'] = 'pages/conact_persons/edit';
            $data['result'] = table::contact_persons(['id' => $id])[0];
            $this->load->view('template/index', $data);
        }
    }

    function contact_remove_image($id)
    {
        $result = table::contact_persons(['id' => $id])[0];
        @unlink(FCPATH . $result['image']);
        table::update('contact_persons', ['image' => null], ['id' => $id]);
        $_SESSION['success'] = 'Contact Person Image Remove';
        redirect('app/contactpersons');
    }

    function contact_delete($id)
    {
        $result = table::contact_persons(['id' => $id])[0];
        @unlink(FCPATH . $result['image']);
        table::delete('contact_persons', ['id' => $id]);
        $_SESSION['success'] = 'Deleted Contact Person';
        redirect('app/contactpersons');
    }

    function welcome_text()
    {

        $data = ['view_name' => 'pages/welcome_text'];
        if ($this->input->method() == 'post') {
            $welcome_text = $this->input->post('welcome_text', true);
            $this->db->update('options', ['value' => $welcome_text], array('id' => 1));
            $_SESSION['success'] = 'Welcome Text Update';
            $data['result'] = $this->db->where(['name' => 'welcome_text'])->get('options')->row_array();
            $_SESSION['success'] = 'Update Contact Person';
            redirect('app/welcome_text');
        } else {
            $data['result'] = $this->db->where(['name' => 'welcome_text'])->get('options')->row_array();
            $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash());
            $data['csrf'] = $csrf;
        }
        $this->load->view('template/index', $data);
    }

    private function setLogin()
    {
        if (!empty($_GET['LOGIN_KEY']) && $_GET['LOGIN_KEY'] == LOGIN_KEY) {
            $_SESSION['login'] = true;
            $_SESSION['login_from'] = 'appbackend';
        }
    }

}
