-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 04, 2019 at 06:45 AM
-- Server version: 10.2.26-MariaDB-log-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trimaxmo_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_persons`
--

CREATE TABLE `contact_persons` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `image` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_persons`
--

INSERT INTO `contact_persons` (`id`, `name`, `description`, `phone`, `email`, `image`) VALUES
(3, 'Trimax UK', '40 Lower Farm Road, Moulton Park, Northampton, NN3 6XF, UK', '01933652235', 'info@trimaxmowers.co.uk', NULL),
(4, 'Rikki Bown', 'Southern Area Sales Manager', '07776196573', 'rikki.bown@trimaxmowers.co.uk', 'uploads/contact_person/3fbbe437d509cbf39579f4366190c171.jpg'),
(5, 'Chris York', 'Central Area Sales Manager', '0776523090', 'chris.york@trimaxmowers.co.uk', 'uploads/contact_person/df165e1b8005b3c05138cf5e86d70ea7.JPG'),
(6, 'Steve Blanchard', 'Northern Area Sales Manager', '07904977837', 'Steve.blanchard@trimaxmowers.co.uk', 'uploads/contact_person/93809d1ae9eef54d120afd318d03764b.png'),
(7, 'Parts', '', '01933 652246', 'parts@trimaxmowers.co.uk', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `customer_email` varchar(100) DEFAULT NULL,
  `customer_business` varchar(100) DEFAULT NULL,
  `customer_position` varchar(100) DEFAULT NULL,
  `customer_business_address` varchar(100) DEFAULT NULL,
  `customer_phone` varchar(100) DEFAULT NULL,
  `serial_number` varchar(100) DEFAULT NULL,
  `info_date` varchar(100) DEFAULT NULL,
  `industry` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `name`, `value`) VALUES
(1, 'welcome_text', 'Thanks for downloading the Trimax App!\r\n\r\nFollowing feedback from reps like yourself, we have created this framework to help give you greater knowledge of our products, some fresh sales and demonstration resources and an easy way to share the Trimax range with your future customers.\r\n\r\nAs always with Trimax, we want this experience to be inclusive, informal and a two way conversation to help us improve. If you have any feedback about what you\'d like, just drop us a note at info@trimaxmowers.co.uk.\r\n\r\nSo from all of us here at Trimax, thanks for being a vital part of the family! Get stuck in, explore the app and if you have any questions along the way feel free to shout out!\r\n\r\nCheers!');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `password` varchar(256) NOT NULL,
  `profile_img` varchar(256) DEFAULT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'inactive',
  `remark` varchar(1000) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `password`, `profile_img`, `company_name`, `status`, `remark`, `created_at`) VALUES
(11, 'Karl', 'Stevenson', 'Karl.stevenson@trimaxmowers.com', '+64226755786', '3e4b4b31d07e06fec19c8322a03d2d42b1c03d8f', NULL, NULL, 'active', NULL, '2018-08-24 20:35:10'),
(12, 'Karl', 'Stevenson', 'karl.stevenson@gmail.com', '+64226755786', '3e4b4b31d07e06fec19c8322a03d2d42b1c03d8f', '03f04cae3f480.jpg', 'Trimax', 'active', NULL, '2018-09-28 07:43:35'),
(23, 'Caroline', 'shaw', 'Caroline.shaw@trimaxmowers.co.uk', '07951689126', '39995332ebfb1cbade77ed3c5ab36cb1c1a924c2', '', NULL, 'active', NULL, '2019-01-09 13:29:25'),
(25, 'Chris ', 'York ', 'Chris.york@trimaxmowers.co.uk', '07765230909', 'e02ada59f62729451fec3c122920974f8e43e90d', '', NULL, 'active', NULL, '2019-03-22 13:18:07'),
(26, 'Larissa', 'Hodgson', 'Larissa.hodgson@gmail.com', '0277752224', 'adf2f21dbfa1bf89dc1332b5046bdc03a946005d', '', NULL, 'active', NULL, '2019-03-22 17:57:19'),
(27, 'Rikki', 'Bown', 'Rikki.bown@trimaxmowers.com', '7776196573', '0d111bf71da07fce945fa866d3eb5d80e91c830e', 'uploads/profile/f4f4556b95d4c19ed071ebbcfe1696b2.jpeg', 'Trimax Mowing Systems', 'active', NULL, '2019-03-23 11:39:38'),
(28, 'Larissa', 'Hodgson', 'larissa.hodgson@trimaxmowers.com', '64277752224', 'adf2f21dbfa1bf89dc1332b5046bdc03a946005d', 'uploads/profile/b7105c8ec13178f8b767d80c0b625615.jpeg', 'Trimax', 'active', NULL, '2019-04-04 01:57:09'),
(52, 'kevin', 'patel', 'bhutkevin1@gmail.com', '1234567891', 'c009dba768cff69943f73c0cf4c0ed5a97f5be8f', '', NULL, 'active', 'Test by kk', '2019-05-09 09:45:18'),
(53, 'Vicky', 'Testing', 'v.yussuff@btinternet.com', '07875171094', '6f76017a1082925bf4592628ae2ac94dd13d27e0', '', NULL, 'active', 'Testing app', '2019-05-09 11:03:57'),
(54, 'Lee', 'kirk ', 'lee.kirk@masonskings.co.uk', '07975612708', 'c26b0fc63502b0c4285c983165ff6017202d0d83', '', NULL, 'active', '', '2019-05-15 15:42:44'),
(55, 'Lee', 'Hatton', 'lee.hatton@listerwilder.co.uk', '07388995723', 'c97b1d8cec088c039863762e38d7930320b69514', '', NULL, 'active', '', '2019-05-15 17:31:47'),
(56, 'Ben ', 'Turner', 'benbturner@BenBurgess.co.uk', '07787544341', '2d47a1b746fd99beac7250a333afc96821aa007c', '', NULL, 'active', '', '2019-05-16 07:07:31'),
(57, 'Phill ', 'Hughes', 'phill.hughes@listerwilder.co.uk', '07500844259', '4997c0deb2edef576a2a57ca2593ca980bd1f0af', '', NULL, 'active', '', '2019-05-20 12:18:10'),
(58, 'Cameron ', 'Burnett ', 'cameron.burnett@trimaxmowers.com', '07792747409', 'e2402b9f51969037bae5f341499823df320ad02b', '', NULL, 'active', '', '2019-05-29 10:16:30'),
(59, 'Natassja', 'Beavis', 'natassja.beavis@trimaxmowers.co.uk', '07802756027', '3bc1f8959ce51b60e7d781a8d8fe95ce7a287843', '', NULL, 'active', '', '2019-05-29 10:29:49'),
(60, 'Vicky', 'Yussuff', 'victoria.yussuff@trimaxmowers.co.uk', '999', '1aaa2ab3cc7a661c6278a2b40a80f6181e2e3218', '', NULL, 'active', 'Long ting', '2019-06-04 13:22:26'),
(61, 'Michael', 'Sievwright', 'Michael.sievwright@trimaxmowers.com', '0275717774', '1ad34795735cfe01132a48acde6a7011b209d9a8', '', NULL, 'active', '', '2019-06-05 08:22:07'),
(62, 'Steve', 'Blanchard', 'steve.blanchard@trimaxmowers.co.uk', '07904977837', '467545b571cfbafadf45a356de6ba261861c062d', '', NULL, 'active', '', '2019-06-05 15:35:57'),
(63, 'Jason', 'Low', 'jason.low@trimaxmowers.com', '+64221310251', '9c10d0b8bf5ddfd3c2cb2355e3e7df7829a37753', '', NULL, 'active', '', '2019-06-10 16:37:16'),
(64, 'Bob', 'Sievwright ', 'bob@trimaxmowers.com', '021619934', '4f9f7c1fae641f5c252b9810fc41b1c410024864', '', NULL, 'active', '', '2019-06-18 00:09:36'),
(69, 'mahipal', 'sinh', 'mahipal.thakor5626@gmail.com', '1234567890', '7c4a8d09ca3762af61e59520943dc26494f8941b', '', NULL, 'active', 'Test', '2019-06-19 04:50:29'),
(74, 'milan', 'na', 'maulik.php5626@gmail.com', NULL, 'f10e2821bbbea527ea02200352313bc059445190', NULL, NULL, 'active', NULL, '2019-06-19 11:34:43'),
(77, 'Simon', 'Fountain', 'simon.fountain@fgadamsonandson.co.uk', '7377361951', '3ed319646df5da75531b3a279bb29f84cef207f0', 'uploads/profile/7f20b6e93df7b598ce22c50ecf422c3f.jpeg', 'F G Adamson & son', 'active', '', '2019-06-25 12:49:20'),
(78, 'Bruce', 'Davie', 'bdavie@tuckwellgroup.com', '07973958303', 'f97433f8256fe0e622c1afec8013491ed5da55f2', '', NULL, 'active', 'P Tuckwell Ltd, Ardleigh Hall, Colchester\n', '2019-06-25 12:59:31'),
(79, 'Richard', 'Ifould', 'Richard.ifould@listerwilder.co.uk', '07899062148', '9b4cc9f9584b80146daab72609cc04b8818b6fac', '', NULL, 'active', '', '2019-06-25 13:01:21'),
(80, 'Andrew ', 'Bowey ', 'andrewbowey@huntforest.com', '07852137710', '59cfceb0a38a9944ce274550ea4c8a49063a06fc', '', NULL, 'active', '', '2019-06-25 13:01:29'),
(81, 'Alan', 'ford', 'aford@tuckwellgroup.com', '07889081234', 'f0a3421ce045f63c751ebc087267d37e477883e6', '', NULL, 'active', '', '2019-06-25 13:15:54'),
(82, 'Paul ', 'Bell ', 'Paul.bell@listerwilder.co.uk', '07879414561', '5933b7a1205c288d2befca72d8bef79de0a5893d', '', NULL, 'active', '', '2019-06-25 13:38:01'),
(83, 'Richard ', 'Ainslie ', 'Richard.ainslie@tallisamosgroup.co.uk', '07970203538', '34d727f5d65b00d1a0eef8624fc51e6133187550', '', NULL, 'active', '', '2019-06-25 14:01:32'),
(84, 'Alan', 'White', 'aw@hendersongm.co.uk', '7793589665', '8a49304ac3af5ab067901b345ac6abe22b603406', 'uploads/profile/9434cc70a507c3a8cab2e511aa0b6e68.jpeg', 'Henderson Grass Machinery ltd', 'active', 'Henderson Grass Machinery Ltd', '2019-06-25 14:28:12'),
(85, 'Colin', 'McIntyre ', 'Colin.mcintyre@tallisamosgroup.co.uk', '01285740115', '3652778f14e3329f7197e9409d5cdd7ae79faa6d', '', NULL, 'active', '', '2019-06-25 15:25:24'),
(86, 'Gordon ', 'smith', 'Gordon.smith@agricar.co.uk', '07977280708', '8bd645adaee49ea49db67179b8525ebc48ef1f25', '', NULL, 'active', '', '2019-06-25 16:26:29'),
(87, 'David', 'Fisher', 'davidfisher@georgebrowns.co.uk', '07767117387', '95baa5443d4b8dec44348fc5d2275d2a1a4ae72b', '', NULL, 'active', '', '2019-06-25 16:35:46'),
(88, 'Luke ', 'Davis ', 'luke@georgebrowns.co.uk', '07548926044', '47456cc868f5920bb1e358c1d5c14c320c529acf', '', NULL, 'active', '', '2019-06-25 17:10:01'),
(89, 'Gareth', 'Beck', 'gbeck@franksutton.co.uk', '07778754765', '1c8c7c4317c0338ba5854b75591bfa0f2c42cef6', '', NULL, 'active', '', '2019-06-25 19:27:50'),
(90, 'Stuart ', 'Dickson', 'stuartd@brysontractors.co.uk', '07779200489', 'd2d4ea4c1c798db036f07a807ed517aca76586a0', '', NULL, 'active', '', '2019-06-26 09:05:23'),
(91, 'Mr', 'Flynn', 'lf@hcuk.co', '7788975034', 'f63ad91d3c8580341512f8baa66aaa967592d8c3', 'uploads/profile/13a76c7150d9dc030ebe6b4e92ef5ce3.jpeg', 'Henton and Chattell', 'active', '', '2019-06-26 16:09:13'),
(92, 'Andrew ', 'Hamilton ', 'andrewh@brysontractors.co.uk', '07793025233', '1be4631c860447f82f9aec5d7fb6c9942ea50219', '', NULL, 'active', '', '2019-06-27 14:50:21'),
(94, 'Gez ', 'Appleyard', 'Gez.appleyard@burdens.com', '07779292179', '57d0510fa2fe1f21b39a2d44c3a96225a3305495', '', NULL, 'active', '', '2019-06-28 05:54:18'),
(95, 'Andy', 'O’Neill', 'andy.oneill@fgadamsonandson.co.uk', '07951046745', 'e3b490d9347c45de1459c93c14109e9e606d40ea', 'uploads/profile/25119dc60d2b27556c8c23fa783fe8f3.jpeg', 'F G Adamson & Son', 'active', '', '2019-06-28 10:23:45'),
(97, 'Nancy', 'Bryson', 'nancy@brysontractors.co.uk', '7801472513', '23545479758f97565a3f5b8a8ef6407841a52471', 'uploads/profile/74ae465a70a4905eef58b25715e9dbfe.jpeg', 'Bryson Tractors Ltd', 'active', '', '2019-06-28 15:18:59'),
(98, 'Tony', 'Jenkins', 'Tony.jenkins@fgadamsonandson.co.uk', '07889031811', 'd479ae8db1407ab5889c98540cb3a41cc6c90490', 'uploads/profile/1c21d5c992199b5124622a4f90e8b977.jpeg', 'F.G. Adamson & Son', 'active', '', '2019-07-01 07:34:01'),
(99, 'James', 'Robson ', 'jr@hcuk.co', '07771844801', '5b3f56a2ec2db227874f9e423872b8e69eae32da', '', NULL, 'active', '', '2019-07-02 09:34:36'),
(100, 'John ', 'Hamilton ', 'johnh@brysontractors.co.uk', '07751563407', 'cfcbd3d17f0b400c5c1ac4d2da83034d096151dc', '', NULL, 'active', '', '2019-07-08 15:09:50'),
(102, 'Neil', 'Peachey', 'npeachey@tuckwellgroup.com', '447889081237', 'fefb460fd5c76730fdf3b51f7b24414c72721254', 'uploads/profile/d30965c80512f3c0d0634426cac8894d.jpeg', 'P. Tuckwell Ltd', 'active', '', '2019-07-10 20:21:38'),
(105, 'David', 'Guy', 'David.guy@burdenbros.co.uk', '07703807578', '4d2745f3ef5c5d990f506a2a0eefa98bdd331336', '', NULL, 'active', 'WC', '2019-08-06 18:58:17'),
(106, 'Nick', 'Bragger', 'nick.bragger@burdenbros.co.uk', '07703807604', 'fedb609c3ac9ed65c434193547be3f25415a07e9', '', NULL, 'active', '', '2019-08-07 16:50:29'),
(107, 'jim', 'read', 'jimread@Huntforest.com', '07525060514', '61e8144867f7b5bd79762c9dd6fa1abc33801390', '', NULL, 'active', 'I am the new golf salesman for the isle of wight and need access to the Trimax price lists', '2019-08-14 09:51:05'),
(108, 'Matthew ', 'Hodge', 'Matthew.h@vincenttractors.co.uk', '01726860332', 'f4745549dfb1787e53686564e065679d81942f02', '', NULL, 'active', '', '2019-08-16 19:53:14'),
(109, 'sakshi', 'goyal', 'sakshigoyal200@gmail.com', '9521459695', '153fa238cec90e5a24b85a79109f91ebe68ca481', '', NULL, 'active', 'I am konstant infosolutions and we are development company. We need to check the app. ', '2019-08-21 07:59:18'),
(110, 'Sandeep', 'Rawat', 'Sandeep.rawat@evontech.com', '8279952898', 'ad9280c159074d9ec90899b584f520606e83d10e', 'uploads/profile/ab25b8474b0c27a3d58d73af9ef7fd70.jpeg', 'evontech', 'active', '', '2019-08-21 12:38:55'),
(112, 'Suryanshu', 'Sahay', 'suryanshu.sahay@xicom.biz', '+14155203551', '5cec175b165e3d5e62c9e13ce848ef6feac81bff', '', NULL, 'active', 'We are an App and Web development agency. ', '2019-08-22 05:13:40'),
(113, 'Parth', 'Patel', 'parth.patelba@hyperlinkinfosystem.com', '123456789', '7c4a8d09ca3762af61e59520943dc26494f8941b', '', NULL, 'active', 'We are all devlopment company ', '2019-08-22 05:22:29'),
(114, 'Ben', 'Kirkby', 'ben.kirkby@fgadamsonandson.co.uk', '07812975544', '3b5144a72bc181bf0c9f71d199a08bac72e72b7b', '', NULL, 'active', '', '2019-08-27 11:10:56'),
(115, 'Rohit', 'Mehra', 'Rohit.anil.mehra@gmail.com', '9636468580', 'b8307b34ddcd3b1a2d3494d8b3230f9f9f08204f', '', NULL, 'active', 'IT company Nine Hertz ', '2019-08-27 14:25:23'),
(116, 'kieran', 'shillito', 'kieran.shillito@trimaxmowers.com', '07496169808', '865ba3b921e59582512c167792c8d8f808b2577f', '', NULL, 'active', '', '2019-09-02 13:34:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_persons`
--
ALTER TABLE `contact_persons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_persons`
--
ALTER TABLE `contact_persons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
