User's Details

Name: {user_name}
Email: {user_email}

Customer's Details

Name: {customer_name}
Email: {customer_email}
Business name: {customer_business}
Position in Organization: {customer_position}
Business address: {customer_business_address}
Phone Number: {customer_phone}
Serial Number: {serial_number}
Date: {date}
Industry: {industry}