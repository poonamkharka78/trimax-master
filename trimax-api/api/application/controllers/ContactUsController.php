<?php

/**
 * @property User $user
 *
 * @author Milan Sanathara
 */
class ContactUsController extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contactus');
    }
    
    /**
     * contact us post data
     */
    public function contactUs()
    {
        try {
             $_POST  = json_decode(file_get_contents('php://input'), true);
            $this->form_validation->set_rules('first_name', 'first name', 'required');
            $this->form_validation->set_rules('email', 'email name', 'required|valid_email');
            $this->form_validation->set_rules('message', 'message', 'required');
             if ($this->form_validation->run() == TRUE) {
            $response     = $this->contactus->saveContact($_POST);
            if ($response == 1) {
                $config['protocol'] = 'mail';
                $config['mailtype'] = 'html';
                $this->load->library('email');
                $this->email->initialize($config);
                
                // Admin Mail
                //$this->email->to('divya.vishwakarma@mail.vinove.com');
                $this->email->to('info@trimaxmowers.co.uk');
                $this->email->from('app@trimaxmowers.co.uk', 'Trimax');
                $this->email->subject('Contact Us');
                $data = elements(array(
                    'first_name',
                    'last_name',
                    'email',
                    'country_code',
                    'message'
                ), $_POST);
                $this->email->message($this->load->view('emails/new_register_info', $data, true));
                $this->email->send();
                echo formatJson(['code'=>200, 'message' => 'Data is inserted successfully']);
                
            } else {
                echo formatJson(['code'=>400, 'message' => 'Error has occured']);
            }
            
        } else {
             echo formatJson(['code'=>401, 'message' => 'Please Enter Required Field']);
        }
         }catch (Exception $e) {
            log_message('error: ', $e->getMessage());
            return;
        }
        
        
    }

    /**
    * validation
    */
    private function validation()
    {
        $this->form_validation->set_rules('first_name', 'first name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
    }
    
}