<?php

/**
 * @property User $user
 *
 * @author Milan Sanathara
 */
class FeedbackController extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('feedback');
    }
    
    /**
     * feedback form data
     */
    public function userFeedBack()
    {
        try {
            $_POST    = json_decode(file_get_contents('php://input'), true);
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('feedback', 'Feedback', 'required');
            if ($this->form_validation->run() == TRUE) {
            $response = $this->feedback->saveFeedback($_POST);
            if ($response == 1) {
                echo formatJson(['code'=>200, 'message' => 'feedback is inserted successfully']);
            } else {
                 echo formatJson(['code'=>400, 'message' => 'Error has occured']);
            }
        } else {
            echo formatJson(['code'=>401, 'message' => 'Please Enter Required Field']);
        }
        }catch (Exception $e) {
            log_message('error: ', $e->getMessage());
            return;
        }
    }
    
    /**
     * validation
     */
    private function validation()
    {
        $this->form_validation->set_rules('feedback', 'feedback', 'required');
    }
    
}