<?php

class AppController extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function contactpersons($id) {
        $result = table::contact_persons(array('country_code' => $id));   
        $finalResult = [];
        foreach ($result as $row) {
            if (!empty($row['image'])) {
                $row['image'] = base_url($row['image']);
            }
            $finalResult[] = $row;
        }
        
        
        echo formatJson($finalResult);
    }

    function welcometext() {
        $result = $this->db->where(['name' => 'welcome_text'])->get('options')->row_array();
        echo formatJson($result['value']);
    }

}
