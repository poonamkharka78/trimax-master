<?php

/**
 * @property User $user
 *
 * @author Milan Sanathara
 */
class UserController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user');
    }

    public function profile($id) {
        $user = $this->user->getUser($id);
        echo formatJson($user);
    }

    public function profileUpdate($id) {
        $this->validation();
        if ($this->form_validation->run()) {
            if (!empty($_FILES['userfile']['name'])) {
                $result = upload('userfile', 'profile_img', 'profile');
                if ($result['status'] == FALSE) {
                    $errors = $this->upload->error_msg;
                    echo formatJsonError(implode("\n", $errors));
                    return;
                }
            }
            $this->user->profileUpdate($id, $_POST);
            $user = $this->user->getUser($id);
            echo formatJson($user);
        } else {
            $errors = $this->form_validation->error_array();
            echo formatJsonError(implode("\n", $errors));
        }
    }

    private function validation() {
        $this->form_validation->set_rules('first_name', 'first name', 'required');
        $this->form_validation->set_rules('email', 'email', 'valid_email|is_unique[users.email]');
    }

}
