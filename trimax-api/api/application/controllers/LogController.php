<?php

class LogController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('parser');
    }

    public function log() {
        $data = elements(
                array('company_name', 'product', 'date', 'customer_name', 'customer_email', 'user_name', 'user_email',
                    'customer_position', 'customer_businees_address', 'customer_phone', 'serial_number'
                )
                , $_POST);
        $msg = $this->parser->parse('emails/log', $data, TRUE);
//        $result = mail("bhutkevin@gmail.com", "Log", $msg);
        $result = mail("info@trimaxmowers.co.uk", "Log", $msg);
        echo formatJson($result);
    }

    public function warranty() {
        $data = elements(array('user_name', 'user_email', 'customer_name', 'customer_email',
            'customer_business', 'customer_position', 'customer_business_address', 'customer_phone',
            'serial_number', 'date', 'industry'), $_POST);
        $msg = $this->parser->parse('emails/warranty', $data, TRUE);
        $result = mail("info@trimaxmowers.co.uk", "Warranty Registration", $msg); //info@trimaxmowers.co.uk
        echo formatJson($result);
    }

}
