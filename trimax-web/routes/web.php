<?php
// cache clear
Route::get(
    'clear-cache',
    function () {
        Artisan::call('route:clear');
        Artisan::call('clear-compiled');
        Artisan::call('config:cache');
        Artisan::call('cache:clear');
        return "Cache is cleared";
    }
);

/*
 * Auth
 */
Route::any('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logout');
Route::middleware(['LoginCheck'])->group(function () {
    /*
     * About
     */
    Route::get('/aboutus', 'AboutUSController@index')->name('aboutus');
    Route::post('/aboutus/update', 'AboutUSController@update')->name('aboutus-update');
    /*
     * Products
     */
    Route::get('/', 'ProductController@index')->name('products');
    Route::any('/product/add', 'ProductController@add')->name('product-add');
    Route::any('/product/update/{product}', 'ProductController@update')->name('product-update');
    Route::get('/product/delete/{product}', 'ProductController@delete')->name('product-delete');
    Route::post('/getproduct', 'ProductController@getProduct')->name('getproduct');
    /*
     * Features
     */
    Route::get('/features', 'FeaturesController@index')->name('features');
    Route::post('/getfeature', 'FeaturesController@getFeature')->name('getfeature');
    Route::any('/feature/add', 'FeaturesController@add')->name('feature-add');
    Route::any('/feature/update/{feature}', 'FeaturesController@update')->name('feature-update');
    Route::get('/feature/delete/{feature}', 'FeaturesController@delete')->name('feature-delete');
    /*
     * General
     */
    Route::get('/generals', 'GeneralController@index')->name('generals');
    Route::any('/general/add', 'GeneralController@add')->name('general-add');
    Route::any('/general/update/{general}', 'GeneralController@update')->name('general-update');
    Route::get('/general/delete/{general}', 'GeneralController@delete')->name('general-delete');
    Route::post('/getgenerals', 'GeneralController@getGeneral')->name('getgenerals');
    /*
     * General Images
     */
    Route::get('/general/images', 'GeneralController@images')->name('general-images');
    Route::any('/general/image/add', 'GeneralController@image_add')->name('general-image-add');
    Route::get('/general/image/delete/{id}', 'GeneralController@image_delete')->name('general-image-delete');
    Route::post('/generalimg', 'GeneralController@getGeneralimages')->name('generalimg');
    /*
     * Library
     */
    Route::get('/library/product/{product_id?}/{isVideo?}', 'LibraryController@index')->name('library');
    Route::any('/library/add', 'LibraryController@add')->name('library-add');
    Route::any('/library/update/{library}', 'LibraryController@update')->name('library-update');
    Route::get('/library/delete/{library}', 'LibraryController@delete')->name('library-delete');
    Route::post('/getlibraryimage/{product_id?}/{isVideo?}', 'LibraryController@getLibrary')->name('getlibrary');
    /*
     * Options
     */
    Route::get('/options', 'OptionController@index')->name('options');
    Route::any('/option/add', 'OptionController@add')->name('option-add');
    Route::any('/option/update/{option}', 'OptionController@update')->name('option-update');
    Route::get('/option/delete/{option}', 'OptionController@delete')->name('option-delete');
    Route::post('/getoptions', 'OptionController@getOption')->name('getoptions');
    /*
     * Parts
     */
    Route::get('/parts', 'PartsController@index')->name('parts');
    Route::any('/part/add', 'PartsController@add')->name('part-add');
    Route::any('/part/update/{part}', 'PartsController@update')->name('part-update');
    Route::get('/part/delete/{part}', 'PartsController@delete')->name('part-delete');
    Route::post('/getparts', 'PartsController@getParts')->name('getparts');
    /*
     * Parts Details
     */
    Route::post('/getupdatedata', 'PartsDetailsController@getupdateData')->name('getparts');
    Route::post('/addpartdetail', 'PartsDetailsController@getpartData')->name('getparts');
    Route::post('/getpartsdetail', 'PartsDetailsController@getPartsDetail')->name('getparts');
    Route::get('/parts/details', 'PartsDetailsController@index')->name('partsdetails');
    Route::any('/parts/details/add', 'PartsDetailsController@add')->name('partsdetail-add');
    Route::any('/parts/details/update/{part}', 'PartsDetailsController@update')->name('partsdetail-update');
    Route::get('/parts/details/delete/{part}', 'PartsDetailsController@delete')->name('partsdetail-delete');
    Route::get('myform/ajax/{id}','PartsDetailsController@myformAjax');
    //Route::post('/addcity', 'PartsDetailsController@adddataCountry')->name('addcity');
    /*
     * Parts Order
     */
    Route::post('/productsequence', 'PartsOrderController@getPartsSequence')->name('productsequence');
    Route::get('/parts/orders', 'PartsOrderController@index')->name('partsorders');
    Route::any('/parts/order/add', 'PartsOrderController@add')->name('partsorders-add');
    Route::any('/parts/order/update/{part}', 'PartsOrderController@update')->name('partsorders-update');
    Route::get('/parts/order/delete/{part}', 'PartsOrderController@delete')->name('partsorders-delete');
    /*
     * Parts Price
     */
    Route::any('/parts/price/add', 'PartsPriceController@add')->name('partsprice-add');
    Route::post('/addcity', 'PartsPriceController@getdataCountry')->name('addcity');
    Route::post('/getprice', 'PartsPriceController@getPrice')->name('getprice');
    Route::get('/parts/price', 'PartsPriceController@index')->name('partsprice');
    //Route::any('/parts/price/add', 'PartsPriceController@add')->name('partsprice-add');
    Route::any('/parts/price/update/{part}', 'PartsPriceController@update')->name('partsprice-update');
    Route::get('/parts/price/delete/{part}', 'PartsPriceController@delete')->name('partsprice-delete');
    /*
     * Specification
     */
    Route::post('/getspecification', 'SpecificationController@getSpecification')->name('getspecification');
    Route::get('/specifications', 'SpecificationController@index')->name('specifications');
    Route::any('/specification/add', 'SpecificationController@add')->name('specification-add');
    Route::any('/specification/update/{part}', 'SpecificationController@update')->name('specification-update');
    Route::get('/specification/delete/{part}', 'SpecificationController@delete')->name('specification-delete');
    /*
     * Specification Details
     */
    Route::post('/getdetail', 'SpecificationDetailsController@getDetail')->name('getdetail');
    Route::get('/specifications/details', 'SpecificationDetailsController@index')->name('specifications-details');
    Route::any('/specification/details/add', 'SpecificationDetailsController@add')->name('specification-details-add');
    Route::any('/specification/details/update/{part}', 'SpecificationDetailsController@update')->name('specification-details-update');
    Route::get('/specification/details/delete/{part}', 'SpecificationDetailsController@delete')->name('specification-details-delete');
    /*
     * Demo Tips
     */
    Route::post('/getdemo', 'DemoTipsController@getDemo')->name('getdemo');
    Route::get('/tips/demo', 'DemoTipsController@index')->name('tips-demo');
    Route::any('/tips/demo/add', 'DemoTipsController@add')->name('tips-demo-add');
    Route::any('/tips/demo/update/{part}', 'DemoTipsController@update')->name('tips-demo-update');
    Route::get('/tips/demo/delete/{part}', 'DemoTipsController@delete')->name('tips-demo-delete');
    Route::post('/removeVideos/{demoId}', 'DemoTipsController@deleteImage');
    /*
     * Sales Tips
     */
    Route::post('/getsales', 'SalesTipsController@getSales')->name('getsales');
    Route::get('/tips/sales', 'SalesTipsController@index')->name('tips-sales');
    Route::any('/tips/sales/add', 'SalesTipsController@add')->name('tips-sales-add');
    Route::any('/tips/sales/update/{part}', 'SalesTipsController@update')->name('tips-sales-update');
    Route::get('/tips/sales/delete/{part}', 'SalesTipsController@delete')->name('tips-sales-delete');
    /*
     *  API Trimax Admin Panel
     */
    Route::get('/apiadmin/users', 'APIAdminController@users')->name('apiadmin-users');
    Route::get('/apiadmin/registrationRequests', 'APIAdminController@registrationRequests')->name('apiadmin-registrationRequests');
    Route::get('/apiadmin/contactpersons', 'APIAdminController@contactpersons')->name('apiadmin-contactpersons');
    Route::get('/apiadmin/welcometext', 'APIAdminController@welcomeText')->name('apiadmin-welcomeText');
    /*
     *  Services
     */
    Route::get('/service/product/{product_id?}/{isVideo?}', 'ServiceController@index')->name('service');
    Route::any('/service/add', 'ServiceController@add')->name('service-add');
    Route::any('/service/update/{library}', 'ServiceController@update')->name('service-update');
    Route::get('/service/delete/{library}', 'ServiceController@delete')->name('service-delete');
    Route::post('/getserviceimage', 'ServiceController@getService')->name('getserviceimage');

    /*
    * manage info
    */
    Route::get('/option', 'ProductOptionController@index')->name('option');
    Route::any('/options/add', 'ProductOptionController@add')->name('options-add');
    Route::get('/button/delete/{general}/{text?}', 'ProductOptionController@delete')->name('button-delete');
    Route::post('/getproductbutton', 'ProductOptionController@filtercountryButton')->name('getproductbutton');
    Route::any('/option/updateproduct/{general}', 'ProductOptionController@update')->name('options-update');

    /*
    * Contact us
    */
    Route::get('/contact-us', 'ContactUsController@index')->name('contact-us');
    Route::any('/contactus/add', 'ContactUsController@add')->name('contactus-add');
    Route::any('/contactus/update/{part}', 'ContactUsController@update')->name('contactus-update');
    Route::get('/contactus/delete/{part}', 'ContactUsController@delete')->name('contactus-delete');
    Route::post('/getcontact', 'ContactUsController@getContact')->name('getparts');
});
