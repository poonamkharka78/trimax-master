<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::post('version', function (Request $request) {
    if ($request->api_token != 'QPOWFaaKQB5v4rJ9Jcip') {
        return response()->json([], 401);
    }
    $version = DB::table('Version')->value('number');
    return response()->json(['version' => $version, 
    'download_url' => url('Trimax.db'),'original_download_url' => url('Trimax2.db'),]);
});
Route::Post('/change-log', 'Api\ChangesLogController@getChangeLog');

// Route::Post('/get-data', 'Api\ChangesLogController@trackDatabase');
