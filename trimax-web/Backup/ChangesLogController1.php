<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Schema;
use App\Models\ChangesLogs;
use App\Models\Features;
use App\Models\Product;


class ChangesLogController extends Controller
{
    public function getChangeLog(Request $request)
    {
        
        $version = DB::table('Version')->value('number');
        $db_ext = DB::connection('sqlite12'); 
        if ($request->time) { 
            $time = str_replace("%20"," ",$request->time);
            $result = ChangesLogs::select('*')->where('time', '>', $time)
            ->where('cid',$request->cid)->get();

            $result1 = $db_ext->table('ChangesLogs')->select('*')->where('cid',$request->cid)
            ->where('time', '>=', $time)->get();

            $last_row =  DB::table('ChangesLogs')->latest('time')->where('cid',$request->cid)->first();
            $images = [];
            $video = [];
            foreach ($result as $key => $value) {
                $images[] = $value->image;
                if ($value->video != '') {
                    $video[] = $value->video;
                }
            }
           
            $responseArray = [
                'images' => $images,
                'videos' => $video,
                'version' => $version,
                'time' => $last_row->time ?? '',
                'download_url' => url('Trimax.db'),
                'orignal_download_url' => url('Trimax2.db'),
                'country_url' => url('country.db'),
                'db_name' => 'country.db',
            ];
        } else {
            
            $this->trackDatabase($request->cid);   
           
            $fetaure = $db_ext->table('features')->select('icon')->where('cid',$request->cid)->get();
            $general_img = $db_ext->table('GeneralImages')->select('img_name')->where('cid',$request->cid)->get();
            $general =  $db_ext->table('Generals')->select('images', 'video_url')->where('cid',$request->cid)->get();
            $library = $db_ext->table('Library')->select('img_name', 'video_url')->where('cid',$request->cid)->get();
            $option = $db_ext->table('Options')->select('icon')->where('cid',$request->cid)->get();
            $product = $db_ext->table('Products')->select('image')->where('cid',$request->cid)->get();
            $service = $db_ext->table('Service')->select('img_name','video_url')->where('cid',$request->cid)->get();
            $sales_video = $db_ext->table('SalesDemoVideo')->select('thumbnail_img','video_url')->where('cid',$request->cid)->get();
            $last_row =  $db_ext->table('ChangesLogs')->latest('time')->first();
           
            //  $last_row =  DB::table('ChangesLogs')->latest('time')->first();
            // $fetaure = DB::table('features')->select('icon')->where('cid',$request->cid)->get();
            // $general_img = DB::table('GeneralImages')->select('img_name')->where('cid',$request->cid)->get();
            // $general = DB::table('Generals')->select('images', 'video_url')->where('cid',$request->cid)->get();
            // $library = DB::table('Library')->select('img_name', 'video_url')->where('cid',$request->cid)->get();
            // $option = DB::table('Options')->select('icon')->where('cid',$request->cid)->get();
            // $product = DB::table('Products')->select('image')->where('cid',$request->cid)->get();
            $all_images = [];
            $video1 = [];
            foreach ($fetaure as $key => $value) {
                $all_images[] = $value->icon;
            }
            foreach ($general_img as $key => $img) {
                $all_images[] = $img->img_name;
            }
            foreach ($general as $key => $value) {
                $all_images[] = $value->images;
                $video1[] = $value->video_url;
            }
            foreach ($library as $key => $value) {
                $all_images[] = $value->img_name;
                if ($value->video_url != '') {
                    $video1[] = $value->video_url;
                }
            }
            foreach ($sales_video as $key => $value) {
                $all_images[] = $value->thumbnail_img;
                if ($value->video_url != '') {
                    $video1[] = $value->video_url;
                }
            }
           
            foreach ($option as $key => $value) {
                $all_images[] = $value->icon;
            }
            foreach ($product as $key => $value) {
                $all_images[] = $value->image;
            }
               
            $responseArray = [
                'version' => $version,
                'images' => $all_images,
                'videos' => $video1,
                'time' => $last_row->time ?? '',
                'download_url' => url('Trimax.db'),
                'orignal_download_url' => url('Trimax2.db'),
                'country_url' => url('country.db'),
                'db_name' => 'country.db',
            ];
        }
        $this->responseArray = [
            'code' => 200,
            'message' => 'Changes Fetch Successfully',
            'data' => $responseArray,
        ];
        return response()->json($this->responseArray, $this->responseArray['code']);
    }

    public function trackDatabase($cid)
    {
        
        // $version = "";
        // if ($request->version > $version) {
            $db_ext = DB::connection('sqlite12'); 
            $products = $db_ext->table('Products')->truncate();
            $demo_tips = $db_ext->table('DemoTips')->truncate();
            $features = $db_ext->table('Features')->truncate();
            $general = $db_ext->table('Generals')->truncate();
            $general = $db_ext->table('GeneralImages')->truncate();
            $library = $db_ext->table('Library')->truncate();
            $option = $db_ext->table('Options')->truncate();
            $part_detail = $db_ext->table('PartDetails')->truncate();
            $part_price = $db_ext->table('PartPrice')->truncate();
            $part_master = $db_ext->table('PartMaster')->truncate();
            $part_order = $db_ext->table('PartOrder')->truncate();
            $sales_tips = $db_ext->table('SalesTips')->truncate();
            $service = $db_ext->table('Service')->truncate();
            $change_log = $db_ext->table('ChangesLogs')->truncate();
            $sales_log = $db_ext->table('SalesDemoVideo')->truncate();
            $specification = $db_ext->table('SpecificationMaster')->truncate();
            $spec_details = $db_ext->table('SpecifiactionDetails')->truncate();
            

            $product = DB::table('Products')->where('country_id', $cid)->get();
            foreach ($product as $pro) {
                $input['id'] = $pro->id;
                $input['name'] = $pro->name;
                $input['image'] = $pro->image;
                $input['category'] = $pro->category;
                $input['img_name'] = $pro->img_name;
                $input['country_id'] = $pro->country_id;
                $products = $db_ext->table('Products')->insert($input);
            }
            $product_id = $db_ext->table('Products')->select('id')->get();
            print_r($product_id);die;
           
            $change = DB::table('ChangesLogs')->where('cid', $cid)->get();
            foreach ($change as $changes) {
                $changes1['image'] = $changes->image;
                $changes1['video'] = $changes->video;
                $changes1['time'] = $changes->time;
                $changes1['cid'] = $changes->cid;
             $changes_logs = $db_ext->table('ChangesLogs')->insert($changes1);
            
            }
            $sales_video = DB::table('SalesDemoVideo')->where('cid', $cid)->whereIn('pid',$product_id)->get();
            foreach ($sales_video as $sales_videos) {
                $sales_videos1['pid'] = $sales_videos->pid;
                $sales_videos1['cid'] = $sales_videos->cid;

                $sales_videos1['video_url'] = $sales_videos->video_url;
                $sales_videos1['video_name'] = $sales_videos->video_name;
                $sales_videos1['video_download'] = $sales_videos->video_download;
                $sales_videos1['type'] = $sales_videos->type;
                $sales_videos1['demo_id'] = $sales_videos->demo_id;
                $sales_videos1['sales_id'] = $sales_videos->sales_id;
                $sales_videos1['thumbnail_img'] = $sales_videos->thumbnail_img;
                $sales_videos1['img_name'] = $sales_videos->img_name;
              $salesdemo = $db_ext->table('SalesDemoVideo')->insert($sales_videos1);
                   
            }

        
        $demo_tips = DB::table('DemoTips')->where('cid', $cid)->whereIn('pid',$product_id)->get();
       dd($demo_tips);
        foreach ($demo_tips as $demo) {
            $demo1['pid'] = $demo->pid;
            $demo1['Desc'] = $demo->Desc;
            $demo1['cid'] = $demo->cid;
            $demo1['video_name'] = $demo->video_name;
            $demo1['video_url'] = $demo->video_url;
            $demo1['video_download'] = $demo->video_download;
            $demo1['thumbnail_img'] = $demo->thumbnail_img;
            $demo1['img_name'] = $demo->img_name;
            $demo_tips = $db_ext->table('DemoTips')->insert($demo1);
        }
        $features = DB::table('Features')->where('cid', $cid)->get();
        
        foreach ($features as $feature) {
            $feature1['pid'] = $feature->pid;
            $feature1['title'] = $feature->title;
            $feature1['desc'] = $feature->desc;
            $feature1['icon'] = $feature->icon;
            $feature1['sequence'] = $feature->sequence;
            //$feature1['country_id'] = $feature->country_id;
            $feature1['icon_name'] = $feature->icon_name;
            $feature1['cid'] = $feature->cid;
             $db_ext->table('Features')->insert($feature1);
        }
        $general = DB::table('Generals')->where('cid', $cid)->get();
        foreach ($general as $generals) {
            $generals1['pid'] = $generals->pid;
            $generals1['video_name'] = $generals->video_name;
            $generals1['images'] = $generals->images;
            $generals1['image_title'] = $generals->image_title;
            $generals1['desc'] = $generals->desc;
            $generals1['desc_title'] = $generals->desc_title;
            $generals1['video_url'] = $generals->video_url;
            $generals1['is_video_downloaded'] = $generals->is_video_downloaded;
            $generals1['img_name'] = $generals->img_name;
            $generals1['cid'] = $generals->cid;
            $db_ext->table('Generals')->insert($generals1);
        }
        $general_img = DB::table('GeneralImages')->where('cid', $cid)->get();

        foreach ($general_img as $general_imgs) {
            $generals_img1['pid'] = $general_imgs->pid;
            $generals_img1['img_name'] = $general_imgs->img_name;
            $generals_img1['img'] = $general_imgs->img;
            $generals_img1['cid'] = $general_imgs->cid;
            $db_ext->table('GeneralImages')->insert($generals_img1);
        }
        $Libraries = DB::table('Library')->where('cid', $cid)->get();
        foreach ($Libraries as $library) {
            $library1['pid'] = $library->pid;
            $library1['img_name'] = $library->img_name;
            $library1['video_url'] = $library->video_url;
            $library1['videoFileName'] = $library->videoFileName;
            $library1['videoPath'] = $library->videoPath;
            $library1['isVideo'] = $library->isVideo;
            $library1['isVideo_downloaded'] = $library->isVideo_downloaded;
            $library1['sequence'] = $library->sequence;
            $library1['img'] = $library->img;
            $library1['cid'] = $library->cid;
            $db_ext->table('Library')->insert($library1);
        }
        $option = DB::table('Options')->where('cid', $cid)->get();
       
        foreach ($option as $options) {
            $options1['pid'] = $options->pid;
            $options1['title'] = $options->title;
            $options1['desc'] = $options->desc;
            $options1['icon'] = $options->icon;
            $options1['sequence'] = $options->sequence;
            $options1['icon_name'] = $options->icon_name;
            $db_ext->table('Options')->insert($options1);
        }
        $details= DB::table('PartDetails')->where('cid', $cid)->get();
        foreach ($details as $detail) {
           // $detail1['id'] = $detail->id;
            $detail1['pid'] = $detail->pid;
            $detail1['prid'] = $detail->prid;
            $detail1['part_no'] = $detail->part_no;
            $detail1['desc'] = $detail->desc;
            $detail1['price'] = $detail->price;
            $detail1['req'] = $detail->req;
            $detail1['cid'] = $detail->cid;
            $db_ext->table('PartDetails')->insert($detail1);
        }
        $part_price= DB::table('PartPrice')->where('cid', $cid)->get();
        foreach ($part_price as $part_prices) {
            $part_prices1['pid'] = $part_prices->pid;
            $part_prices1['prid'] = $part_prices->prid;
            $part_prices1['title'] = $part_prices->title;
            $part_prices1['desc'] = $part_prices->desc;
            $part_prices1['sequence'] = $part_prices->sequence;
            $part_prices1['cid'] = $part_prices->cid;
            $db_ext->table('PartPrice')->insert($part_prices1);
        }
        $Partmaster= DB::table('PartMaster')->where('cid', $cid)->get();
        foreach ($Partmaster as $Partmasters) {
            //$Partmaster1['id'] = $Partmasters->id;
            $Partmaster1['pid'] = $Partmasters->pid;
            $Partmaster1['part'] = $Partmasters->part;
            $Partmaster1['cid'] = $detail->cid;
            $db_ext->table('PartMaster')->insert($Partmaster1);
        }
        $partorder= DB::table('PartOrder')->where('cid', $cid)->get();
        foreach ($partorder as $partorders) {
            $partorder1['pid'] = $partorders->pid;
            $partorder1['index'] = $partorders->index;
            $partorder1['cid'] = $partorders->cid;
            $db_ext->table('PartOrder')->insert($partorder1);
        }
        $sales_tips= DB::table('SalesTips')->where('cid', $cid)->get();
        foreach ($sales_tips as $sales_tip) {
            $sales_tip1['pid'] = $sales_tip->pid;
            $sales_tip1['Desc'] = $sales_tip->Desc;
            $sales_tip1['sequence'] = $sales_tip->sequence;
            $sales_tip1['cid'] = $sales_tip->cid;
            $sales_tip1['video_name'] = $sales_tip->video_name;
            $sales_tip1['video_url'] = $sales_tip->video_url;
            $sales_tip1['video_download'] = $sales_tip->video_download;
            
            $db_ext->table('SalesTips')->insert($sales_tip1);
        }
        $services= DB::table('Service')->where('cid', $cid)->get();
       
        foreach ($services as $service) {
            $service1['pid'] = $service->pid;
            $service1['cid'] = $service->cid;
            $service1['sequence'] = $service->sequence;
            $service1['img_name'] = $service->img_name;
            $service1['video_url'] = $service->video_url;
            $service1['videoFileName'] = $service->videoFileName;
            $service1['videoPath'] = $service->videoPath;
            $service1['isVideo'] = $service->isVideo;
            $service1['isVideo_downloaded'] = $service->isVideo_downloaded;
            $service1['img'] = $service->img;
             $db_ext->table('Service')->insert($service1);
        }
        $specificationMasters= DB::table('SpecificationMaster')->where('cid', $cid)->get();
       
        foreach ($specificationMasters as $specificationMaster) {
            $specificationMaster1['pid'] = $specificationMaster->pid;
            $specificationMaster1['cid'] = $specificationMaster->cid;
            $specificationMaster1['model'] = $specificationMaster->model;
             $db_ext->table('SpecificationMaster')->insert($specificationMaster1);
        }
        $specificationDeatils = DB::table('SpecifiactionDetails')->where('cid', $cid)->get();
      
        foreach ($specificationDeatils as $specificationDeatil) {
            $specificationDeatil1['sid'] = $specificationDeatil->sid;
            $specificationDeatil1['pid'] = $specificationDeatil->pid;
            $specificationDeatil1['title'] = $specificationDeatil->title;
            $specificationDeatil1['desc'] = $specificationDeatil->desc;
            $specificationDeatil1['sequence'] = $specificationDeatil->sequence;
            $specificationDeatil1['cid'] = $specificationDeatil->cid;
            $ddfs = $db_ext->table('SpecifiactionDetails')->insert($specificationDeatil1);
          
        }
        
       
    }
}
