<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DemoTips;
use App\Models\SalesDemoVideo;
use Illuminate\Support\Facades\DB;


class DemoTipsController extends Controller
{

    public function index()
    {
        dd("hello1");
        die();
        $result = DemoTips::select('DemoTips.*', 'Products.name')->join('Products', 'DemoTips.pid', '=', 'Products.id')
            ->orderBy('Products.name', 'asc')->get();
        $grouped = $result->groupBy('name');
        return view('tips.demo.index', ['result' => $grouped->toArray()]);
    }

    public function add(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            
            
            $part = new DemoTips;
            $uploadimage =  $this->uploadImage($request->img_name,'','');
            
            $part->img_name =  $uploadimage[2];
            $part->thumbnail_img =  $uploadimage[1];
            $uploadvideo =  $this->upload($request->video);
            
            $part->video_name = $uploadvideo[2];
            $part->video_url = $uploadvideo[1];
             $part->video_download = 0;
            $part->pid = $request->pid;
            $part->cid = $request->country_id;
            $part->Desc = $request->Desc;
            
            $part->save();
            $demo = new SalesDemoVideo;
            $demo->video_name = $uploadvideo[2];
            $demo->video_url = $uploadvideo[1];
            $demo->video_download = 0;
            $demo->pid = $request->pid;
            $demo->demo_id = $part->Id;
            $demo->cid = $request->country_id;
            $demo->type = 1;
            $demo->img_name =  $uploadimage[2];
            $demo->thumbnail_img =  $uploadimage[1];
            $demo->save();
            $this->versionUpdate($request->country_id);
          //  $this->addVersionLog($uploadimage[1],$uploadvideo[1], $request->country_id);
            return redirect()->route('tips-demo')->with('success', 'New Demo Tip Added');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('tips.demo.add', compact('products'));
        }
    }

    public function update(DemoTips $part, SalesDemoVideo $demo, Request $request)
    {
        die("hello");
        if ($request->getMethod() == 'POST') {
          
            $image = $request->file('img_name');
            $value =  DB::table('SalesDemoVideo')->where('demo_id', $part->Id)->get();
           
            if (!empty($value[0])||!count($value)<0) {
               
                $uploadimage =  $this->updateImage($part->img_name, $image,$part->Id);
                $uploadvideo =  $this->updateVideo($part->video_name, $request->video,$part->Id);
                $part->thumbnail_img =  $uploadimage[1];
                $demo->thumbnail_img = $uploadimage[1];
                $demo->img_name =  $uploadimage[2];
                $part->img_name =  $uploadimage[2];
                $part->video_name = $uploadvideo[1];
                $part->video_url = $uploadvideo[0];
                $demo->video_name = $uploadvideo[1];
                $demo->video_url = $uploadvideo[0];
                $part->pid = $request->pid;
                $demo->pid = $request->pid;
                $part->cid = $request->country_id;
                $demo->cid = $request->country_id;
                $demo->video_download = 0;
                $demo->demo_id = $part->Id;
                $demo->type = 1;
                $part->Desc = $request->Desc;
                $part->save();
                $userData = SalesDemoVideo::updatedemoVideo($uploadvideo, $request, $part->Id, $uploadimage);
            } else {
              
              if($request->img_name){
                $uploadimage =  $this->uploadImage($request->img_name,$part->Id);
                $part->thumbnail_img =  $uploadimage[1];
                $demo->thumbnail_img = $uploadimage[1];
                $demo->img_name =  $uploadimage[2];
                $part->img_name =  $uploadimage[2];
              }
              if($request->video){
                $uploadvideo =  $this->upload($request->video,$part->Id);
                $part->video_name = $uploadvideo[2];
                $part->video_url = $uploadvideo[1];
                $demo->video_name = $uploadvideo[2];
                $demo->video_url = $uploadvideo[1];
              }
                $part->pid = $request->pid;
                $demo->pid = $request->pid;
                $part->cid = $request->country_id;
                $demo->cid = $request->country_id;
                $demo->video_download = 0;
                $demo->demo_id = $part->Id;
                $demo->type = 1;
                $part->Desc = $request->Desc;               
                $part->save();
                $demo->save();
            }
            $this->versionUpdate($request->country_id);
            return redirect()->route('tips-demo')->with('success', 'Demo Tips Update');
        }    
            else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('tips.demo.update', compact('part', 'products'));
        }
    }

    public function delete(DemoTips $part)
    {
        $part->delete();
        $userData = SalesDemoVideo::deleteSales($part->Id);
        $this->versionUpdate($part->cid);
        return redirect()->route('tips-demo')->with('success', 'Demo Tip Deleted');
    }
    public function getDemo(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = DemoTips::select('DemoTips.*', 'Products.name')->join('Products', 'DemoTips.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')
                ->whereIn('cid', $array['country_id'])->get();
            $grouped = $result->groupBy('name');
            $data = view('tips.demo.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = DemoTips::select('DemoTips.*', 'Products.name')->join('Products', 'DemoTips.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')->get();
            $grouped = $result->groupBy('name');
            $data = view('tips.demo.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }
}
