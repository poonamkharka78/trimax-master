<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SalesTips;
use App\Models\SalesDemoVideo;
use DB;

class SalesTipsController extends Controller
{

    public function index()
    {
        $result = SalesTips::select('SalesTips.*', 'Products.name')->join('Products', 'SalesTips.pid', '=', 'Products.id')
            ->orderBy('Products.name', 'asc')->orderBy('SalesTips.sequence', 'asc')->get();
        $grouped = $result->groupBy('name');
        return view('tips.sales.index', ['result' => $grouped->toArray()]);
    }

    public function add(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $part = new SalesTips;
            $image = $request->file('img_name');
            $original_image = time() . '.' . $image->getClientOriginalExtension();
            $original = explode(".", $original_image);
            $destinationPath = public_path('uploads/image');
            $image->move($destinationPath, $original_image);
            $image_url = url('/uploads/image') . '/' .  $original_image;
            $part->img_name =  $original_image;
            $part->thumbnail_img =  $image_url;
            $req =  $this->upload($request->video);
            $part->video_name = $req[2];
            $part->video_url = $req[1];
            // $part->is_video_downloaded = 0;
            $part->pid = $request->pid;
            $part->cid = $request->country_id;
            $part->Desc = $request->Desc;
            $part->sequence = $request->sequence;
            $part->save();

            $demo = new SalesDemoVideo;
            $demo->video_name = $req[2];
            $demo->video_url = $req[1];
            $demo->video_download = 0;
            $demo->pid = $request->pid;
            $demo->sales_id = $part->Id;
            $demo->cid = $request->country_id;
            $demo->type = 1;
            $demo->img_name =  $original_image;
            $demo->thumbnail_img =  $image_url;
            $demo->save();
            $this->versionUpdate($request->country_id);
            return redirect()->route('tips-sales')->with('success', 'New Sales Tip Added');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            foreach ($products as $row) {
                $row->max = SalesTips::where('pid', '=', $row->id)->max('sequence');
            }
            return view('tips.sales.add', compact('products'));
        }
    }

    public function update(SalesTips $part, SalesDemoVideo $demo, Request $request)
    {
        dd("hello1");
        if ($request->getMethod() == 'POST') {
            $image = $request->file('img_name');
            $value =  DB::table('SalesDemoVideo')->where('sales_id', $part->Id)->get();
            if (!empty($value[0]) || !count($value) < 0) {
                $uploadimage =  $this->updateImage($part->img_name, $image, $part->Id);
                $uploadvideo =  $this->updateVideo($part->video_name, $request->video, $part->Id);
                $part->thumbnail_img =  $uploadimage[1];
                $demo->thumbnail_img = $uploadimage[1];
                $demo->img_name =  $uploadimage[2];
                $part->img_name =  $uploadimage[2];
                $part->video_name = $uploadvideo[1];
                $part->video_url = $uploadvideo[0];
                $demo->video_name = $uploadvideo[1];
                $demo->video_url = $uploadvideo[0];
                $part->pid = $request->pid;
                $demo->pid = $request->pid;
                $part->cid = $request->country_id;
                $demo->cid = $request->country_id;
                $demo->video_download = 0;
                $demo->demo_id = $part->Id;
                $demo->type = 1;
                $part->Desc = $request->Desc;

                $part->save();
                $userData = SalesDemoVideo::updatesalesVideo($uploadvideo, $request, $part->Id, $uploadimage);
            } else {
                if ($request->img_name) {
                    $uploadimage =  $this->uploadImage($request->img_name, $part->Id);
                    $part->thumbnail_img =  $uploadimage[1];
                    $demo->thumbnail_img = $uploadimage[1];
                    $demo->img_name =  $uploadimage[2];
                    $part->img_name =  $uploadimage[2];
                }
                if ($request->video) {
                    $uploadvideo =  $this->upload($request->video, $part->Id);
                    $part->video_name = $uploadvideo[2];
                    $part->video_url = $uploadvideo[1];
                    $demo->video_name = $uploadvideo[2];
                    $demo->video_url = $uploadvideo[1];
                }
                $part->pid = $request->pid;
                $demo->pid = $request->pid;
                $part->cid = $request->country_id;
                $demo->cid = $request->country_id;
                $demo->video_download = 0;
                $demo->sales_id = $part->Id;
                $demo->type = 1;
                $part->Desc = $request->Desc;
                $part->sequence = $request->sequence;
                $demo->save();
                $part->save();
            }
            $this->versionUpdate($request->country_id);
            return redirect()->route('tips-sales')->with('success', 'Sales Tip Update');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('tips.sales.update', compact('part', 'products'));
        }
    }

    public function delete(SalesTips $part)
    {
        $uploadimage =  $this->deleteImage($part->img_name);
        $userData = SalesDemoVideo::deleteSales($part->Id);
        $part->delete();
        $request = $part;
        $this->versionUpdate($part->cid);
        return redirect()->route('tips-sales')->with('success', 'Sales Tip Deleted');
    }
    public function getSales(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = SalesTips::select('SalesTips.*', 'Products.name')->join('Products', 'SalesTips.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')->orderBy('SalesTips.sequence', 'asc')
                ->whereIn('cid', $array['country_id'])->get();
            $grouped = $result->groupBy('name');
            $data = view('tips.sales.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = SalesTips::select('SalesTips.*', 'Products.name')->join('Products', 'SalesTips.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')->orderBy('SalesTips.sequence', 'asc')->get();
            $grouped = $result->groupBy('name');
            $data = view('tips.sales.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }
}
