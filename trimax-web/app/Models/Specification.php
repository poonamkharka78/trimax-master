<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Specification extends Model {

    protected $table = 'SpecificationMaster';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function dependencyDelete($sid) {
        DB::table('SpecifiactionDetails')->where('sid', $sid)->delete();
    }
    public function dependencyGet($sid) {
        $req =DB::table('SpecifiactionDetails')->whereIn('sid', $sid)->get();
       
        return $req;
    }
}
