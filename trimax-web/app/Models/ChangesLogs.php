<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChangesLogs extends Model
{
    protected $table = 'ChangesLogs';
    protected $primaryKey = 'Id'; 
    public $timestamps = false;
  
}
