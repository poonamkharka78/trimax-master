<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Features extends Model {

    protected $table = 'Features';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
