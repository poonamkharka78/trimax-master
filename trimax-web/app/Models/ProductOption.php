<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    protected $table = 'ProductOption';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function Product()
    {
        return $this->belongsTo(Product::class, 'pid', 'id');
    }
}
