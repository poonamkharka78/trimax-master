<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DemoTips extends Model {

    protected $table = 'DemoTips';
    protected $primaryKey = 'Id';
    public $timestamps = false;

    public  function product()
    {
        return $this->belongsTo(Product::class, 'pid', 'id');
    }

    public function salesDemoVideo()
    {
    	return $this->belongsTo(SalesDemoVideo::class, 'demo_id', 'id');
    }

    public function delete()
    {
        $this->salesDemoVideo()->delete();
        return parent::delete();
    }

}
