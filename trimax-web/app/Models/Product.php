<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model {


    protected $table = 'Products';

    protected $primaryKey = 'id';
    
    public $timestamps = false;

    public function dependencyDelete($productID,$productImgname) {
       
        DB::table('DemoTips')->where('pid', $productID)->delete();
        DB::table('Features')->where('pid', $productID)->delete();
        DB::table('GeneralImages')->where('pid', $productID)->delete(); 
        DB::table('Generals')->where('pid', $productID)->delete();
        DB::table('Library')->where('pid', $productID)->delete();
        DB::table('Options')->where('pid', $productID)->delete();
        DB::table('PartDetails')->where('pid', $productID)->delete();
        DB::table('PartMaster')->where('pid', $productID)->delete();
        DB::table('PartOrder')->where('pid', $productID)->delete();
        DB::table('PartPrice')->where('pid', $productID)->delete();
        DB::table('SalesTips')->where('pid', $productID)->delete();
        DB::table('SpecifiactionDetails')->where('pid', $productID)->delete();
        DB::table('SpecificationMaster')->where('pid', $productID)->delete();
         DB::table('Products')->where('img_name',$productImgname)->delete();
       
    }
    public function dependencyGet($productID) {
      $image =  DB::table('Products')->where('id', $productID)->get('image'); 
        return $image;
    }

    public  function salesTips()
    {
        return $this->hasOne(SalesTips::class, 'pid', 'id');
    }
    public  function demoTips()
    {
        return $this->hasOne(DemoTips::class, 'pid', 'id');
    }
    
    
}
