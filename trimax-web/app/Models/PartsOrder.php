<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartsOrder extends Model {

    protected $table = 'PartOrder';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
