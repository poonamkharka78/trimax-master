<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Generals extends Model {

    protected $table = 'Generals';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
