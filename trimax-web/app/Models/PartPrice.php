<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartPrice extends Model {

    protected $table = 'PartPrice';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
