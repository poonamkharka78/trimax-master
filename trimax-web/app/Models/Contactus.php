<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contactus extends Model
{
    protected $table = 'ContactUs';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
