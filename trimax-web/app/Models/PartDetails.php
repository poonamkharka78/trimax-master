<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartDetails extends Model {

    protected $table = 'PartDetails';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
