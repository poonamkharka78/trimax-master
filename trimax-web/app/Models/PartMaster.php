<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartMaster extends Model
{
    protected $table = 'PartMaster';

    protected $primaryKey = 'id';
    
    public $timestamps = false;
}
