<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecificationDetails extends Model {

    protected $table = 'SpecifiactionDetails';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
