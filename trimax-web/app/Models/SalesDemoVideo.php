<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;


class SalesDemoVideo extends Model
{
    protected $table = 'SalesDemoVideo';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public static function updatesalesVideo($uploadvideo, $request, $id, $uploadimage)
    {
        $value =  DB::table('SalesDemoVideo')->where('sales_id', $id)->get();
      
            $sale = SalesDemoVideo::where('sales_id', $id)->update([
                "pid" => $request->pid,
                "video_url" => $uploadvideo[0],
                "video_name" => $uploadvideo[1],
                'sales_id' => $id,
                'type' => 1,
                'cid' => $request->country_id,
                'video_download' => 0,
                "thumbnail_img" => $uploadimage[1],
                "img_name" => $uploadimage[0],
            ]);
            
        
    }
    public static function deleteSales($id){
        $res=SalesDemoVideo::where('sales_id',$id)->delete();
    }

    public static function updatedemoVideo($uploadvideo, $request, $id, $uploadimage)
    {
        $value =  DB::table('SalesDemoVideo')->where('demo_id', $id)->get();
      
            $sale = SalesDemoVideo::where('demo_id', $id)->update([
                "pid" => $request->pid,
                "video_url" => $uploadvideo[0],
                "video_name" => $uploadvideo[1],
                'demo_id' => $id,
                'type' => 1,
                'cid' => $request->country_id,
                'video_download' => 0,
                "thumbnail_img" => $uploadimage[1],
                "img_name" => $uploadimage[0],
            ]);
            
        
    }
    public static function deleteDemo($id){
        $res=SalesDemoVideo::where('demo_id',$id)->delete();
    }
}
