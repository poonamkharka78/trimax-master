<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesTips extends Model {

    protected $table = 'SalesTips';
    protected $primaryKey = 'Id';
    public $timestamps = false;
    
    public  function product()
    {
        return $this->belongsTo(Product::class, 'pid', 'id');
    }

}
