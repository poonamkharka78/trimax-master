<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Parts extends Model {

    protected $table = 'PartMaster';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function dependencyDelete($prid) {
        DB::table('PartDetails')->where('prid', $prid)->delete();
        DB::table('PartPrice')->where('prid', $prid)->delete();
    }

}
