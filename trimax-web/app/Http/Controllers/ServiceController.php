<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    public function index($product_id = 1, $isVideo = 1)
    {
        $result = Service::select('Service.*')
                ->where('Service.pid', '=', $product_id)->where('Service.isVideo', '=', $isVideo)
                ->orderBy('name', 'asc')->orderBy('Service.sequence', 'asc')
                ->groupBy('Service.sequence')
                ->having('count','>',1)
                ->get();                
        $products = \App\Models\Product::get();
        return view('service.index', ['result' => $result, 'products' => $products, 'product_id' => $product_id, 'isVideo' => $isVideo]);
    }
    public function add(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $service = new Service;
            $image = $request->file('img_name');
            $original_images = time() . '.' . $image->getClientOriginalExtension();
            $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $original_images);
            $original = explode(".",$original_image);
            $destinationPath = public_path('uploads/image');
            $image->move($destinationPath, $original_image);
            $image_url = url('/uploads/image') . '/' .  $original_image;   
            if ($request->video) {
                $req =  $this->upload($request->video);      
                $isVideo = 1;
            } else {
                $isVideo = 0;
            }
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'img_name' => $image_url,
                    'video_url' => $req[1]??'',
                    'videoFileName' => $req[2]??'',
                    'videoPath' => null,
                    'isVideo' => $isVideo,
                    'isVideo_downloaded' => 0,
                    'sequence' => $request->sequence,
                    'img' => $original_image,
                    'cid' => $request->country_id[$i],
                ];
            }
            Service::insert($product); 
            $this->versionUpdate($request->country_id);    
            if($request->file('img_name')){   
                $this->addVersionLog($image_url,'',$request->country_id); 
            }elseif($request->video){
                $this->addVersionLog('',$req[1],$request->country_id);
            }else{
               
            }
          
            return redirect()->route('service')->with('success', 'New Service Added');
        } else {
            
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            $max = \Illuminate\Support\Facades\DB::table('Service')->where('pid', '=', $request->product_id)->max('sequence');
            return view('service.add', compact('products', 'max'));
        }
    }

    public function update(Service $library, Request $request)
    {
       
        if ($request->getMethod() == 'POST') {
            
            $img = $library->img;
            if ($request->hasFile('img_name')) {
                $image = $request->file('img_name');
                $str_arr = explode("/", $library->img_name);   
                if (file_exists(public_path('uploads/image') . '/' . $str_arr[5])) {
                     unlink(public_path('uploads/image') . '/' . $str_arr[5]);
                }
                $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $library->img); 
                $destinationPath = public_path('uploads/image');
                $image->move($destinationPath, $original_image);
                $image_url = url('/uploads/image') . '/' .  $original_image;   
                $library->img_name =  $image_url;
            }
            if ($request->video) {
                $updateVideo = $this->updateVideo($library->videoFileName,$request->video,'');
                $videoFileName = $updateVideo[1];
                $video_url = $updateVideo[0];
                $videoDownload = 0;
            }
            $ids = Service::select('id', 'cid')->where('img', $library->img)->get();
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['id'];
                $cid[] = $arr['cid'];
            }
            $update = Service::where('id', $id)->first();
                        if (!is_null($update)) {
                        $update['pid'] = $request->pid;
                        if($request->img_name!=null)
                        {
                            $update['img_name'] = $image_url;
                            $update['img'] = $original_image??$library->img;
                        }
                        $update['sequence'] = $request->sequence;
                        $update['cid'] = $request->cid;
                       
                        if($request->videoFileName!=null)
                        {
                            $update['videoFileName'] = $videoFileName??$library->videoFileName;
                            $update['video_url'] = $video_url??$library->video_url;
                            $update['isVideo_downloaded'] = $videoDownload ?? $library->isVideo_downloaded;
                        }
                        $update->save();
                     }

            $this->versionUpdate($cid);
            if($request->hasFile('img_name')){    
                $this->addVersionLog($library->image_name,'',$cid); 
            }elseif($request->hasFile('video')){
                $this->addVersionLog('',$library->video_url,$cid);
            }else{
                $this->addVersionLog($library->image_name,$library->video_url,$cid);
            }
            session()->flash('success', 'Service Media Update');
            return redirect()->route('service', ['product_id' => $library->pid, 'isVideo' => $library->isVideo]);
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('service.update', compact('library', 'products'));
        }
    }
    public function delete(Service $library)
    {
        $ids = Service::select('id', 'cid')->where('img', $library->img)->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['id'];
            $cid[] = $arr['cid'];
        }
        if (file_exists(public_path('uploads/videos') . '/' . $library->videoFileName)) {
            unlink(public_path('uploads/videos') . '/' . $library->videoFileName);
        }
        if(file_exists(public_path('uploads/image').'/'.$library->img)){
            unlink(public_path('uploads/image').'/'.$library->img);
        }else{
            dd('File does not exists.');
          }
          DB::table('Service')->whereIn('id', $id)->delete();
        $this->versionUpdate($cid);
        return back()->with('success', 'Service Media Deleted');
    }
    public function getService(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = Service::select('Service.*')
            ->where('Service.pid', '=', $request->product_id)->where('Service.isVideo', '=', $request->isVideo)
            ->whereIn('cid', $array['country_id'])
            ->orderBy('name', 'asc')->orderBy('Service.sequence', 'asc')
            ->groupBy('Service.sequence')
            ->having('count','>',1)
            ->get();                
             $products = \App\Models\Product::get();
            
            $data = view('service.ajax', ['result' => $result,'products' => $request->product_id, 'product_id' => $request->product_id, 'isVideo' => $request->isVideo])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = Service::select('Service.*')
            ->where('Service.pid', '=', $request->product_id)->where('Service.isVideo', '=', $request->isVideo)
            ->orderBy('name', 'asc')->orderBy('Service.sequence', 'asc')
            ->groupBy('Service.sequence')
            ->having('count','>',1)
            ->get();                
             $products = \App\Models\Product::get();
            $data = view('service.ajax', ['result' => $result,'products' => $request->product_id, 'product_id' => $request->product_id, 'isVideo' => $request->isVideo])->render();

            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }

}
