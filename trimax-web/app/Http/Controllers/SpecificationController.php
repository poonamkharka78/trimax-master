<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Specification;

class SpecificationController extends Controller
{

    public function index()
    {
        $result = Specification::select('SpecificationMaster.*', 'Products.name')->join('Products', 'SpecificationMaster.pid', '=', 'Products.id')
            ->orderBy('Products.name', 'asc')
            ->groupBy('SpecificationMaster.model')
            ->having('count', '>', 1)->get();
        $grouped = $result->groupBy('name');
        return view('specification.index', ['result' => $grouped->toArray()]);
    }

    public function add(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'cid' => $request->country_id[$i],
                    'model' => $request->model,
                ];
            }
            Specification::insert($product);
            $this->versionUpdate($request->country_id);
            return redirect()->route('specifications')->with('success', 'New Specification Added');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('specification.add', compact('products'));
        }
    }

    public function update(Specification $part, Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $ids = Specification::select('id', 'cid')->where('pid', $part->pid)
            ->where('model',$part->model)->get();
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['id'];
                $cid[] = $arr['cid'];
            }
            Specification::whereIn('id', $id)->update([
                'pid' => $request->pid,
                'model' => $request->model,
            ]);
            $this->versionUpdate($cid);
            return redirect()->route('specifications')->with('success', 'Specification Update');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('specification.update', compact('part', 'products'));
        }
    }

    public function delete(Specification $part)
    {
        $ids = Specification::select('id', 'cid')->where('pid', $part->pid)
            ->where('model',$part->model)->get();
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['id'];
                $cid[] = $arr['cid'];
            }
        $req = $part->dependencyGet($id);
        Specification::whereIn('id', $id)->delete();
       
        if(count($req)>0){
        $part->dependencyDelete($id);
        }
        $this->versionUpdate($cid);
        return redirect()->route('specifications')->with('success', 'Specification Deleted');
    }
    public function getSpecification(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = Specification::select('SpecificationMaster.*', 'Products.name')->join('Products', 'SpecificationMaster.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')
                ->groupBy('SpecificationMaster.model')
                ->having('count', '>', 1)
                ->whereIn('cid', $array['country_id'])->get();
            $grouped = $result->groupBy('name');
            $data = view('specification.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = Specification::select('SpecificationMaster.*', 'Products.name')->join('Products', 'SpecificationMaster.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')
                ->groupBy('SpecificationMaster.model')
                ->having('count', '>', 1)
                ->get();
            $grouped = $result->groupBy('name');
            $data = view('specification.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }
}
