<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\PartDetails;
use App\Models\Product;
use App\Models\Parts;
use App\Models\PartMaster;

class PartsDetailsController extends Controller {

    public function index() {
        $result = PartDetails::select('PartDetails.*', 'Products.name', 'PartMaster.part')
                        ->join('Products', 'PartDetails.pid', '=', 'Products.id')
                        ->join('PartMaster', 'PartDetails.prid', '=', 'PartMaster.id')
                        ->orderBy('Products.name', 'asc')->orderBy('PartMaster.part', 'asc')
                        ->orderBy('PartDetails.desc', 'asc')
                        ->groupBy('PartDetails.desc')
                        /*->having('count', '>', 1)*/->get();
        $grouped = $result->groupBy('name');
        return view('partsdetails.index', ['result' => $grouped->toArray()]);
    }

    /**
     * Adding Part Details
     * @param Request $request
     */
    public function add(Request $request) {
         /// $price = $request->currency . '' . $request->price;
        if ($request->getMethod() == 'POST') {
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'cid' => $request->country_id[$i],
                    'prid' => $request->prid,
                    'part_no' => $request->part_no,
                    'desc' => $request->desc,
                    'price' => $request->price,
                    'req' => $request->req,
                ];
            }
            $check = PartDetails::where('pid', $product[0]['pid'])->where('cid', $product[0]['cid'])->where('prid', $product[0]['prid'])->where('part_no', $product[0]['part_no'])->first();
            if($check){
                return redirect()->route('partsdetails')->with('error', 'Part Details Alreay Exists');
            } else {
                PartDetails::insert($product);
            
                $this->versionUpdate($request->country_id);
                return redirect()->route('partsdetails')->with('success', 'New Part Details Added');
            }
            
        } else {
            $products = Product::select('id', 'name')->groupBy('name')->orderBy('name', 'asc')->get();

            $result = \App\Models\Parts::select('PartMaster.*', 'Products.name')->join('Products', 'PartMaster.pid', '=', 'Products.id')->orderBy('Products.name', 'asc')->get();
            $result = $result->groupBy('name');

            return view('partsdetails.add', compact('products', 'result'));
        }
    }

    /**
     * Getting Part on the bases of Product Id
     * @param  int $id
     * @return json
     */
    public function myformAjax($id)
    {
        //$parts = PartMaster::select('id', 'part')->where("pid",$id)->get();
        $parts = Parts::select('PartMaster.*', 'Products.name')->join('Products', 'PartMaster.pid', '=', 'Products.id')
                ->where('Products.id', '=', $id)
                ->orderBy('Products.name', 'asc')
                ->get();
        
        return json_encode($parts);
    }

    public function update(PartDetails $part, Request $request) {
        if ($request->getMethod() == 'POST') {
            $ids = PartDetails::select('id', 'cid')->where('pid', $part->pid)
                ->where('part_no', $part->part_no)->get();
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['id'];
                $cid[] = $arr['cid'];
            }
            PartDetails::whereIn('id', $id)->update([
                'pid' => $request->pid,
                'prid' => $request->prid,
                'part_no' => $request->part_no,
                'desc' => $request->desc,
                'price' => $request->price,
                'req' => $request->req,

            ]);
            $this->versionUpdate($cid);
            return redirect()->route('partsdetails')->with('success', 'Part Details Update');
        } else {
            $products = Product::groupBy('name')->orderBy('name', 'asc')->get();
            $result = \App\Models\Parts::select('PartMaster.*', 'Products.name')->join('Products', 'PartMaster.pid', '=', 'Products.id')
                            ->orderBy('Products.name', 'asc')->get();
            $result = $result->groupBy('name');
            
            return view('partsdetails.update', compact('part', 'products', 'result'));
        }
    }

    public function delete(PartDetails $part) {
        $ids = PartDetails::select('id', 'cid')->where('pid', $part->pid)
                ->where('part_no', $part->part_no)->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['id'];
            $cid[] = $arr['cid'];
        }
        PartDetails::whereIn('id', $id)->delete();
        $this->versionUpdate($cid);
        return redirect()->route('partsdetails')->with('success', 'Part Details Deleted');
    }

    public function getPartsDetail(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = PartDetails::select('PartDetails.*', 'Products.name', 'PartMaster.part')
                        ->join('Products', 'PartDetails.pid', '=', 'Products.id')
                        ->join('PartMaster', 'PartDetails.prid', '=', 'PartMaster.id')
                        ->orderBy('Products.name', 'asc')->orderBy('PartMaster.part', 'asc')
                        ->orderBy('PartDetails.desc', 'asc')
                        ->groupBy('PartDetails.desc')
                        ->whereIn('PartDetails.cid',$array['country_id'])->get();
           $grouped = $result->groupBy('name');
            $data = view('partsdetails.ajax', ['result' =>  $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = PartDetails::select('PartDetails.*', 'Products.name', 'PartMaster.part')
            ->join('Products', 'PartDetails.pid', '=', 'Products.id')
            ->join('PartMaster', 'PartDetails.prid', '=', 'PartMaster.id')
            ->orderBy('Products.name', 'asc')->orderBy('PartMaster.part', 'asc')
            ->orderBy('PartDetails.desc', 'asc')
            ->groupBy('PartDetails.desc')
            ->having('count', '>', 1)->get();
            $grouped = $result->groupBy('name');
            $data = view('partsdetails.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }

    // public function getupdateData(PartDetails $part,Request $request) {    
    //     $result = PartDetails::select('*')->where('pid',$request->pid)
    //     ->where('prid',$request->prid)->where('cid',$request->country_id)->get();
    //       $data = view('partsdetails.updatecountry', ['result' => $result])->render();

    //         return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        
    // }
}
