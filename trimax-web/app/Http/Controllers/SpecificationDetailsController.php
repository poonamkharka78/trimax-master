<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SpecificationDetails;

class SpecificationDetailsController extends Controller
{

    public function index()
    {
        $result = SpecificationDetails::select('SpecifiactionDetails.*', 'Products.name', 'SpecificationMaster.model')
            ->join('Products', 'SpecifiactionDetails.pid', '=', 'Products.id')
            ->join('SpecificationMaster', 'SpecifiactionDetails.sid', '=', 'SpecificationMaster.id')
            ->orderBy('Products.name', 'asc')
            ->orderBy('SpecificationMaster.model', 'asc')
            ->orderBy('SpecifiactionDetails.sequence', 'asc')
            //->groupBy('SpecifiactionDetails.desc')
            //->having('count', '>', 1)
            ->get();
        $grouped = $result->groupBy('name');
        return view('specifications_details.index', ['result' => $grouped->toArray()]);
    }

    public function add(Request $request)
    {
       // $metrics = $request->metrice . '' . $request->desc;
        if ($request->getMethod() == 'POST') {
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'cid' => $request->country_id[$i],
                    'sid' => $request->model[$request->pid],
                    'title' => $request->title,
                    'desc' => $request->desc,
                    'sequence' => $request->sequence,
                ];
            }
            SpecificationDetails::insert($product);
            $this->versionUpdate($request->country_id);
            return redirect()->route('specifications-details')->with('success', 'New Specification Details Added');
        } else {
            $result = \App\Models\Specification::select('SpecificationMaster.*', 'Products.name')
                ->join('Products', 'SpecificationMaster.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')->orderBy('SpecificationMaster.model', 'asc')->get();
            $result = $result->groupBy('name');
            foreach ($result as $models) {
                foreach ($models as $row) {
                    $row->max = \Illuminate\Support\Facades\DB::table('SpecifiactionDetails')->where('id', '=', $row->id)->max('sequence');
                }
            }
            return view('specifications_details.add', compact('result'));
        }
    }

    public function update(SpecificationDetails $part, Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $ids = SpecificationDetails::select('id', 'cid')->where('pid', $part->pid)
                ->where('sid', $part->sid)->where('title', $part->title)->get();
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['id'];
                $cid[] = $arr['cid'];
            }
            $idss = $part->id;
            $update = SpecificationDetails::where('id', $idss)->first();
            $update['pid'] = $request->pid;
            $update['sid'] = $request->sid;
            $update['desc'] = $request->desc;
            $update['pid'] = $request->pid;
            $update['cid'] = $request->cid;
            $update['sequence'] = $request->sequence;
            $update['sequence'] = $request->sequence;
            $update->save();
            $this->versionUpdate($cid);
            return redirect()->route('specifications-details')->with('success', 'Specification Details Update');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            $result = \App\Models\Specification::select('SpecificationMaster.*', 'Products.name')
                ->join('Products', 'SpecificationMaster.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')->orderBy('SpecificationMaster.model', 'asc')->get();
            $result = $result->groupBy('name');
            return view('specifications_details.update', compact('part', 'products', 'result'));
        }
    }

    public function delete(SpecificationDetails $part)
    {
        $cid = $part->cid;
        $ids = SpecificationDetails::select('id', 'cid')->where('pid', $part->pid)
            ->where('model', $part->model)->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['id'];
            $cid[] = $arr['cid'];
        }
        SpecificationDetails::where('id', $part->id)->delete();
        $this->versionUpdate($cid);
        return redirect()->route('specifications-details')->with('success', 'Specification Details Deleted');
    }
    public function getDetail(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = SpecificationDetails::select('SpecifiactionDetails.*', 'Products.name', 'SpecificationMaster.model')
                ->join('Products', 'SpecifiactionDetails.pid', '=', 'Products.id')
                ->join('SpecificationMaster', 'SpecifiactionDetails.sid', '=', 'SpecificationMaster.id')
                ->orderBy('Products.name', 'asc')
                ->orderBy('SpecificationMaster.model', 'asc')
                ->orderBy('SpecifiactionDetails.sequence', 'asc')
                ->groupBy('SpecifiactionDetails.desc')
                ->having('count', '>', 1)
                ->whereIn('SpecifiactionDetails.cid', $array['country_id'])
                ->get();
            $grouped = $result->groupBy('name');
            $data = view('specifications_details.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = SpecificationDetails::select('SpecifiactionDetails.*', 'Products.name', 'SpecificationMaster.model')
                ->join('Products', 'SpecifiactionDetails.pid', '=', 'Products.id')
                ->join('SpecificationMaster', 'SpecifiactionDetails.sid', '=', 'SpecificationMaster.id')
                ->orderBy('Products.name', 'asc')
                ->orderBy('SpecificationMaster.model', 'asc')
                ->orderBy('SpecifiactionDetails.sequence', 'asc')
                ->groupBy('SpecifiactionDetails.desc')
                ->having('count', '>', 1)
                ->get();
            $grouped = $result->groupBy('name');
            $data = view('specifications_details.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }
}
