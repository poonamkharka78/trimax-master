<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DemoTips;
use App\Models\SalesDemoVideo;
use Illuminate\Support\Facades\DB;


class DemoTipsController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit','256M');
    }

    public function index()
    {
        $result = DemoTips::select('DemoTips.*', 'Products.name')
                ->join('Products', 'DemoTips.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')
                //->groupby('DemoTips.Desc')/*->having('count', '>', 1)*/
                ->get();

        $grouped = $result->groupBy('name');
        return view('tips.demo.index', ['result' => $grouped->toArray()]);
    }

    public function add(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $part = new DemoTips;
            $uploadimage =  $this->uploadImage($request->img_name, '', '');
            $uploadvideo =  $this->upload($request->video);
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'desc' => $request->Desc,
                    'video_name' => $uploadvideo[2],
                    'video_url' => $uploadvideo[1],
                    'video_download' => 0,
                    'thumbnail_img' => $uploadimage[1],
                    'img_name' => $uploadimage[2],
                    'cid' => $request->country_id[$i],
                    'video_path' => null,

                ];
            }
            DemoTips::insert($product);
            for ($j = 0; $j < count($request->country_id); $j++) { 
                $saledemo[] = [
                    'pid' => $request->pid,
                    'video_url' => $uploadvideo[1],
                    'video_name' => $uploadvideo[2],
                    'video_download' => 0,
                    'type' => 1,
                    'demo_id' => DB::getPDO()->lastInsertId(),
                    'thumbnail_img' => $uploadimage[1],
                    'img_name' => $uploadimage[2],
                    'cid' => $request->country_id[$j],
                    'video_path' => null,

                ];
            }
            SalesDemoVideo::insert($saledemo);
            $this->versionUpdate($request->country_id);
            //  $this->addVersionLog($uploadimage[1],$uploadvideo[1], $request->country_id);
            return redirect()->route('tips-demo')->with('success', 'New Demo Tip Added');
        } else {
           $products = \App\Models\Product::with('demoTips')->orderBy('name', 'asc')->get();
            return view('tips.demo.add', compact('products'));
        }
    }

    public function update(DemoTips $part, SalesDemoVideo $demo, Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $image = $request->file('img_name');
            $value =  DB::table('SalesDemoVideo')->where('demo_id', $part->Id)->get();
            $ids = DemoTips::select('id', 'cid')->where('img_name', $part->img_name)
                ->get();
            $uploadimage =  $this->updateImage($request->img_name, $image, $part->Id);
            $uploadvideo =  $this->updateVideo($request->video_name, $request->video, $part->Id);
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['Id'];
                $cid[] = $arr['cid'];
            }
            DemoTips::where('id', $part->Id)->update([
                'pid' => $request->pid,
                'Desc' => $request->Desc,
                'video_name' => empty(isset($request->video_name)) ? "" : $uploadvideo[1]??$part->video_name,
                'video_url' => empty(isset($request->video_name)) ? "" : $uploadvideo[0]??$part->video_url,
                'thumbnail_img' => empty(isset($request->img_name)) ? " " : $uploadimage[1]??$part->thumbnail_img, 
                'img_name' => empty(isset($request->img_name)) ? " " : $uploadimage[2]??$part->img_name,
            ]);
            $demo_ids = SalesDemoVideo::select('id', 'cid')->where('img_name', $part->img_name)
                ->get();
            $saledemoarray = json_decode(json_encode($demo_ids), true);
            $id1 = [];
            foreach ($saledemoarray as $array) {
                $id1[] = $array['id'];
            }
            $country_id = DemoTips::where('id', $part->Id)->first();
            $find = SalesDemoVideo::where('demo_id', $part->Id)->first();
            if($find) {
                   SalesDemoVideo::where('demo_id', $part->Id)->update([
                    'pid' => $request->pid,
                    'video_name' => $uploadvideo[1]??$demo->video_name,
                    'video_url' => $uploadvideo[0]??$demo->video_url,
                    'thumbnail_img' => $uploadimage[1]??$demo->thumbnail_img,
                    'img_name' => $uploadimage[2]??$demo->img_name,
                ]); 
            } else { 
                $saledemo[] = [
                    'pid' => $request->pid,
                    'video_url' => $uploadvideo[1],
                    'video_name' => $uploadvideo[2],
                    'video_download' => 0,
                    'type' => 1,
                    'demo_id' => $part->Id,
                    'thumbnail_img' => $uploadimage[1],
                    'img_name' => $uploadimage[2],
                    'cid' => $country_id->cid,
                    'video_path' => null,

               ];
                SalesDemoVideo::insert($saledemo);
            }
             
            
            $this->versionUpdate($cid);
            return redirect()->route('tips-demo')->with('success', 'Demo Tips Update');
        } else {
            $products = \App\Models\DemoTips::with('product')->where('id',$part->Id)->orderBy('name', 'asc')->first();
            return view('tips.demo.update', compact('part', 'products'));
        }
    }

    public function delete(DemoTips $part, SalesDemoVideo $demo)
    {
        $ids = DemoTips::select('id', 'cid')->where('img_name', $part->img_name)
            ->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['Id'];
            $cid[] = $arr['cid'];
        }
        $demo_ids = SalesDemoVideo::select('id', 'cid')->where('img_name', $part->img_name)
            ->get();   
        $saledemoarray = json_decode(json_encode($demo_ids), true);
        $id1 = [];
        foreach ($saledemoarray as $array) {
            $id1[] = $array['id'];
        }
        DemoTips::whereIn('Id', $id)->delete();
        SalesDemoVideo::whereIn('id', $id1)->delete();
        
       // $userData = SalesDemoVideo::deleteSales($part->Id);
        $this->versionUpdate($cid);
        return redirect()->route('tips-demo')->with('success', 'Demo Tip Deleted');
    }
    public function getDemo(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = DemoTips::select('DemoTips.*', 'Products.name')->join('Products', 'DemoTips.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')
                ->whereIn('cid', $array['country_id'])
               ->get();
            $grouped = $result->groupBy('name');
            $data = view('tips.demo.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = DemoTips::select('DemoTips.*', 'Products.name')->join('Products', 'DemoTips.pid', '=', 'Products.id')
            ->orderBy('Products.name', 'asc')
            ->groupby('DemoTips.Desc')->having('count', '>', 1)->get();
             $grouped = $result->groupBy('name');
            $data = view('tips.demo.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }

    public function deleteImage($demoId)
    {
        $removeVideo = DemoTips::where('id', $demoId)->update(['thumbnail_img' => null, 'img_name' => null]);
            if($removeVideo){
                return json_encode(["success" => "deleted"]);
            }
    }
    
}
