<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contactus;

class ContactUsController extends Controller
{
    public function index()
    {
        $result = Contactus::orderBy('id', 'asc')->orderBy('country_name', 'asc')->get();
        return view('contactus.index', ['result' => $result]);
    }
    public function add(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $part = new Contactus;
            $cid ='';
            if($request->country_name=='Australia'){
                $cid =1;
            }elseif($request->country_name=='New Zealand'){
                $cid = 2;
            }elseif($request->country_name=='United Kingdom'){
                $cid = 3;
            }elseif($request->country_name == 'United States'){
                $cid = 4;
            }
            $part->country_name = $request->country_name;
            $part->country_email = $request->country_email;
            $part->cid = $cid;
            $part->save();
            return redirect()->route('contact-us')->with('success', 'New Email Added');
        } else {
            $products = \App\Models\Contactus::orderBy('id', 'asc')->get();
            return view('contactus.add', compact('products'));
        }
    }
    public function update(Contactus $part, Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $cid ='';
            if($request->country_name=='Australia'){
                $cid =1;
            }elseif($request->country_name=='New Zealand'){
                $cid = 2;
            }elseif($request->country_name=='United Kingdom'){
                $cid = 3;
            }elseif($request->country_name == 'United States'){
                $cid = 4;
            }
            $part->country_name = $request->country_name;
            $part->country_email = $request->country_email;
            $part->cid = $cid;
            $part->save();
            return redirect()->route('contact-us')->with('success', 'Email Update');
        } else {
            $products = \App\Models\Contactus::orderBy('name', 'asc')->get();
            return view('contactus.edit', compact('part', 'products'));
        }
    }
    public function delete(Contactus $part)
    { 
        $id = $part->delete();   
        return redirect()->route('contact-us')->with('success', 'Email Deleted');
    }
    public function getContact(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = Contactus::orderBy('id', 'asc')->orderBy('country_name', 'asc')
            ->whereIn('country_name',$array['country_id'])->get();
            $data = view('contactus.ajax', ['result' =>  $result])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = Contactus::orderBy('id', 'asc')->orderBy('country_name', 'asc')->get();       
            $data = view('contactus.ajax', ['result' => $result])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }
}
