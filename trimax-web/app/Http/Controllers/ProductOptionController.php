<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductOption;

class ProductOptionController extends Controller
{
    public function index()
    {
        $result = ProductOption::select('ProductOption.*', 'Products.name')->join('Products', 'ProductOption.pid', '=', 'Products.id')
            ->orderBy('name', 'asc')
            ->groupBy('ProductOption.pid')
            ->having('count', '>', 1)
            ->get();
        $result = $result->groupBy('name')->toArray();
        return view('product_option.index', compact('result'));
    }
    public function add(Request $request)
    {
       //dd($request->all());
        if ($request->getMethod() == 'POST') {
            if ($request->log_demo) {
                $sale = ProductOption::where('pid', $request->pid)->whereIn('cid',$request->country_id)
                ->update([
                    "log_demo" => 1,
                ]);
               
            }  if ($request->register_warranty) {
                $sale = ProductOption::where('pid', $request->pid)->update([
                    "register_warranty" => 1,
                ]);
            }  if ($request->share_product) {
                
                $sale = ProductOption::where('pid', $request->pid)->update([
                    "share_product" => 1,
                ]);
            } if ($request->sales_demo) {
                $sale = ProductOption::where('pid', $request->pid)->update([
                    "sales_demo" => 1,
                ]);
                
            } if ($request->place_order) {
                $sale = ProductOption::where('pid',$request->pid)->update([
                    "place_order" => 1,
                ]);
            } if ($request->product_feedback) {
                $sale = ProductOption::where('pid', $request->id)->update([
                    "product_feedback" => 0,
                ]);
            }
            $this->versionUpdate($request->country_id);
            session()->flash('success', 'New Product option Added');
            return redirect()->route('option')->with('success', 'New button Sequence Added');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('product_option.add', compact('products'));
        }
    }
    public function delete(ProductOption $general, $text)
    {
        
        if ($text == 'log_demo') {
            $sale = ProductOption::where('pid',$general->pid)->update([
                "log_demo" => 0,
            ]);
        } else if ($text == 'register_warranty') {
            $sale = ProductOption::where('id', $general->id)->update([
                "register_warranty" => 0,
            ]);
        } else if ($text == 'share_product') {
            
            $sale = ProductOption::where('id', $general->id)->update([
                "share_product" => 0,
            ]);
        } elseif ($text == 'sales_demo') {
            $sale = ProductOption::where('id', $general->id)->update([
                "sales_demo" => 0,
            ]);
            
        } elseif ($text == 'place_order') {
            $sale = ProductOption::where('id', $general->id)->update([
                "place_order" => 0,
            ]);
        } elseif ($text == 'product_feedback') {
            $sale = ProductOption::where('id', $general->id)->update([
                "product_feedback" => 0,
            ]);
        }

        return redirect()->route('option')->with('success', 'Product Option Deleted');   
     }
     public function filtercountryButton(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = ProductOption::select('ProductOption.*', 'Products.name')->join('Products', 'ProductOption.pid', '=', 'Products.id')
            ->orderBy('name', 'asc')
            ->whereIn('cid',$array['country_id'])
            ->get();
             $result = $result->groupBy('name')->toArray();
            $data = view('product_option.ajax', ['result' => $result])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = ProductOption::select('ProductOption.*', 'Products.name')->join('Products', 'ProductOption.pid', '=', 'Products.id')
            ->orderBy('name', 'asc')
            ->groupBy('ProductOption.pid')
            ->having('count', '>', 1)
            ->get();
            $result = $result->groupBy('name')->toArray();
            $data = view('product_option.ajax', ['result' => $result])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }
    public function update(ProductOption $general, Request $request)
    {
        if ($request->getMethod() == 'POST') {        
            // $updateArr['cid'] = $request->country_id ;
            if($request->log_demo){
             $updateArr['log_demo'] = ($request->log_demo == '1') ? 1 : NULL;
             $update = ProductOption::where('pid', $general->pid)->update($updateArr);
            } if ($request->register_warranty){
                $updateArr['register_warranty'] = ($request->register_warranty == '2') ? 1 : NULL;
                 $update = ProductOption::where('pid', $general->pid)->update($updateArr);
            }  if ($request->share_product) {
                $updateArr['share_product'] = ($request->share_product == '3') ? 1 : NULL;
                 $update = ProductOption::where('pid', $general->pid)->update($updateArr);
            } if ($request->sales_demo) {
                $updateArr['sales_demo'] = ($request->sales_demo == '4') ? 1 : NULL;
                $update = ProductOption::where('pid', $general->pid)->update($updateArr);
            } if($request->place_order){
                $updateArr['place_order'] = ($request->place_order == '5') ? 1 : NULL;
                $update = ProductOption::where('pid', $general->pid)->update($updateArr);
            } if($request->product_feedback){
                $updateArr['product_feedback'] = ($request->product_feedback == '6') ? 0 : NULL;
                $update = ProductOption::where('pid', $general->pid)->update($updateArr);
            }
             
             $this->versionUpdate($request->country_id);
            session()->flash('success', 'New Product option Added');
            return redirect()->route('option')->with('success', 'Successfully updated');
        } else {
            $products = \App\Models\ProductOption::where('pid', $general->id)->with('product:id,name')->get();
            //dd($products);
            return view('product_option.update', compact('products'));
        }
    }
}
