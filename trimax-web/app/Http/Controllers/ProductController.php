<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Validation\Rules\Exists;
use Storage;
use Illuminate\Support\Facades\DB;
use File;

class ProductController extends Controller
{
    public function index()
    {
        $result = Product::groupBy('category', 'name')->having('count', '>', 1)->get();
        $result = $result->groupBy('category');
        return view('product.index', ['result' => $result]);
    }
    /*
     * product add
     */

    public function add(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $image = $request->file('img');
            $original_images = time() . '.' . $image->getClientOriginalExtension();
            $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $original_images);
            $original = explode(".", $original_image);
            $destinationPath = public_path('uploads/image');
            $image->move($destinationPath, $original_image);
            $image_url = url('/uploads/image') . '/' .  $original_image;
            for ($i = 0; $i < count($request->country); $i++) {
                $product[] = [
                    'name' => $request->name,
                    'category' => $request->category,
                    'country_id' => $request->country[$i],
                    'img_name' => $original_image,
                    'image' => $image_url,
                ];
            }
            $check = Product::where('name', $product[0]['name'])->where('category', $product[0]['category'])->where('country_id', $product[0]['country_id'])->first();
            if($check) {
                return redirect()->route('products')->with('error', 'Product already exists');
            } else {
                Product::insert($product);
                $this->versionUpdate($request->country);
                $this->addVersionLog($image_url, '', $request->country);
                return redirect()->route('products')->with('success', 'New Product Added');  
            }
        } else {
            return view('product.add');
        }
    }
    /*
     * product edit
     */
    public function update(Product $product, Request $request)
    {
        $ids = Product::select('id', 'country_id')->where('img_name', $product->img_name)->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['id'];
            $cid[] = $arr['country_id'];
        }
        if ($request->getMethod() == 'POST') {
            if ($request->hasFile('img')) {
                $image = $request->file('img');
                if (file_exists(public_path('uploads/image') . '/' . $_FILES['img']['name'])) {
                    File::delete(public_path('uploads/image') . '/' . $_FILES['img']['name']);
                    //unlink(public_path('uploads/image') . '/' . $_FILES['img']['name']);
                }
                $pathToStore = public_path('uploads/image');
                $filename = $image->getClientOriginalName();
                $extension = $image -> getClientOriginalExtension();
                $picture = sha1($image . time()) . '.' . $extension;
                $upload_success = $image->move($pathToStore, $picture);
                $image_url = url('/uploads/image') . '/' .  $picture;
            }
        
            Product::where('id', $id)->update([
                'name' => $request->name,
                'image' => $image_url ?? $product->image,
                'category' => $request->category,
                'img_name' => $picture ?? $product->img_name,
            ]);
            $this->versionUpdate($cid);
            $this->addVersionLog($product->image, '', $cid);
            return redirect()->route('products')->with('success', 'Product Update');
        } else {
            return view('product.update', compact('product'));
        }
    }
    /*
     * product delete
     */
    public function delete(Product $product)
    {
        $ids = Product::select('id', 'country_id')->where('img_name', $product->img_name)->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['id'];
            $cid[] = $arr['country_id'];
        }
        $productID = $product->id;
        $productImgname = $product->img_name;
        $str_arr = explode("/", $product->image);
        if (file_exists(public_path('uploads/image') . '/' . $str_arr[7])) {
            unlink(public_path('uploads/image') . '/' . $str_arr[7]);
            $product->dependencyDelete($productID, $productImgname);
        } else {
            dd('File does not exists.');
        }

        $this->versionUpdate($cid);
        return redirect()->route('products')->with('success', 'Product Deleted');
    }
    /*
     * product filter API 
     */
    public function getProduct(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result  = Product::groupBy('category', 'name')->having('count', '>', 1)
                ->whereIn('country_id', $array['country_id'])->get();
            $result = $result->groupBy('category');
            $data = view('product.ajax', ['result' => $result])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = Product::groupBy('category', 'name')->having('count', '>', 1)->get();
            $result = $result->groupBy('category');
            $data = view('product.ajax', ['result' => $result])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }
}
                                                                                                                                