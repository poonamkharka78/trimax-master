<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PartsOrder;
use DB;

class PartsOrderController extends Controller {

    public function index() {
        $result = PartsOrder::select('PartOrder.*', 'Products.name', 'Products.category')->join('Products', 'PartOrder.pid', '=', 'Products.id')
                        ->orderBy('PartOrder.index', 'asc')
                        ->groupby('PartOrder.index')->having('count','>',1)
                        ->get();
        return view('parts_order.index', compact('result'));
    }

    public function add(Request $request) {
         if ($request->getMethod() == 'POST') {
            $part = new PartsOrder;
            $part->pid = $request->pid;
            $part->index = $request->index;
            $part->cid = $request->country_id;
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'index' => $request->index,
                    'cid' => $request->country_id[$i]
                ];
            }
            PartsOrder::insert($product);
            $this->versionUpdate($request->country_id);
            return redirect()->route('partsorders')->with('success', 'New Product Sequence Added');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            $max = \Illuminate\Support\Facades\DB::table('PartOrder')->max('index');
            return view('parts_order.add', compact('products', 'max'));
        }
    }

    public function update(PartsOrder $part, Request $request) {
        $ids = PartsOrder::select('id', 'cid')->where('pid', $part->pid)
        ->where('index',$part->index)->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['id'];
            $cid[] = $arr['cid'];
        }
        if ($request->getMethod() == 'POST') {
            PartsOrder::whereIn('id', $id)->update([
                'pid' => $request->pid,
                'index' => $request->index,
                'cid' => $request->cid,
            ]);
            $this->versionUpdate($cid);
            return redirect()->route('partsorders')->with('success', 'Product Sequence Update');
            
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('parts_order.update', compact('part', 'products'));
        }
    }

    public function delete(PartsOrder $part) {   
        $ids = PartsOrder::select('id', 'cid')->where('pid', $part->pid)
        ->where('index',$part->index)->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['id'];
            $cid[] = $arr['cid'];
        }
        PartsOrder::whereIn('id', $id)->delete();   
        $this->versionUpdate($cid);
        return redirect()->route('partsorders')->with('success', 'Product Sequence Deleted');
    }
    public function getPartsSequence(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = PartsOrder::select('PartOrder.*', 'Products.name', 'Products.category')->join('Products', 'PartOrder.pid', '=', 'Products.id')
            ->orderBy('PartOrder.index', 'asc')
            ->whereIn('PartOrder.cid',$array['country_id'])->get();
          
            $data = view('parts_order.ajax', ['result' =>  $result])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = PartsOrder::select('PartOrder.*', 'Products.name', 'Products.category')->join('Products', 'PartOrder.pid', '=', 'Products.id')
                        ->orderBy('PartOrder.index', 'asc')
                        ->groupby('PartOrder.index')->having('count','>',1)->get();
            $grouped = $result->groupBy('name');
            $data = view('parts_order.ajax', ['result' => $result])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }

}
