<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Parts;

class PartsController extends Controller {

    public function index() {
        $result = Parts::select('PartMaster.*', 'Products.name')->join('Products', 'PartMaster.pid', '=', 'Products.id')
                        ->orderBy('Products.name', 'asc')
                        ->groupBy('PartMaster.part')
                        ->having('count', '>', 1)
                        ->get();
        $grouped = $result->groupBy('name');
        return view('parts.index', ['result' => $grouped->toArray()]);
    }

    public function add(Request $request) {
         if ($request->getMethod() == 'POST') {
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'cid' => $request->country_id[$i],
                    'part' => $request->part,
                ];    
            }
            Parts::insert($product);
            $this->versionUpdate($request->country_id);
            return redirect()->route('parts')->with('success', 'New Part Added');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('parts.add', compact('products'));
        }
    }

    public function update(Parts $part, Request $request) {
        if ($request->getMethod() == 'POST') {
            $ids = Parts::select('id', 'cid')->where('pid', $part->pid)
                ->where('part', $part->part)->get();
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['id'];
                $cid[] = $arr['cid'];
            }
            Parts::whereIn('id', $id)->update([
                'pid' => $request->pid,
                'part' => $request->part,
            ]);
            $this->versionUpdate($cid);
            return redirect()->route('parts')->with('success', 'Part Update');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('parts.update', compact('part', 'products'));
        }
    }

    public function delete(Parts $part) { 
        $ids = Parts::select('id', 'cid')->where('pid', $part->pid)
                ->where('part', $part->part)->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['id'];
            $cid[] = $arr['cid'];
        }
        Parts::whereIn('id', $id)->delete();
      //  $part->dependencyDelete($id);
        $this->versionUpdate($cid);
        return redirect()->route('parts')->with('success', 'Part Deleted');
    }
    public function getParts(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = Parts::select('PartMaster.*', 'Products.name')->join('Products', 'PartMaster.pid', '=', 'Products.id')
            ->orderBy('Products.name', 'asc')
            ->groupBy('PartMaster.part')
            ->having('count', '>', 1)
            ->whereIn('cid',$array['country_id'])->get();
            $grouped = $result->groupBy('name');
            $data = view('parts.ajax', ['result' =>  $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = Parts::select('PartMaster.*', 'Products.name')->join('Products', 'PartMaster.pid', '=', 'Products.id')
            ->orderBy('Products.name', 'asc')
            ->groupBy('PartMaster.part')
            ->having('count', '>', 1)->get();
            $grouped = $result->groupBy('name');
            $data = view('parts.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }

}
