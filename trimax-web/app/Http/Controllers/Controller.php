<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use App\Models\ChangesLogs;
use Carbon\Carbon;
use File;

class Controller extends BaseController
{
    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function versionUpdate($cid)
    {
        DB::table('Version')->whereIn('cid',$cid)->increment('number');
    }

    public function addVersionLog($request, $video_url, $cid)
    {
        
        if (!empty($request)) {
            for ($i = 0; $i < count($cid); $i++) {
                $version[] = [
                    'image' => $request,
                    'video' => $video_url,
                    'cid' => $cid[$i],
                    'time' =>  date('Y-m-d H:i:s'),
                ];
            } 
            ChangesLogs::insert($version);  
        }
    }
    public function DataGet()
    {
        $fetaure = DB::table('features')->select('icon')->get();
        $general_img = DB::table('GeneralImages')->select('img_name')->get();
        $general = DB::table('Generals')->select('images', 'video_url')->get();
        $library = DB::table('Library')->select('img_name', 'video_url')->get();
        $option = DB::table('Options')->select('icon')->get();
        $product = DB::table('Products')->select('image')->get();
        $all_images = [];
        $video1 = [];
        foreach ($fetaure as $key => $value) {
            $all_images[] = $value->icon;
        }
        foreach ($general_img as $key => $img) {
            $all_images[] = $img->img_name;
        }
        foreach ($general as $key => $value) {
            $all_images[] = $value->images;
            $video1[] = $value->video_url;
        }
        foreach ($library as $key => $value) {
            $all_images[] = $value->img_name;
            if ($value->video_url != '') {
                $video1[] = $value->video_url;
            }
        }
        foreach ($option as $key => $value) {
            $images1[] = $value->icon;
        }
        foreach ($product as $key => $value) {
            $all_images[] = $value->image;
        }
        return $all_images;
        return $video1;
    }
    public function upload($request)
    { 
        if($request==null){
            return;
        }
        $original_video = time() . '.' . $request->getClientOriginalExtension();
        $original = explode(".", $original_video);
        $destinationPath = public_path('uploads/videos');
        $request->move($destinationPath, $original_video);
        $image_url = url('/uploads/videos') . '/' .  $original_video;
        return array($original, $image_url, $original_video);
    }
    public function updateVideo($video_name,$request,$id)
    { 
        if($request==null){
            return;
        }
        if (file_exists(public_path('uploads/videos') . '/' . $video_name)) {
            File::delete(public_path('uploads/videos') . '/' . $video_name);
            //unlink(public_path('uploads/videos') . '/' . $video_name);
        }
        $original_video = $video_name;
        $destinationPath = public_path('uploads/videos');
        $request->move($destinationPath, $original_video);
        $video_url = url('/uploads/videos') . '/' .  $original_video;
        return array($video_url, $original_video);
    }
    public function uploadImage($request,$id)
    { 
        if($request==null){
            $value =  DB::table('SalesDemoVideo')->where('demo_id', $id)->get();
            $image_url = isset($value[0]) ? $value[0]->thumbnail_img: null;
            $original_image = isset($value[0]) ? $value[0]->img_name : null;
            return array($original_image, $image_url, $original_image);
        }
        $original_images = time() . '.' . $request->getClientOriginalExtension();
        $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $original_images);
        $original = explode(".", $original_image);
        $destinationPath = public_path('uploads/image');
        $request->move($destinationPath, $original_image);
        $image_url = url('/uploads/image') . '/' .  $original_image;
        return array($original, $image_url, $original_image);
    }
    public function updateImage($request, $image,$id)
    {
       if($image==null){
            return;
        }
        if (file_exists(public_path('uploads/image') . '/' . $request)) {
            File::delete(public_path('uploads/image') . '/' . $request);
            //unlink(public_path('uploads/image') . '/' . $request);
        }
        $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $request);
        $destinationPath = public_path('uploads/image');
        //dd($destinationPath);
        $data = $image->move($destinationPath, $original_image);
        $image_url = url('/uploads/image') . '/' .  $original_image;
        return array($original_image, $image_url, $original_image);
    }
    public function deleteImage($image)
    {
        if (file_exists(public_path('uploads/image') . '/' . $image)) {
            unlink(public_path('uploads/image') . '/' . $image);  
        } else {
            dd('File does not exists.');
        }
    }
}
