<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Options;

class OptionController extends Controller
{

    public function index()
    {
        $result = Options::select('Options.*', 'Products.name')->join('Products', 'Options.pid', '=', 'Products.id')
            ->orderBy('Products.name', 'asc')
            ->orderBy('Options.sequence', 'asc')
            ->groupBy('Options.icon')
            ->having('count', '>', 1)
            ->get();
        $grouped = $result->groupBy('name');
        return view('options.index', ['result' => $grouped->toArray()]);
    }

    public function add(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $option = new Options;
            $image = $request->file('icon');
            $original_images = time() . '.' . $image->getClientOriginalExtension();
            $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $original_images);
            $original = explode(".", $original_image);
            $destinationPath = public_path('uploads/image');
            $image->move($destinationPath, $original_image);
            $image_url = url('/uploads/image') . '/' .  $original_image;
            $option->icon =  $image_url;
            $option->icon_name =  $original_image;
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'title' => $request->title,
                    'desc' => $request->desc,
                    'icon' => $image_url,
                    'sequence' => $request->sequence,
                    'icon_name' => $original_image,
                    'cid' => $request->country_id[$i],
                ];
            }
            Options::insert($product);
            $this->versionUpdate($request->country_id);
            $this->addVersionLog($image_url, '', $request->country_id);
            return redirect()->route('options')->with('success', 'New option Added');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            foreach ($products as $row) {
                $row->max = Options::where('pid', '=', $row->id)->max('sequence');
            }
            return view('options.add', compact('products'));
        }
    }

    public function update(Options $option, Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $ids = Options::select('id', 'cid')->where('icon_name', $option->icon_name)->get();
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['id'];
                $cid[] = $arr['cid'];
            }
            if ($request->hasFile('icon')) {
                $image = $request->file('icon');
                $str_arr = explode("/", $option->icon);
                if (file_exists(public_path('uploads/image') . '/' . $str_arr[7])) {
                    unlink(public_path('uploads/image') . '/' . $str_arr[7]);
                }
                $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $option->icon_name);
                $destinationPath = public_path('uploads/image');
                $image->move($destinationPath, $original_image);
                $image_url = url('/uploads/image') . '/' . $original_image;
            }
            Options::whereIn('id', $id)->update([
                'pid' => $request->pid,
                'title' => $request->title,
                'desc' => $request->desc,
                'icon' => $image_url??$option->icon,
                'sequence' => $request->sequence,
                'icon_name' => $original_image??$option->icon_name,
            ]);

            $this->versionUpdate($cid);
            $this->addVersionLog($option->icon, '', $cid);
            return redirect()->route('options')->with('success', 'Option Update');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('options.update', compact('option', 'products'));
        }
    }

    public function delete(Options $option)
    {
        $ids = Options::select('id', 'cid')->where('icon_name', $option->icon_name)->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['id'];
            $cid[] = $arr['cid'];
        }
        $str_arr = explode("/", $option->icon);
        if (file_exists(public_path('uploads/image') . '/' . $str_arr[7])) {
            unlink(public_path('uploads/image') . '/' . $str_arr[7]);
        } else {
            dd('File does not exists.');
        }
        Options::whereIn('id', $id)->delete();
        $this->versionUpdate($cid);
        return redirect()->route('options')->with('success', 'Option Deleted');
    }
    public function getOption(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = Options::select('Options.*', 'Products.name')->join('Products', 'Options.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')->orderBy('Options.sequence', 'asc')
                ->whereIn('cid', $array['country_id'])
                ->groupBy('Options.icon')
                ->having('count', '>', 1)->get();
            $grouped = $result->groupBy('name');
            $data = view('options.ajax', ['result' =>  $grouped->toArray()])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = Options::select('Options.*', 'Products.name')->join('Products', 'Options.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')->orderBy('Options.sequence', 'asc')
                ->groupBy('Options.icon')
                ->having('count', '>', 1)->get();
            $grouped = $result->groupBy('name');

            $data = view('options.ajax', ['result' => $grouped->toArray()])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }
}
