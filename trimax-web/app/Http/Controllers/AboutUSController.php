<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AboutUs;
use DB;

class AboutUSController extends Controller {

    public function index() {
        $result = DB::table('about_us')->select('*')->get();
        return view('aboutus.index', ['result' => $result]);
    }

    /*public function update(Request $request) {
        $update = [];
        foreach (['title', 'title1', 'title2', 'title3', 'desc', 'desc1', 'desc2', 'desc3'] as $col) {
            if (!empty($request->$col)) {
                $update [$col] = $request->$col;
            }
        }
        if ($request->hasFile('img')) {
            $imgFileData = $request->file('img')->get();
            $imgFile = base64_encode($imgFileData);
            $update['img'] = $imgFile;
        }
        if ($request->hasFile('img1')) {
            $imgFileData = $request->file('img1')->get();
            $imgFile = base64_encode($imgFileData);
            $update['img1'] = $imgFile;
        }
        if ($request->hasFile('img2')) {
            $imgFileData = $request->file('img2')->get();
            $imgFile = base64_encode($imgFileData);
            $update['img2'] = $imgFile;
        }
        if ($request->hasFile('img3')) {
            $imgFileData = $request->file('img3')->get();
            $imgFile = base64_encode($imgFileData);
            $update['img3'] = $imgFile;
        }
        $data = DB::table('AboutUs')->update($update);
        $this->versionUpdate($data);
        return back()->with('success', 'About US Details Update');
    }*/

    public function update(Request $request) 
    {
        $id = $request->id;
        $update = AboutUs::find($id);
        $update->title = $request->title;
        $update->description = $request->description;
        if ($request->hasFile('image')) {
            $imgFileData = $request->file('image')->get();
            $imgFile = base64_encode($imgFileData);
            $update->image = $imgFile;
        }
        $updateData = $update->save();
        
        return back()->with('success', 'About US Details Update');
    }

}
