<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Features;
use File;

class FeaturesController extends Controller {

    public function index() { 
        
        $result = Features::select('Features.*', 'Products.name')->join('Products', 'Features.pid', '=', 'Products.id')->orderBy('Products.name', 'asc')
                ->orderBy('Features.sequence', 'asc')
                //->groupBy('Features.sequence')
                //->having('count','>',1)*/
                ->get();
        $grouped = $result->groupBy('name');
        return view('features.index', ['result' => $grouped->toArray()]);
       
    }

    public function add(Request $request) {
        if ($request->getMethod() == 'POST') {
            $feature = new Features;
            $image = $request->file('icon');
            $original_images = time() . '.' . $image->getClientOriginalExtension();
            $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $original_images);
            $original = explode(".",$original_image);
            $destinationPath = public_path('uploads/image');
            $image->move($destinationPath, $original_image);
            $image_url = url('/uploads/image') . '/' .  $original_image;   
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'title' => $request->title,
                    'desc' => $request->desc,
                    'icon' => $image_url,
                    'sequence' => $request->sequence,
                    'icon_name' => $original_image,
                    'cid' => $request->country_id[$i],
                ];
            }
            Features::insert($product); 
            $this->versionUpdate($request->country_id);
            $this->addVersionLog( $image_url,'',$request->country_id);
            return redirect()->route('features')->with('success', 'New Feature Added');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            foreach ($products as $row) {
                $row->max = Features::where('pid', '=', $row->id)->max('sequence');
            }
            return view('features.add', compact('products'));
        }
    }

    public function update(Features $feature, Request $request) {
        $icon = $feature->icon;
        if ($request->getMethod() == 'POST') {
            $ids = Features::select('id', 'cid')->where('icon_name', $feature->icon_name)->get();
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['id'];
                $cid[] = $arr['cid'];
            }
            if ($request->hasFile('icon')) {
                $image = $request->file('icon');
                $str_arr = explode("/", $feature->icon);
                //dd($_FILES['icon']['name']);
                if (file_exists(public_path('uploads/image') . '/' . $_FILES['icon']['name'])) {
                    File::delete(public_path('uploads/image') . '/' . $_FILES['icon']['name']);
                     //unlink(public_path('uploads/image') . '/' . $_FILES['icon']['name']);
                }
                $destinationPath = public_path('uploads/image');
                $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $feature->icon_name);
                $image->move($destinationPath, $original_image);
                $image_url = url('/uploads/image') . '/' .  $original_image;        
              
            }
            /*Features::whereIn('id', $id)->update([
                'pid' => $request->pid,
                'title' => $request->title,
                'desc' => $request->desc,
                'icon' => $image_url??$feature->icon,
                'sequence' => $request->sequence,
                'icon_name' => $original_image??$feature->icon_name,
            ]);*/
            $update = Features::where('id', $id)->first();
            if (!is_null($update)) {
            $update['pid'] = $request->pid;
            $update['title'] = $request->title;
            $update['desc'] = $request->desc;
            if($request->icon!=null)
            {
                $update['icon'] = $image_url;
            }
            $update['sequence'] = $request->sequence;
           
            if($request->icon_name!=null)
            {
                $update['icon_name'] = $image_url;
            }
             $update['cid'] = $request->cid;
            $update->save();
           }
            
            $this->versionUpdate($cid);
            $this->addVersionLog( $icon,'',$cid);
            return redirect()->route('features')->with('success', 'Feature Update');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('features.update', compact('feature', 'products'));
        }
    }

    public function delete(Features $feature) {
        $ids = Features::select('id', 'cid')->where('icon_name', $feature->icon_name)->get();
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['id'];
                $cid[] = $arr['cid'];
            }
         
        $str_arr = explode ("/", $feature->icon);
        $icon = $feature->icon;
        if(file_exists(public_path('uploads/image').'/'.$feature['icon_name'])) {
            unlink(public_path('uploads/image').'/'.$feature['icon_name']);
        }else{
            Features::whereIn('id',$id)->delete();
            $this->versionUpdate($cid);
            return redirect()->route('features')->with('success', 'Feature Deleted');
        }
    }
    public function getFeature(Request $request) {
      
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = Features::select('Features.*', 'Products.name')->join('Products', 'Features.pid', '=', 'Products.id')->orderBy('Products.name', 'asc')
            ->orderBy('Features.sequence', 'asc')
            ->groupBy('Features.desc')
            ->having('count','>',1)
            ->whereIn('cid',$array['country_id'])
            ->get();
             $grouped = $result->groupBy('name');
            $data = view('features.ajax', ['result' => $grouped->toArray()])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = Features::select('Features.*', 'Products.name')->join('Products', 'Features.pid', '=', 'Products.id')->orderBy('Products.name', 'asc')
            ->orderBy('Features.sequence', 'asc')
            ->groupBy('Features.desc')
            ->having('count','>',1)
            ->get();
             $grouped = $result->groupBy('name');
            $data = view('features.ajax', ['result' => $grouped->toArray()])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }

}
