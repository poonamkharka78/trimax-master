<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Schema;
use App\Models\ChangesLogs;
use App\Models\Features;
use App\Models\Product;
use App\Models\DemoTips;
use Helper;


class UnitedKingdomController extends Controller
{
    public function getUnitedkingdom($time, $cid)
    {
        $version = DB::table('Version')->where('cid', $cid)->value('number');
        $country_email =  DB::table('ContactUs')->where('cid', $cid)->first();
        $db_ext = DB::connection('sqlite14');
        if ($time) {
            app('App\Http\Controllers\Api\ChangesLogController')->trackDatabase($cid);
            $time = str_replace("%20", " ", $time);
            $result = ChangesLogs::select('*')->where('time', '>', $time)
                ->where('cid', $cid)->get();
            $last_row =  DB::table('ChangesLogs')->latest('time')->where('cid', $cid)->first();
            $images = [];
            $video = [];
            foreach ($result as $key => $value) {
                $images[] = $value->image;
                if ($value->video != '') {
                    $video[] = $value->video;
                }
            }
            return array($version, $images, $video, $last_row->time ?? '',$country_email->country_email??'', url('Trimax.db'), url('UK.db'));
        } else {
            app('App\Http\Controllers\Api\ChangesLogController')->trackDatabase($cid);
            $fetaure = $db_ext->table('features')->select('icon')->where('cid', $cid)->get();
            $general_img = $db_ext->table('GeneralImages')->select('img_name')->where('cid', $cid)->get();
            $general =  $db_ext->table('Generals')->select('images', 'video_url')->where('cid', $cid)->get();
            $library = $db_ext->table('Library')->select('img_name', 'video_url')->where('cid', $cid)->get();
            $option = $db_ext->table('Options')->select('icon')->where('cid', $cid)->get();
            $product = $db_ext->table('Products')->select('image')->where('country_id', $cid)->get();
            $service = $db_ext->table('Service')->select('img_name', 'video_url')->where('cid', $cid)->get();
            $sales_video = $db_ext->table('SalesDemoVideo')->select('thumbnail_img', 'video_url')->where('cid', $cid)->get();
            $last_row =  $db_ext->table('ChangesLogs')->latest('time')->first();
            $all_images = [];
            $video1 = [];
            foreach ($fetaure as $key => $value) {
                $all_images[] = $value->icon;
            }
            foreach ($general_img as $key => $img) {
                $all_images[] = $img->img_name;
            }
            foreach ($general as $key => $value) {
                $all_images[] = $value->images;
                $video1[] = $value->video_url;
            }
            foreach ($library as $key => $value) {
                $all_images[] = $value->img_name;
                if ($value->video_url != '') {
                    $video1[] = $value->video_url;
                }
            }
            foreach ($sales_video as $key => $value) {
                $all_images[] = $value->thumbnail_img;
                if ($value->video_url != '') {
                    $video1[] = $value->video_url;
                }
            }
            foreach ($option as $key => $value) {
                $all_images[] = $value->icon;
            }

            foreach ($product as $key => $value) {
                $all_images[] = $value->image;
            }
            foreach ($service as $key => $value) {
                $all_images[] = $value->img_name;
                if ($value->video_url != '') {
                    $video1[] = $value->video_url;
                }
            }
            return array($version, $all_images, $video1, $last_row->time ?? '',$country_email->country_email??'', url('Trimax.db'));
        }
    }
}
