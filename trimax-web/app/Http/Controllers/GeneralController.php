<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Generals;
use App\Models\GeneralImages;
use DB;
use Storage;

class GeneralController extends Controller
{
    
    public function index()
    {
        $result = Generals::select('Generals.*', 'Products.name')->join('Products', 'Generals.pid', '=', 'Products.id')
            ->orderBy('name', 'asc')
            ->groupBy('Generals.desc')
            ->having('count', '>', 1)
            ->get();
        $result = $result->groupBy('name')->toArray();
        return view('generals.index', ['result' => $result]);
    }

    public function add(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $general = new Generals;
            $video_name = $request->file('video')->storeAs('', $this->uniqidReal(15) . '.mp4');
            $image_title = str_replace('tm', '', $request->product_name_collection[$request->pid]);
            $image_title = str_replace('™', '', $image_title);
            $image_title = str_replace('TM', '', $image_title);
            $image = $request->file('thumb');
            $original_images = time() . '.' . $image->getClientOriginalExtension();
            $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $original_images);
            $destinationPath = public_path('uploads/image');
            $image->move($destinationPath, $original_image);
            $image_url = url('/uploads/image') . '/' .  $original_image;
            $video_url = Storage::url($video_name);
            $this->upload($request->video);
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'video_name' => $video_name,
                    'images' => $image_url,
                    'image_title' => $image_title,
                    'desc' => $request->desc,
                    'video_url' => Storage::url($video_name),
                    'is_video_downloaded' => 0,
                    'img_name' => $original_image,
                    'cid' => $request->country_id[$i],
                    'img_name' => $original_image,
                ];
            }
            Generals::insert($product);
            $this->versionUpdate($request->country_id);
            $this->addVersionLog($image_url, $video_url, $request->country_id);
            session()->flash('success', 'New General Added');
            return response()->json(['url' => route('generals')]);
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('generals.add', compact('products'));
        }
    }

    public function update(Generals $general, Request $request)
    {
        if ($request->getMethod() == 'POST') {
           
            $ids = Generals::select('id', 'cid')->where('img_name', $general->img_name)->get();
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['id'];
                $cid[] = $arr['cid'];
            }
            $image = $general->image;
            if ($request->video) {
             $updatevideo =  $this->updateVideo($general->video_name,$request->video,'');
                 $video_url = $updatevideo[0];
                 $video_name = $updatevideo[1];
            }
            if ($request->hasFile('thumb')) {
                $image = $request->file('thumb');
                $str_arr = explode("/", $general->images);
                if (file_exists(public_path('uploads/image') . '/' . $str_arr[5])) {
                    unlink(public_path('uploads/image') . '/' . $str_arr[5]);
                }
                $destinationPath = public_path('uploads/image');
                $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $general->img_name); 
                $image->move($destinationPath, $original_image);
                $image_url = url('/uploads/image') . '/' .  $original_image;
            }
            Generals::whereIn('id', $id)->update([
                'video_name' => $video_name ?? $general->video_name,
                'desc' => $request->desc,
                'video_url' => $video_url ?? $general->video_url,
                'is_video_downloaded' => 0,
                'img_name' => $original_image ?? $general->img_name,
            ]);

            $this->versionUpdate($cid);
            if ($request->hasFile('video')) {
                $this->addVersionLog('', $general->video_url, $cid);
            } elseif ($request->hasFile('thumb')) {
                $this->addVersionLog($image_url, '', $cid);
            } else {
                $this->addVersionLog($general->image, $general->video_url, $cid);
            }
            $general->save();
            session()->flash('success', 'General Update');
            return redirect()->route('generals');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('generals.update', compact('general', 'products'));
        }
    }

    public function delete(Generals $general)
    {
        $ids = Generals::select('id', 'cid')->where('img_name', $general->img_name)->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['id'];
            $cid[] = $arr['cid'];
        }
        $request = $general->images;
        $video_url = $general->video_url;
        @Storage::delete($general->video_name);
        Storage::delete('public/categoryimages/' . $general->image);
        DB::table('Generals')->whereIn('id', $id)->delete();
        $this->versionUpdate($cid);
        return redirect()->route('generals')->with('success', 'General Deleted');
    }

    public function images()
    {
        $result = DB::table('GeneralImages')->select('GeneralImages.*', 'Products.name')
            ->join('Products', 'GeneralImages.pid', '=', 'Products.id')
            ->orderBy('Products.name', 'asc')
            ->groupBy('GeneralImages.img')
            ->having('count', '>', 1)
            ->get();
        $result = $result->groupBy('name');
        return view('generals.images.index', ['result' => $result]);
    }

    public function image_add(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $insertData = ['pid' => $request->pid];
            $image = $request->file('img');
            $original_images = time() . '.' . $image->getClientOriginalExtension();
            $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $original_images);
            $original = explode(".", $original_image);
            $destinationPath = public_path('uploads/image');
            $image->move($destinationPath, $original_image);
            $image_url = url('/uploads/image') . '/' .  $original_image;
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'img_name' => $image_url,
                    'img' => $original_image,
                    'cid' => $request->country_id[$i],
                ];
            }
            DB::table('GeneralImages')->insert($product);
            $this->addVersionLog($image_url, '', $request->country_id);
            return redirect()->route('general-images')->with('success', 'Image Added');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('generals.images.add', compact('products'));
        }
    }

    public function image_delete($id)
    {

        DB::table('GeneralImages')->where('id', $id)->delete();
        return back()->with('success', 'Image Deleted');
    }

    function uniqidReal($lenght = 13)
    {
        // uniqid gives 13 chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($lenght / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        return substr(bin2hex($bytes), 0, $lenght);
    }
    public function getGeneral(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = Generals::select('Generals.*', 'Products.name')->join('Products', 'Generals.pid', '=', 'Products.id')
                ->orderBy('name', 'asc')
                ->groupBy('Generals.desc')
                ->having('count', '>', 1)
                ->whereIn('cid', $array['country_id'])
                ->get();
            $result = $result->groupBy('name')->toArray();
            $data = view('generals.ajax', ['result' => $result])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = Generals::select('Generals.*', 'Products.name')->join('Products', 'Generals.pid', '=', 'Products.id')
                ->orderBy('name', 'asc')
                ->groupBy('Generals.desc')
                ->having('count', '>', 1)
                ->get();
            $result = $result->groupBy('name')->toArray();
            $data = view('generals.ajax', ['result' => $result])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }
    public function getGeneralimages(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = DB::table('GeneralImages')->select('GeneralImages.*', 'Products.name')
                ->join('Products', 'GeneralImages.pid', '=', 'Products.id')->orderBy('Products.name', 'asc') 
                ->groupBy('GeneralImages.img')
                ->having('count', '>', 1)
                ->whereIn('cid', $array['country_id'])
                ->get();
            $result = $result->groupBy('name');
            
            $data = view('generals.images.ajax', ['result' => $result])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = DB::table('GeneralImages')->select('GeneralImages.*', 'Products.name')
                ->join('Products', 'GeneralImages.pid', '=', 'Products.id')->orderBy('Products.name', 'asc')
                ->groupBy('GeneralImages.img')
                ->having('count', '>', 1)
                ->get();
            $result = $result->groupBy('name')->toArray();
            $data = view('generals.images.ajax', ['result' => $result])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }
}
