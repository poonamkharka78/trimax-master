<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Library;
use Storage;

class LibraryController extends Controller
{

    public function index($product_id = 1, $isVideo = 1)
    {
        $result = Library::select('Library.*')
                ->where('Library.pid', '=', $product_id)->where('Library.isVideo', '=', $isVideo)
                ->orderBy('name', 'asc')->orderBy('Library.sequence', 'asc')
                ->groupBy('Library.sequence')
                ->having('count','>',1)
                ->get();
        $products = \App\Models\Product::get();   
        return view('library.index', ['result' => $result, 'products' => $products, 'product_id' => $product_id, 'isVideo' => $isVideo]);
    }

    public function add(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $library = new Library;
            $image = $request->file('img_name');
            $original_images = time() . '.' . $image->getClientOriginalExtension();
            $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $original_images);
            $original = explode(".",$original_image);
            $destinationPath = public_path('uploads/image');
            $image->move($destinationPath, $original_image);
            $image_url = url('/uploads/image') . '/' .  $original_image;   
            if ($request->hasFile('video')) {
                $videoFileName = $this->uniqidReal(15) . '.mp4';
                $request->file('video')->storeAs('', $videoFileName);
                $video_url = Storage::url($videoFileName);
                $isVideo = 1;
            } else {
                $isVideo = 0;
            }
            $library->isVideo_downloaded = 0;
            $library->sequence = $request->sequence;
           
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'img_name' => $image_url,
                    'video_url' => $video_url,
                    'videoFileName' => $videoFileName,
                    'videoPath' => null,
                    'isVideo' => $isVideo,
                    'isVideo_downloaded' => 0,
                    'sequence' => $request->sequence,
                    'img' => $original_image,
                    'cid' => $request->country_id[$i],
                ];
            }
            Library::insert($product); 

            $this->versionUpdate($request->country_id);
            if($request->file('img_name')){    
                $this->addVersionLog($image_url,'',$request->country_id); 
            }elseif($request->hasFile('video')){
                $this->addVersionLog('',Storage::url($videoFileName),$request->country_id);
            }else{
                $this->addVersionLog($image_url,Storage::url($videoFileName),$request->country_id);
            }
          //  $this->addVersionLog( $image_url,Storage::url($videoFileName));
          session()->flash('success', 'New Library Media Added');
          return redirect()->route('library',['product_id' => $request->pid, 'isVideo' => $request->isVideo])->with('success', 'New Demo Tip Added');
           
           // return response()->json(['url' => route('library', ['product_id' => $request->pid, 'isVideo' => $request->isVideo])]);
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            $max = \Illuminate\Support\Facades\DB::table('Library')->where('pid', '=', $request->product_id)->max('sequence');
            return view('library.add', compact('products', 'max'));
        }
    }

    public function update(Library $library, Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $img = $library->img;
            if ($request->hasFile('img_name')) {
                $image = $request->file('img_name');
                $str_arr = explode("/", $library->img_name);  
                if (file_exists(public_path('uploads/image') . '/' . $str_arr[5])) {
                     unlink(public_path('uploads/image') . '/' . $str_arr[5]);
                }
                $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $library->img);
                $destinationPath = public_path('uploads/image');
                $image->move($destinationPath, $original_image);
                $image_url = url('/uploads/image') . '/' .  $original_image;   
                $library->img_name =  $image_url;
            }
            if ($request->hasFile('video')) {
                @Storage::delete($library->videoFileName);
                $videoFileName = $this->uniqidReal(15) . '.mp4';
                $request->file('video')->storeAs('', $videoFileName);
                $video_url = Storage::url($videoFileName);
                $library->videoFileName = $videoFileName;
                $library->videoPath = null;
                $library->isVideo = 1;
            }
            $ids = Library::select('id', 'cid')->where('img', $library->img)->get();
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['id'];
                $cid[] = $arr['cid'];
            }
            /*Library::whereIn('id', $id)->update([
                'pid' => $request->pid,
                'img_name' => $image_url,
                'video_url' => $video_url??$library->video_url,
                'videoFileName' => $videoFileName??$library->videoFileName,
                'sequence' => $request->sequence,
                'img'=> $original_image,
            ]);*/
            $update = Library::where('id', $id)->first();
                        if (!is_null($update)) {
                        $update['pid'] = $request->pid;
                        if($request->img_name!=null)
                        {
                            $update['img_name'] = $image_url;
                            $update['img'] = $original_image??$library->img;
                        }
                        $update['sequence'] = $request->sequence;
                        $update['cid'] = $request->cid;
                       
                        if($request->videoFileName!=null)
                        {
                            $update['videoFileName'] = $videoFileName??$library->videoFileName;
                            $update['video_url'] = $video_url??$library->video_url;
                            $update['isVideo_downloaded'] = $videoDownload ?? $library->isVideo_downloaded;
                        }
                        $update->save();
                     }
            $this->versionUpdate($cid);
            if($request->hasFile('img_name')){    
                $this->addVersionLog($library->image_name,'',$cid); 
            }elseif($request->hasFile('video')){
                $this->addVersionLog('',$library->video_url,$cid);
            }else{
                $this->addVersionLog($library->image_name,$library->video_url,$cid);
            }
            session()->flash('success', 'Library Media Update');
            return response()->json(['url' => route('library', ['product_id' => $library->pid, 'isVideo' => $library->isVideo])]);
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            return view('library.update', compact('library', 'products'));
        }
    }

    public function delete(Library $library)
    {
        $ids = Library::select('id', 'cid')->where('img', $library->img)->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['id'];
            $cid[] = $arr['cid'];
        }
        Library::whereIn('id', $id)->delete();
        @Storage::delete($library->videoFileName);
        $request =$library->img_name . '/' . $library->video_url; 
        $this->versionUpdate($cid);
        return back()->with('success', 'Library Media Deleted');
    }

    
    function uniqidReal($lenght = 13)
    {
        // uniqid gives 13 chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($lenght / 2));
           
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        return substr(bin2hex($bytes), 0, $lenght);
    }
    public function getLibrary(Request $request)
    {
      
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
                $result = Library::select('Library.*')
                ->where('Library.pid', '=', $request->product_id)->where('Library.isVideo', '=', $request->isVideo)
                ->whereIn('cid', $array['country_id'])
                ->orderBy('name', 'asc')->orderBy('Library.sequence', 'asc')
                ->groupBy('Library.sequence')
                ->having('count','>',1)
                ->get();
                 $products = \App\Models\Product::get();
            
            $data = view('library.ajax', ['result' => $result,'products' => $products, 'product_id' => $request->product_id, 'isVideo' => $request->isVideo])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = Library::select('Library.*')
                ->where('Library.pid', '=', $request->product_id)->where('Library.isVideo', '=', $request->isVideo)
                ->orderBy('name', 'asc')->orderBy('Library.sequence', 'asc')
                ->groupBy('Library.sequence')
                ->having('count','>',1)
                ->get();
                $products = \App\Models\Product::get();
            $data = view('library.ajax', ['result' => $result,'products' => $products, 'product_id' => $request->product_id, 'isVideo' => $request->isVideo])->render();

            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }

}
