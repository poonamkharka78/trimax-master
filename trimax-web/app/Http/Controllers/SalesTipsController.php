<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SalesTips;
use App\Models\SalesDemoVideo;
use DB;

class SalesTipsController extends Controller
{

    public function index()
    {
        $result = SalesTips::select('SalesTips.*', 'Products.name')->join('Products', 'SalesTips.pid', '=', 'Products.id')
            ->orderBy('Products.name', 'asc')->orderBy('SalesTips.sequence', 'asc')->get();
            //->groupby('SalesTips.Desc')->having('count', '>', 1);
        $grouped = $result->groupBy('name');
        return view('tips.sales.index', ['result' => $grouped->toArray()]);
    }

    public function add(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $part = new SalesTips;
            $image = $request->file('img_name');
            if (isset($image)) {
                $original_images = time() . '.' . $image->getClientOriginalExtension();
                $original_image = preg_replace("/[^a-z0-9\_\-\.]/i", '', $original_images);
                $original = explode(".", $original_image);
                $destinationPath = public_path('uploads/image');
                $image->move($destinationPath, $original_image);
                $image_url = url('/uploads/image') . '/' .  $original_image;
            }
            
            $req =  $this->upload($request->video);
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'desc' => $request->Desc,
                    'sequence' => $request->sequence,
                    'video_name' => $req[2],
                    'video_url' => $req[1],
                    'video_download' => 0,
                    'thumbnail_img' => isset($image_url) ? $image_url : "",
                    'img_name' => isset($original_image) ? $original_image : "",
                    'cid' => $request->country_id[$i],
                    'video_path' => null,

                ];
            }
            SalesTips::insert($product);
            for ($j=0; $j < count($request->country_id); $j++) { 
                $saledemo[] = [
                    'pid' => $request->pid,
                    'video_url' => $req[1],
                    'video_name' => $req[2],
                    'video_download' => 0,
                    'type' => 0,
                    'sales_id' => DB::getPDO()->lastInsertId(),
                    'thumbnail_img' => isset($image_url) ? $image_url : "",
                    'img_name' => isset($original_image) ? $original_image : "",
                    'cid' => $request->country_id[$j],
                    'video_path' => null,

                ];
            }
            SalesDemoVideo::insert($saledemo);
            $this->versionUpdate($request->country_id);
            return redirect()->route('tips-sales')->with('success', 'New Sales Tip Added');
        } else {
            $products = \App\Models\Product::with('salesTips')->orderBy('name', 'asc')->get();
            foreach ($products as $row) {
                $row->max = SalesTips::where('pid', '=', $row->id)->max('sequence');
            }
            //dd($products);
            return view('tips.sales.add', compact('products'));
        }
    }

    public function update(SalesTips $part, SalesDemoVideo $demo, Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $image = $request->file('img_name');
            $value =  DB::table('SalesDemoVideo')->where('demo_id', $part->Id)->get();
            $ids = SalesTips::select('id', 'cid')->where('img_name', $part->img_name)
                ->get();
            $uploadimage =  $this->updateImage($request->img_name, $image, $part->Id);
            $uploadvideo =  $this->updateVideo($request->video_name, $request->video, $part->Id);
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['Id'];
                $cid[] = $arr['cid'];
            }
            SalesTips::where('id', $part->Id)->update([
                'pid' => $request->pid,
                'Desc' => $request->Desc,
                'video_name' => empty(isset($request->video_name)) ? "" : $uploadvideo[1]??$part->video_name,
                'video_url' => empty(isset($request->video_name)) ? "" : $uploadvideo[0]??$part->video_url,
                'thumbnail_img' => empty(isset($request->img_name)) ? " " : $uploadimage[1]??$part->thumbnail_img, 
                'img_name' => empty(isset($request->img_name)) ? " " : $uploadimage[2]??$part->img_name,
            ]);
            $demo_ids = SalesDemoVideo::select('id', 'cid')->where('img_name', $part->img_name)
                ->get();
            $saledemoarray = json_decode(json_encode($demo_ids), true);
            $id1 = [];
            foreach ($saledemoarray as $array) {
                $id1[] = $array['id'];
            }
            $country_id = SalesTips::where('id', $part->Id)->first();
            $find = SalesDemoVideo::where('sales_id', $part->Id)->first();
            if ($find) {
                     SalesDemoVideo::whereIn('id', $id)->update([
                    'pid' => $request->pid,
                    'video_name' => $uploadvideo[1]??$demo->video_name,
                    'video_url' => $uploadvideo[0]??$demo->video_url,
                    'thumbnail_img' => $uploadimage[1]??$demo->thumbnail_img,
                    'img_name' => $uploadimage[2]??$demo->img_name
                ]);
            } else {
                $saledemo[] = [
                    'pid' => $request->pid,
                    'video_url' => $uploadvideo[1],
                    'video_name' => $uploadvideo[2],
                    'video_download' => 0,
                    'type' => 1,
                    'sales_id' => $part->Id,
                    'thumbnail_img' => $uploadimage[1],
                    'img_name' => $uploadimage[2],
                    'cid' => $country_id->cid,
                    'video_path' => null,

                ];
                SalesDemoVideo::insert($saledemo);
            }
            
            $this->versionUpdate($cid);
            return redirect()->route('tips-sales')->with('success', 'Sales Tip Update');
        } else {
            $products = \App\Models\SalesTips::with('product')->where('id',$part->Id)->orderBy('name', 'asc')->first();
            return view('tips.sales.update', compact('part', 'products'));
        }
    }

    public function delete(SalesTips $part)
    {
        $ids = SalesTips::select('id', 'cid')->where('img_name', $part->img_name)
            ->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['Id'];
            $cid[] = $arr['cid'];
        }
        $demo_ids = SalesDemoVideo::select('id', 'cid')->where('img_name', $part->img_name)
            ->get();
        $saledemoarray = json_decode(json_encode($demo_ids), true);
        $id1 = [];
        foreach ($saledemoarray as $array) {
            $id1[] = $array['id'];
        }
        SalesTips::whereIn('Id', $id)->delete();
        SalesDemoVideo::whereIn('id', $id1)->delete();

        $this->versionUpdate($cid);
        return redirect()->route('tips-sales')->with('success', 'Sales Tip Deleted');
    }
    public function getSales(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = SalesTips::select('SalesTips.*', 'Products.name')->join('Products', 'SalesTips.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')->orderBy('SalesTips.sequence', 'asc')
                ->whereIn('cid', $array['country_id'])->get();
            $grouped = $result->groupBy('name');
            $data = view('tips.sales.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = SalesTips::select('SalesTips.*', 'Products.name')->join('Products', 'SalesTips.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')->orderBy('SalesTips.sequence', 'asc')->get();
            $grouped = $result->groupBy('name');
            $data = view('tips.sales.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }
}
