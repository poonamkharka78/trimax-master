<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PartPrice;

class PartsPriceController extends Controller
{

    public function index()
    {
        $result = PartPrice::select('PartPrice.*', 'Products.name', 'PartMaster.part')
            ->join('Products', 'PartPrice.pid', '=', 'Products.id')
            ->join('PartMaster', 'PartPrice.prid', '=', 'PartMaster.id')
            ->orderBy('Products.name', 'asc')->orderBy('PartMaster.part', 'asc')
            ->orderBy('PartPrice.sequence', 'asc')
            ->groupBy('PartPrice.sequence')
            ->having('count', '>', 1)->get();
        $grouped = $result->groupBy('name');
        return view('parts_price.index', ['result' => $grouped->toArray()]);
    }

    public function add(Request $request)
    {
        $price = $request->currency . '' . $request->desc;
        if ($request->getMethod() == 'POST') {
            for ($i = 0; $i < count($request->country_id); $i++) {
                $product[] = [
                    'pid' => $request->pid,
                    'cid' => $request->country_id[$i],
                    'prid' => $request->part[$request->pid],
                    'title' => $request->title,
                    'desc' => $request->desc,
                    'sequence' => $request->sequence,
                ];
               
            }
            PartPrice::insert($product);
            $this->versionUpdate($request->country_id);
            return redirect()->route('partsprice')->with('success', 'New Part Price Added');
        } else {
            $result = \App\Models\Parts::select('PartMaster.*', 'Products.name')->join('Products', 'PartMaster.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')->get();
            $result = $result->groupBy('name');
            foreach ($result as $products) {
                foreach ($products as $part) {
                    $part->max = PartPrice::where('prid', '=', $part->id)->max('sequence');
                }
            }
            return view('parts_price.add', compact('products', 'result'));
        }
    }

    public function update(PartPrice $part, Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $ids = PartPrice::select('id', 'cid')->where('pid', $part->pid)
                ->where('prid', $part->prid)->where('title', $part->title)->get();
            $array = json_decode(json_encode($ids), true);
            $id = [];
            $cid = [];
            foreach ($array as $arr) {
                $id[] = $arr['id'];
                $cid[] = $arr['cid'];
            }
            PartPrice::whereIn('id', $id)->update([
                'pid' => $request->pid,
                'prid' => $request->prid,
                'title' => $request->title,
                'desc' => $request->desc,
                'sequence' => $request->sequence,
            ]);
            $this->versionUpdate($cid);
            return redirect()->route('partsprice')->with('success', 'Part Price Update');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            $result = \App\Models\Parts::select('PartMaster.*', 'Products.name')->join('Products', 'PartMaster.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')->get();
            $result = $result->groupBy('name');
            return view('parts_price.update', compact('part', 'products', 'result'));
        }
    }

    public function delete(PartPrice $part)
    {
        $ids = PartPrice::select('id', 'cid')->where('pid', $part->pid)
                ->where('prid', $part->prid)->where('title', $part->title)->get();
        $array = json_decode(json_encode($ids), true);
        $id = [];
        $cid = [];
        foreach ($array as $arr) {
            $id[] = $arr['id'];
            $cid[] = $arr['cid'];
        }
        PartPrice::whereIn('id', $id)->delete();
        $this->versionUpdate($cid);
        return redirect()->route('partsprice')->with('success', 'Part Price Deleted');
    }
    public function getPrice(Request $request)
    {
        $array = $request->all();
        if (array_key_exists('country_id', $array)) {
            $result = PartPrice::select('PartPrice.*', 'Products.name', 'PartMaster.part')
                ->join('Products', 'PartPrice.pid', '=', 'Products.id')
                ->join('PartMaster', 'PartPrice.prid', '=', 'PartMaster.id')
                ->orderBy('Products.name', 'asc')->orderBy('PartMaster.part', 'asc')
                ->orderBy('PartPrice.sequence', 'asc')
                ->whereIn('PartPrice.cid', $array['country_id'])->get();
            $grouped = $result->groupBy('name');
            $data = view('parts_price.ajax', ['result' =>  $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        } else {
            $result = PartPrice::select('PartPrice.*', 'Products.name', 'PartMaster.part')
                ->join('Products', 'PartPrice.pid', '=', 'Products.id')
                ->join('PartMaster', 'PartPrice.prid', '=', 'PartMaster.id')
                ->orderBy('Products.name', 'asc')->orderBy('PartMaster.part', 'asc')
                ->orderBy('PartPrice.sequence', 'asc')->get();
            $grouped = $result->groupBy('name');
            $data = view('parts_price.ajax', ['result' => $grouped])->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
    }

    public function getdataCountry(Request $request)
    {
       
        if ($request->getMethod() == 'POST') {
            $part = new PartPrice();
            $part->pid = $request->pid;
            $part->prid = $request->part[$request->pid];
            $part->title = $request->title;
            $part->cid = $request->country;
            $part->desc = $request->desc;
            $part->sequence = $request->sequence;
           // $part->save();
            $this->versionUpdate($request->country);
            return redirect()->route('partsprice')->with('success', 'New Part Price Added');
        } else {
            $products = \App\Models\Product::orderBy('name', 'asc')->get();
            $result = \App\Models\Parts::select('PartMaster.*', 'Products.name')->join('Products', 'PartMaster.pid', '=', 'Products.id')
                ->orderBy('Products.name', 'asc')->get();
            $result = $result->groupBy('name');    
            $data = view('parts_price.addcountry', compact('part', 'products', 'result'))->render();
            // $data = view('parts_price.addcountry')->render();
            return response()->json(["data" => $data, "status" => "200", "message" => "Successfully"]);
        }
        
    }
}
