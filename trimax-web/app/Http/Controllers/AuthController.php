<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller {

    public function login(Request $request) {
        if ($request->getMethod() == 'POST') {
            if ($request->username === 'trimaxappadmin' && $request->password === 'trimax@687#4223') {
                session(['login' => true]);
                return redirect('/')->with('success', 'Welcome Admin');
            } else {
                return redirect('login')->with('error', 'Invalid Login');
            }
        } else {
            return view('auth.login');
        }
    }

    public function logout(Request $request) {
        session(['login' => false]);
        return redirect('/login');
    }

}
