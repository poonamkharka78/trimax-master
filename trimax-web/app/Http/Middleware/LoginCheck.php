<?php

namespace App\Http\Middleware;

use Closure;

class LoginCheck {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (session('login') != true) {
            return redirect('/login');
        }

        return $next($request);
    }

}
