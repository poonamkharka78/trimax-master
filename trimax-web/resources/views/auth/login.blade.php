<!DOCTYPE html>
<html>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Login :: Trimax App Backend</title>
        <style>
            .border {
                border: 1px solid lightgray;
            }
        </style>
    </head>

    <body>
        <header>
            <nav class="red lighten-1" role="navigation">
                <div class="nav-wrapper container center-align">
                    <a id="logo-container" href="#" style="font-size: 2em;">Trimax App Backend</a>
                    <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                </div>
            </nav>
        </header>
        <main>
            <div class="section">
                <div class="container">
                    @if(session('error'))<div class="card-panel red white-text darken-4" style="padding: 15px;margin: 0">{!! session('error') !!}</div>@endif
                    <form action="{!! url('login') !!}" method="post">
                        @csrf
                        <div class="input-field">
                            <input type="text" id="textarea1" name="username" class="" required="">
                            <label for="textarea1">Username</label>
                        </div>
                        <div class="input-field">
                            <input type="password" id="textarea2" name="password" class="" required="">
                            <label for="textarea2">Password</label>
                        </div>
                        <p><button class="btn waves-effect waves-light" type="submit" name="action">Login</button></p>
                    </form>
                </div>
            </div>
        </main>
        <!-- Compiled and minified JavaScript -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.sidenav').sidenav();
                $('.materialboxed').materialbox();
                $('select').formSelect();
                $('.modal').modal();
                $('.fixed-action-btn').floatingActionButton();
            });
        </script>

    </body>
</html>