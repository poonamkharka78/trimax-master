@extends('layouts.ajax')
@if(count($result)!=0)
<div class="abc1">
    @foreach($result as $category => $product1)
    <div class="col s12">
        <h1>{{$category == 1 ? 'Rotary Mowers' : 'Flail Mowers'}}</h1>
    </div>
    <div class="row ">
        @foreach($product1 as $product)
        <div class="col s12 m4 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="{{$product->image}}" class="materialboxed" style="height: 300px">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="padding-bottom: .5em">{{$product->name}}</p>
                        <p>{{$product->category == 1 ? 'Rotary Mowers' : 'Flail Mowers'}}</p>
                    </div>
                    <div class="card-action" style="padding-top: 0;border-top:0">
                        <a class="btn waves-effect waves-light" href="{!! route('product-update',['product'=>$product->id]) !!}">Edit</a>
                        <a class="btn red lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!! $product->id !!}">Delete</a>
                        <div id="modal_{!! $product->id !!}" class="modal bottom-sheet">
                            <div class="modal-content">
                                <h4 class="center-align">Delete {{$product->name}}?</h4>
                                <p class="center-align">It also deletes the related information</p>
                            </div>
                            <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('product-delete',['product'=>$product->id]) !!}">Yes, Delete It</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endforeach
</div>
@else
<tr>
    <td class="center-align" style="font-size: 50px;">No Records Found</td>
</tr>
@endif

<script>
    $(document).ready(function () {
        $('.modal').modal({
            dismissible: true
        });
        $('.dropdown-trigger').dropdown();
        $('.scrollspy').scrollSpy();
    });
</script>