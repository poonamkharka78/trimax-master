@extends('layouts.admin')
@section('content_header')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<style>
    .select2-container {
        width: 100% !important;
    }
</style>
@stop
@section('title','New Product')
@section('content')
<div class="row">

    <div class="col s12">
        <h3>New Product</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input-field ">
            <input id="title" type="text" name="name" value="" required="">
            <label for="title">Name</label>
        </div>
        <div class="input-field ">
            <select name="category">
                <option value="1">Rotary Mowers</option>
                <option value="2">Flail Mowers</option>
            </select>
            <label>Category</label>
        </div>
        <div class="input-field ">
            <select name="country[] " multiple="multiple" class="js-example-basic-multiple">
                <option value="1">Australia</option>
                <option value="2">New Zealand</option>
                <option value="3">United Kingdom</option>
                <option value="4">United States</option>
            </select>
            <label>Country</label>
        </div>

        <div class="file-field input-field">
            <div class="btn">
                <span>Image</span>
                <input type="file" name="img" accept="image/*" required="">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text">
            </div>
            <p><button class="btn waves-effect waves-light" type="submit" name="action">Add Product</button></p>
        </div>
    </form>
</div>
@endsection
@push('footer')


@endpush