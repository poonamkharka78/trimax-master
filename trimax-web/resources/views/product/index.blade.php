@extends('layouts.admin')
@section('title','Products')
@section('content')
<div class="row " id="ab1">
    <div class="col s12">
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href="{!! route('product-add') !!}">
                <i class="large material-icons">add</i>
            </a>
        </div>
    </div>
    <div id="check1">
        <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="1">Australia</label>
        <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="2">New Zealand</label>
        <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="3">United Kingdom</label>
        <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="4">United States</label>
    </div>
    <div>
        <div class="abc1">
            @foreach($result as $category => $product1)
            <div class="col s12">
                <h1>{{$category == 1 ? 'Rotary Mowers' : 'Flail Mowers'}}</h1>
            </div>
            <div class="row ">
                @foreach($product1 as $product)
                <div class="col s12 m4 l4">
                    <div class="card hoverable">
                        <div class="card-image">
                            <img src="{{$product->image}}" class="materialboxed" style="height: 300px">
                        </div>
                        <div class="card-stacked">
                            <div class="card-content">
                                <p style="padding-bottom: .5em">{{$product->name}}</p>
                                <p>{{$product->category == 1 ? 'Rotary Mowers' : 'Flail Mowers'}}</p>
                            </div>
                            <div class="card-action" style="padding-top: 0;border-top:0">
                                <a class="btn waves-effect waves-light" href="{!! route('product-update',['product'=>$product->id]) !!}">Edit</a>
                                <a class="btn red lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!! $product->id !!}">Delete</a>
                                <div id="modal_{!! $product->id !!}" class="modal bottom-sheet">
                                    <div class="modal-content">
                                        <h4 class="center-align">Delete {{$product->name}}?</h4>
                                        <p class="center-align">It also deletes the related information</p>
                                    </div>
                                    <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                        <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                        <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('product-delete',['product'=>$product->id]) !!}">Yes, Delete It</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>
    @endsection
    @section("js")
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#check1").click(function() {
                var country_id = [];
                $.each($("input[name='country']:checked"), function() {
                    country_id.push($(this).val());
                });
                data = {
                    _token: "{{csrf_token()}}",
                    'country_id': country_id
                };
                url = "{{ url('getproduct') }}";
                $.post(url, data, function(response) {
                    $(".abc1").html(response.data)

                })
            });
        });
    </script>