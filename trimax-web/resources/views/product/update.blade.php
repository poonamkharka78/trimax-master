@extends('layouts.admin')
@section('title','Update Product')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>Update Product</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input-field ">
            <input id="title" type="text" name="name" value="{{$product->name}}" required="">
            <label for="title">Name</label>
        </div>
        <div class="input-field ">
            <select name="category">
                <option value="1" @if($product->category == 1) selected @endif>Rotary Mowers</option>
                <option value="2" @if($product->category == 2) selected @endif>Flail Mowers</option>
            </select>
            <label>Category</label>
        </div>
       
        <!-- <div class="input-field ">
            <select name="country_id">
                <option value="1" @if($product->country_id == 1) selected @endif>Australia</option>
                <option value="2" @if($product->country_id == 2) selected @endif>New Zealand</option>
                <option value="3" @if($product->country_id == 3) selected @endif>United Kingdom</option>
                <option value="4" @if($product->country_id == 4) selected @endif>United States</option>

            </select>
            <label>Country</label>
        </div> -->
        <div class="row">
            <div class="file-field input-field col s6">
                <div class="btn">
                    <span>Image</span>
                    <input type="file" name="img" accept="image/*">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path" type="text">
                </div>
                <p><button class="btn waves-effect waves-light" type="submit" name="action">Update Product</button></p>
            </div>
            <div class="col s6">
                <img src="{{$product->image}}" class="responsive-img"/>
            </div>
        </div>
    </form>
</div>
@endsection
@push('footer')
<script>

</script>
@endpush