@extends('layouts.admin')
@section('title','Update Part Details')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>Update Part Details</h3>
    </div>
    <form class="col s12" id="partprice1" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
       
        <div class="input-field ">
            <select name="pid">
                @foreach($products as $item)
                <option value="{{$item->id}}" @if($part->pid == $item->id) selected @endif>{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        <div class="input-field ">
            <select name="prid">
                @foreach($result as $productName => $item)
                <optgroup label="{{$productName}}">
                    @foreach($item as $value)
                    <option value="{{$value->id}}" @if($part->prid == $value->id) selected @endif>{{$value->part}}</option>
                    @endforeach
                </optgroup>
                @endforeach
            </select>
            <label>Part</label>
        </div>
        <div class="input-field ">
            <input id="title" type="text" name="part_no" value="{{$part->part_no}}" required="">
            <label for="title">Part No</label>
        </div>
        <div class="input-field">
            <textarea id="textarea1" name="desc" class="materialize-textarea">{{$part->desc}}</textarea>
            <label for="textarea1">Description</label>
        </div>
        <div class="input-field ">
            <input id="title" type="text" name="price" value="{{$part->price}}" required="">
            <label for="title">Price</label>
        </div>
        <div class="input-field ">
            <input id="title" type="text" name="req" value="{{$part->req}}" required="">
            <label for="title">Req.</label>
        </div>
        <p><button class="btn waves-effect waves-light" type="submit" name="action">Update Part Details</button></p>
    </form>
</div>
@endsection
@section("js")
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

<script>
    $(function() {
        $("#country").change(function() {
            var country_id = $('select[name=country_id]').val()
            var pid = $('select[name=pid]').val()
            var prid = $('select[name=prid]').val()
            var url =$(location).attr('href');
            parts = url.split("/"),
            last_part = parts[parts.length-1];
            // alert(last_part);
            if (country_id) {
                data = {
                    _token: "{{csrf_token()}}",
                    'country_id': country_id,
                    'prid':prid,
                    'pid':pid,
                };
                url = "{{ url('getupdatedata') }}";
                $.post(url, data, function(response) {

                    $("#partprice1").html(response.data)
                })
            }

        });
    });
</script>