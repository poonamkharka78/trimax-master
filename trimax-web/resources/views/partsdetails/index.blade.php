@extends('layouts.admin')
@section('title','Parts Details')
@section('content')
<div class="fixed-action-btn">
    <a class="btn-floating btn-large red" href="{!! route('partsdetail-add') !!}">
        <i class="large material-icons">add</i>
    </a>
</div>
<div id="check1">
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="1">Australia</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="2">New Zealand</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="3">United Kingdom</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="4">United States</label>
</div>
<div class="row" id ="partdetail">
    <div class="col s12 m9 l10">
        @foreach($result as $productName => $collection)
        <div id="{{snake_case($productName)}}" class="section scrollspy">
            <div style="overflow: auto">
                <table class="striped highlight">
                    <thead>
                        <tr>
                            <th class="border">#</th>
                            <th class="border">Product</th>
                            <th class="border">Part</th>
                            <th class="border">Part No</th>
                            <th class="border">Description</th>
                            <th class="border">Price</th>
                            <th class="border">REQ</th>
                            <th class="border">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($collection as $index => $feature)
                        <tr>
                            <td class="border">{!! $index + 1 !!}</td>
                            <td class="border">{{$feature['name']}}</td>
                            <td class="border">{{$feature['part']}}</td>
                            <td class="border">{{$feature['part_no']}}</td>
                            <td class="border">{{$feature['desc']}}</td>
                            <td class="border">{{$feature['price']}}</td>
                            <td class="border">{{$feature['req']}}</td>
                            <td class="border">
                                <a class='dropdown-trigger btn' href='#' data-target="dropdown{!! $feature['id'] !!}">Action</a>
                                <ul id="dropdown{!! $feature['id'] !!}" class='dropdown-content'>
                                    <li><a class=" waves-effect waves-light" href="{!! route('partsdetail-update',['part'=>$feature['id']]) !!}">Edit</a></li>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" red-text lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!!$feature['id'] !!}">Delete</a></li>
                                </ul>
                                <div id="modal_{!! $feature['id'] !!}" class="modal bottom-sheet">
                                    <div class="modal-content">
                                        <h4 class="center-align">Delete {{$feature['name']}} - {{$feature['part']}} - {{$feature['part_no']}}?</h4>
                                    </div>
                                    <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                        <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                        <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('partsdetail-delete',['part'=>$feature['id']]) !!}">Yes, Delete It</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endforeach
    </div>
    <div class="col hide-on-small-only m3 l2">
        <ul class="section table-of-contents">
            @foreach($result as $productName => $collection)
            <li style="padding: 1em"><a href="#{{snake_case($productName)}}">{{$productName}}</a></li>
            @endforeach
        </ul>
    </div>
</div>
@endsection
@push('footer')
<script>
    $(document).ready(function () {
     
            $("#check1").click(function() {
                var country_id = [];
                $.each($("input[name='country']:checked"), function() {
                    country_id.push($(this).val());
                });
                data = {
                    _token: "{{csrf_token()}}",
                    'country_id': country_id,
                    
                };
                url = "{{ url('getpartsdetail') }}";
                $.post(url, data, function(response) {
                    $("#partdetail").html(response.data)

                })
            });
       
        $('.dropdown-trigger').dropdown();
        $('.scrollspy').scrollSpy();
    });
    
</script>
@endpush