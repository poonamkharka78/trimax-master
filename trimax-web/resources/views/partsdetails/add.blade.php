@extends('layouts.admin')
@section('title','New Part Details')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>New Part Details</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <!-- <div id="check1" class="check" name="country_id">
            <label class="checkbox-label"><input type="radio"  id="check" name="country_id" style="opacity: 13;position: inherit;" value="1">Australia</label>
            <label class="checkbox-label"><input type="radio"  id="check" name="country_id" style="opacity: 13;position: inherit;" value="2">New Zealand</label>
            <label class="checkbox-label"><input type="radio"  id="check" name="country_id" style="opacity: 13;position: inherit;" value="3">United Kingdom</label>
            <label class="checkbox-label"><input type="radio"  id="check" name="country_id" style="opacity: 13;position: inherit;" value="4">United States</label>
        </div> -->
        <div class="input-field ">
            <select name="pid" id="pid">
                @foreach($products as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        <div class="input-field ">
            <select name="prid" id="prid"></select>
            <label>Part</label>
        </div>
        <div class="input-field ">
            <select name="country_id[] " multiple="multiple" class="js-example-basic-multiple">
                <option value="1">Australia</option>
                <option value="2">New Zealand</option>
                <option value="3">United Kingdom</option>
                <option value="4">United States</option>
            </select>
            <label>Country</label>
        </div>

        <div class="input-field ">
            <input id="title" type="text" name="part_no" value="" required="">
            <label for="title">Part No</label>
        </div>
        <div class="input-field">
            <textarea id="textarea1" name="desc" class="materialize-textarea"></textarea>
            <label for="textarea1">Description</label>
        </div>
        <div class="input-field ">
            <input id="title1" type="text" name="price" value="" required="">
            <label for="title">Price</label>
        </div>
        <!-- <div class="input-field ">
            <input id="currency" type="text" name="currency" value="" required="">
            <label for="title">Currency</label>
        </div> -->
        <div class="input-field ">
            <input id="title" type="text" name="req" value="" required="">
            <label for="title">Req.</label>
        </div>
        <p><button class="btn waves-effect waves-light" id = "sub"  name="action">Add Part Details</button></p>
    </form>
</div>
@endsection
@section("js")
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>

$(document).ready(function(){
        var pro_id = $( "#pid" ).val();
        if(pro_id) {
                $.ajax({
                    url: '/myform/ajax/'+pro_id,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('#prid').html("");
                        $.each(data, function(key, value) {
                             console.log(data.length);
                                $('#prid').formSelect().append('<option value="'+ value.id +'">'+ value.part +'</option>')
                        });
                        $('#prid').formSelect();
                    }
                });
            } else {
                $('#prid').html("");
            }
    $('#check1 input[type=radio]').change(function () {
       var selectedCountry  = $(this).val();
        if (selectedCountry == 1) {
            $('#currency').val("A$");
        }
        if (selectedCountry == 2) {
            $('#currency').val("$");
        }
        if (selectedCountry == 3) {
            $('#currency').val("£");
        }
        if (selectedCountry == 4) {
            $('#currency').val("$");
        }

    });


    $('#pid').change(function() {
            var pid = $(this).val();
            if(pid) {
                $.ajax({
                    url: '/myform/ajax/'+pid,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('#prid').html("");
                        $.each(data, function(key, value) {
                             console.log(data.length);
                                $('#prid').formSelect().append('<option value="'+ value.id +'">'+ value.part +'</option>')
                        });
                        $('#prid').formSelect();
                    }
                });
            } else {
                $('#prid').html("");
            }
        });
});
</script>