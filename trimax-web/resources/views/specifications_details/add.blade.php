@extends('layouts.admin')
@section('title','New Specification Details')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>New Specification Details</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <!-- <div id="check1" class="check" name="country_id">
            <label class="checkbox-label"><input type="radio"  id="check" name="country_id" style="opacity: 13;position: inherit;" value="1">Australia</label>
            <label class="checkbox-label"><input type="radio"  id="check" name="country_id" style="opacity: 13;position: inherit;" value="2">New Zealand</label>
            <label class="checkbox-label"><input type="radio"  id="check" name="country_id" style="opacity: 13;position: inherit;" value="3">United Kingdom</label>
            <label class="checkbox-label"><input type="radio"  id="check" name="country_id" style="opacity: 13;position: inherit;" value="4">United States</label>
        </div> -->
        <div class="input-field ">
            <select name="pid" class="" id="productSelect">
                @foreach($result as $productName => $item)
                @if($loop->first)
                @php $keyName = $productName; @endphp
                @endif
                <option value="{{$item[0]->pid}}">{{$productName}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        @foreach($result as $productName => $item)
        <div class="input-field modelDiv" id="mdl_ddl_{{$item[0]->pid}}" @if(!$loop->first)style="display: none;"@endif>
             <select name="model[{{$item[0]->pid}}]" class="modelSelect">
                @foreach($item as $value)
                <option value="{{$value->id}}" data-max="{{$value->max}}">{{$value->model}}</option>
                @endforeach
            </select>
            <label>{{$productName}} - Model</label>
        </div>
        @endforeach
        <div class="input-field ">
            <select name="country_id[] " multiple="multiple" class="js-example-basic-multiple">
                <option value="1">Australia</option>
                <option value="2">New Zealand</option>
                <option value="3">United Kingdom</option>
                <option value="4">United States</option>
            </select>
            <label>Country</label>
        </div>
        <div class="input-field">
            <input id="title" type="text" name="title" value="" required="">
            <label for="title">Title</label>
        </div>
        <div class="input-field">
            <textarea id="textarea1" name="desc" class="materialize-textarea"></textarea>
            <label for="textarea1">Description</label>
        </div>
        <!-- <div class="input-field ">
            <input id="metrics" type="text" name="metrice" value="" required="">
            <label for="title">Metrics</label>
        </div> -->
        <div class="input-field">
            <input id="title" type="text" name="sequence" value="{!! $result[$keyName][0]->max + 1 !!}" required="">
            <label for="title">Sequence</label>
        </div>
        <p><button class="btn waves-effect waves-light" type="submit" name="action">Add Specification Details</button></p>
    </form>
</div>
@endsection
@push('footer')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script>
   

    $("#productSelect").on('change', function () {
        $(".modelDiv").hide();
        let pid = $(this).val();
        let selector = "#mdl_ddl_" + pid;
        $(selector).show();
        setSequence(selector + " select");
    });
    $(".modelSelect").on('change', function () {
        setSequence($(this));
    });
    function setSequence(selector) {
        let value = $(selector).find(':selected').data('max');
        $("input[name='sequence']").val(value + 1);

    }
    $(document).ready(function(){
        
        $('#check1 input[type=radio]').change(function () {
           var selectedCountry  = $(this).val();
            if (selectedCountry == 1) {
                $('#metrics').val("mm");
            }
            if (selectedCountry == 2) {
                $('#metrics').val("inc");
            }
            if (selectedCountry == 3) {
                $('#metrics').val("mm");
            }
            if (selectedCountry == 4) {
                $('#metrics').val("cm");
            }
    
        });
    });
</script>
@endpush