@extends('layouts.admin')
@section('title','Update Specification Details')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>Update Specification Details</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="pid" value="{!! $part->pid !!}" />
        <input type="hidden" name="sid" value="{!! $part->sid !!}" />
        <div class="input-field ">
            <select disabled="">
                @foreach($products as $item)
                <option value="{{$item->id}}" @if($part->pid == $item->id) selected @endif>{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>

        <div class="input-field ">
            <select name="cid" disabled="">
                <option value="1" @if($part->cid == 1) selected @endif>Australia</option>
                <option value="2" @if($part->cid == 2) selected @endif>New Zealand</option>
                <option value="3" @if($part->cid == 3) selected @endif>United Kingdom</option>
                <option value="4" @if($part->cid == 4) selected @endif>United States</option>
            </select>
            <label>Country</label>
        </div>
        
        <div class="input-field ">
            <select disabled="">
                @foreach($result as $productName => $item)
                <optgroup label="{{$productName}}">
                    @foreach($item as $value)
                    <option value="{{$value->id}}" @if($part->sid == $value->id) selected @endif>{{$value->model}}</option>
                    @endforeach
                </optgroup>
                @endforeach
            </select>
            <label>Model</label>
        </div>
        <div class="input-field ">
            <input id="title" type="text" name="title" value="{{$part->title}}" required="">
            <label for="title">Title</label>
        </div>
        <div class="input-field">
            <textarea id="textarea1" name="desc" class="materialize-textarea">{{$part->desc}}</textarea>
            <label for="textarea1">Description</label>
        </div>
        <div class="input-field ">
            <input id="title" type="text" name="sequence" value="{{$part->sequence}}" required="">
            <label for="title">Sequence</label>
        </div>
        <p><button class="btn waves-effect waves-light" type="submit" name="action">Update Specification Details</button></p>
    </form>
</div>
@endsection