@extends('layouts.ajax')

<div class="row" id="librarytable">
@if(count($result)!=0)
    <div class="col s12">
        <div style="overflow: auto">
            <table class="striped highlight" >
                <thead>
                    <tr>
                        <th class="border center-align">#</th>
                        @if($isVideo == 1)
                        <th class="border center-align">Thumbnail Image</th>
                        <th class="border center-align">Video</th>
                        @else
                        <th class="border center-align">Image</th>
                        @endif
                        <th class="border center-align">Sequence</th>
                        <th class="border center-align">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($result as $index => $row)
                    <tr>
                        <td class="border center-align">{!! $index + 1 !!}</td>
                        <td class="border center-align">
                            <img src="{{$row->img_name}}" class="materialboxed responsive-img" style="height: 200px;display: block;margin: auto" />
                        </td>
                        @if($isVideo == 1)
                        <td class="border center-align">
                            <video class="responsive-video" controls style="height: 200px;display: block;margin: auto">
                                <source src="{{$row->video_url}}" class="" type="video/mp4">
                            </video>
                        </td>
                        @endif
                        <td class="border center-align">{{$row->sequence}}</td>
                        <td class="border center-align">
                            <a class='dropdown-trigger btn' href='#' data-target="dropdown{!! $row->id !!}">Action</a>
                            <ul id="dropdown{!! $row->id !!}" class='dropdown-content'>
                                <li><a class=" waves-effect waves-light" href="{!! route('library-update',['library'=>$row->id]) !!}">Edit</a></li>
                                <li class="divider" tabindex="-1"></li>
                                <li><a class=" red-text lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!!$row->id !!}">Delete</a></li>
                            </ul>
                            <div id="modal_{!! $row->id !!}" class="modal bottom-sheet">
                                <div class="modal-content">
                                    <h4 class="center-align">Delete?</h4>
                                    <img src="{{$row->img_name}}" class="responsive-img" style="height: 100px;;margin: auto;display: block" />
                                </div>
                                <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                    <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                    <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('library-delete',['library'=>$row->id]) !!}">Yes, Delete It</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
<tr>
    <td class="center-align" style="font-size: 50px;">No Records Found</td>
</tr>
@endif
<div class="col hide-on-small-only m3 l2">
        <ul class="section table-of-contents">
            @foreach($result as $productName => $collection)
            <li style="padding: 1em"><a href="#{{snake_case($productName)}}">{{$productName}}</a></li>
            @endforeach
        </ul>
    </div>
</div>
</div>

<script>
    var i = 1;
    $(document).ready(function() {
        $('.modal').modal({
                dismissible: true
            });
        $('.dropdown-trigger').dropdown();
        $('.scrollspy').scrollSpy();
    });

    
</script>