@extends('layouts.admin')
@section('title','Update Library Media')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>Update Library Media</h3>
    </div>
    <div class="col s12" id="jsUploadProgressBarContainer" style="display: none">
        <div class="row">
            <div class="col s11">
                <div class="progress">
                    <div class="determinate green" id="jsUploadProgressBar" style="width: 0%"></div>
                </div>
            </div>
            <div class="col s1"><p class="flow-text" id="progressText" style="margin: 0;margin-top: -1em">4%</p></div>
        </div>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data" id="fom_file_uploading">
        @csrf
       
        <div class="input-field ">
            <select name="pid">
                @foreach($products as $item)
                <option value="{{$item->id}}" @if($item->id == $library->pid) selected @endif>{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>  
         <div class="input-field ">
            <select name="country_id">
                <option value="1" @if($library->cid == 1) selected @endif>Australia</option>
                <option value="2" @if($library->cid == 2) selected @endif>New Zealand</option>
                <option value="3" @if($library->cid == 3) selected @endif>United Kingdom</option>
                <option value="4" @if($library->cid == 4) selected @endif>United States</option>

            </select>
            <label>Country</label>
        </div>      
        <div class="input-field ">
            <input id="title" type="text" name="sequence" value="{{$library->sequence}}" required="">
            <label for="title">Sequence</label>
        </div>
        <div class="file-field input-field">
            <div class="btn">
                <span>Thumbnail / Image</span>
                <input type="file" name="img_name" accept="image/*">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text">
            </div>
        </div>
        @if($library->isVideo == 1)
        <div class="file-field input-field">
            <div class="btn">
                <span>Video</span>
                <input type="file" name="video" accept=".mp4">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text">
            </div>
        </div>
        @endif
        <p><button class="btn waves-effect waves-light" id="js_mdl_file_submit_btn" type="submit" name="action">Update Library Media</button></p>
    </form>
</div>
<div class="row">
    <div class="col s12"><hr><h4>Media</h4><br></div>
    <div class="col s6">
        <img src="{{$library->img_name}}" class="responsive-img materialboxed"/>
    </div>
    @if($library->isVideo)
    <div class="col s6">        
        <video class="responsive-video" controls style="height: 200px">
            <source src="{{$library->video_url}}"  class="" type="video/mp4">
        </video>
    </div>
    @endif
</div>
@endsection
@push('footer')
<script>
    $("#fom_file_uploading").submit(function (e) {
        e.preventDefault();
        var data = new FormData(document.getElementById('fom_file_uploading')); // create formdata
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);

                        if (percentComplete === 100) {
                            // use before and complete method for better ui
                        } else {
                            $('#jsUploadProgressBar').css('width', percentComplete + '%');
                            $('#progressText').text(percentComplete + '%');
                        }

                    }
                }, false);
                return xhr;
            },
            url: '{!! url()->current() !!}',
            processData: false, // set this false
            contentType: false, // set this false
            type: "POST",
            data: data,
            beforeSend: function (xhr) {
                $('#jsUploadProgressBarContainer').show();
                $("#js_mdl_file_submit_btn").text('Uploading...');
                $('#jsUploadProgressBar').removeClass('red');
                $('#jsUploadProgressBar').removeClass('green');
                $('#jsUploadProgressBar').addClass('green');
                $('#progressText').text('0%');
            },
            dataType: "json",
            success: function (result) {
                window.location.href = result.url;
            },
            complete: function (jqXHR, textStatus) {
                $('#jsUploadProgressBar').css('width', '100%');
                $('#jsUploadProgressBar').text('100%');
                $("#js_mdl_file_submit_btn").text('Update Library Media');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#jsUploadProgressBar').css('width', '100%');
                $('#jsUploadProgressBar').text('100%');
                $('#jsUploadProgressBar').addClass('red');
                $('#jsUploadProgressBar').removeClass('green');
                $("#js_mdl_file_submit_btn").text('Update Library Media');
                alert(jqXHR.responseText);
            }
        });

    });
</script>
@endpush