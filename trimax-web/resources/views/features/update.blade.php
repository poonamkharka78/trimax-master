@extends('layouts.admin')
@section('title','Update Feature')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>Update Feature</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="pid" value="{!! $feature->pid !!}" />
        <div class="input-field ">
            <select disabled="">
                @foreach($products as $item)
                <option value="{{$item->id}}" @if($item->id == $feature->pid) selected  @endif>{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        <div class="input-field ">
            <select name="cid">
                <option value="1" @if($feature->cid == 1) selected @endif>Australia</option>
                <option value="2" @if($feature->cid == 2) selected @endif>New Zealand</option>
                <option value="3" @if($feature->cid == 3) selected @endif>United Kingdom</option>
                <option value="4" @if($feature->cid == 4) selected @endif>United States</option>
            </select>
            <label>Country</label>
        </div>
        
        <div class="input-field ">
            <input id="title" type="text" name="title" value="{{$feature->title}}" required="">
            <label for="title">Title</label>
        </div>
        <div class="input-field">
            <textarea id="textarea1" name="desc" class="materialize-textarea" required="">{{$feature->desc}}</textarea>
            <label for="textarea1">Description</label>
        </div>

        <div class="input-field ">
            <input id="title" type="number" name="sequence" required="" value="{{$feature->sequence}}">
            <label for="title">Sequence</label>
        </div>
        <div class="row">
            <div class="file-field input-field col s6">
                <div class="btn">
                    <span>Icon</span>
                    <input type="file" name="icon" accept="image/*">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path" type="text">
                </div>
                <p><button class="btn waves-effect waves-light" type="submit" name="action">Update Feature</button></p>
            </div>
            <div class="col s6">
                <img src="{{$feature['icon']}}" class="responsive-img materialboxed"/>
            </div>
        </div>
    </form>
</div>
@endsection