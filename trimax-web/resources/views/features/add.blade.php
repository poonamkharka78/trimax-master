@extends('layouts.admin')
@section('title','New Feature')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>New Feature</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input-field ">
            <select name="pid">
                @foreach($products as $item)
                <option value="{{$item->id}}" data-max="{!! $item->max !!}">{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        <div class="input-field ">
            <select name="country_id[] " multiple="multiple" class="js-example-basic-multiple">
                <option value="1">Australia</option>
                <option value="2">New Zealand</option>
                <option value="3">United Kingdom</option>
                <option value="4">United States</option>
            </select>
            <label>Country</label>
        </div>
        <div class="input-field ">
            <input id="title" type="text" name="title" value="" required="">
            <label for="title">Title</label>
        </div>
        <div class="input-field">
            <textarea id="textarea1" name="desc" class="materialize-textarea" required=""></textarea>
            <label for="textarea1">Description</label>
        </div>
        <div class="input-field ">
            <input id="title" type="number" name="sequence" value="{!! $products[0]->max + 1 !!}" required="">
            <label for="title">Sequence</label>
        </div>
        <div class="file-field input-field">
            <div class="btn">
                <span>Icon</span>
                <input type="file" name="icon" accept="image/*" required="">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text">
            </div>
        </div>
        <p><button class="btn waves-effect waves-light" type="submit" name="action">Add Feature</button></p>
    </form>
</div>
@endsection
@push('footer')
<script>
    $("select[name='pid']").on('change', function () {
        let value = $(this).find(':selected').data('max');
        $("input[name='sequence']").val(value + 1);
    });
$(document).ready(function() {
        $(".js-example-basic-multiple").select2();

    });

</script>
@endpush