@extends('layouts.admin')
@section('title','New Part')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>New contact</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        
        <div class="input-field ">
            <select name="country_name">
                <option value="Australia">Australia</option>
                <option value="New Zealand">New Zealand</option>
                <option value="United Kingdom">United Kingdom</option>
                <option value="United States">United States</option>
            </select>
            <label>Country</label>
        </div>
        <div class="input-field ">
            <input id="title" type="text" name="country_email" value="" required="">
            <label for="title">Country email</label>
        </div>

        <p><button class="btn waves-effect waves-light" type="submit" name="action">Add Country Email</button></p>
    </form>
</div>
@endsection