@extends('layouts.admin')
@section('title','Update Country Email')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>Update Country Email</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
  
        <div class="input-field ">
            <select name="country_name">
                <option value="Australia" @if($part->country_name == 'Australia') selected @endif>Australia</option>
                <option value="New Zealand" @if($part->country_name == 'New Zealand') selected @endif>New Zealand</option>
                <option value="United Kingdom" @if($part->country_name == 'United Kingdom') selected @endif>United Kingdom</option>
                <option value="United States" @if($part->country_name == 'United States') selected @endif>United States</option>

            </select>
            <label>Country</label>
        </div>  
        <div class="input-field ">
            <input id="title" type="text" name="country_email" value="{{$part->country_email}}" required="">
            <label for="title">Country Email</label>
        </div>

        <p><button class="btn waves-effect waves-light" type="submit" name="action">Update Country Info</button></p>
    </form>
</div>
@endsection