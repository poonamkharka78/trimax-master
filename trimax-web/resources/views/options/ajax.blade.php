<div class="row" id="optiontable">
    <div class="col s12 m9 l10">
        @foreach($result as $productName => $collection)
       
        <div id="{{snake_case($productName)}}" class="section scrollspy">
            <div style="overflow: auto">
                <table class="striped highlight">
                    <thead>
                        <tr>
                            <th class="border">#</th>
                            <th class="border">Product</th>
                            <th class="border">Title</th>
                            <th class="border">Description</th>
                            <th class="border">Icon</th>
                            <th class="border">Sequence</th>
                            <th class="border">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($collection as $index => $feature)
                        <tr>
                            <td class="border">{!! $index + 1 !!}</td>
                            <td class="border">{{$feature['name']}}</td>
                            <td class="border">{{$feature['title']}}</td>
                            <td class="border">{{$feature['desc']}}</td>
                            <td class="border"><img src="{{ $feature['icon'] }}" class="responsive-img materialboxed" /> </td>
                            <td class="border">{{$feature['sequence']}}</td>
                            <td class="border">
                                <a class='dropdown-trigger btn' href='#' data-target="dropdown{!! $feature['id'] !!}">Action</a>
                                <ul id="dropdown{!! $feature['id'] !!}" class='dropdown-content'>
                                    <li><a class=" waves-effect waves-light" href="{!! route('option-update',['option'=>$feature['id']]) !!}">Edit</a></li>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" red-text lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!!$feature['id'] !!}">Delete</a></li>
                                </ul>
                                <div id="modal_{!! $feature['id'] !!}" class="modal bottom-sheet">
                                    <div class="modal-content">
                                        <h4 class="center-align">Delete {{$feature['name']}} - {{$feature['title']}}?</h4>
                                    </div>
                                    <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                        <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                        <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('option-delete',['option'=>$feature['id']]) !!}">Yes, Delete It</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endforeach
    </div>
    <div class="col hide-on-small-only m3 l2">
        <ul class="section table-of-contents">
            @foreach($result as $productName => $collection)
            <li style="padding: 1em"><a href="#{{snake_case($productName)}}">{{$productName}}</a></li>
            @endforeach
        </ul>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.modal').modal({
            dismissible: true
        });
        $('.dropdown-trigger').dropdown();
        $('.scrollspy').scrollSpy();
    });
</script>