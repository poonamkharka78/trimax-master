@extends('layouts.ajax')
<div id="check1" class="check">
    <label class="checkbox-label"><input type="radio" class="clsAnswer" id="check" name="country" style="opacity: 13;position: inherit;" value="1">Australia</label>
    <label class="checkbox-label"><input type="radio" class="clsAnswer" id="check" name="country" style="opacity: 13;position: inherit;" value="2">New Zealand</label>
    <label class="checkbox-label"><input type="radio" class="clsAnswer" id="check" name="country" style="opacity: 13;position: inherit;" value="3">United Kingdom</label>
    <label class="checkbox-label"><input type="radio" class="clsAnswer" id="check" name="country" style="opacity: 13;position: inherit;" value="4">United States</label>
</div>
<form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="country_id" value="1">
    <div class="input-field ">
        <select name="pid" id="productSelect">
            @foreach($result as $productName => $item)
            @if($loop->first)
            @php $keyName = $productName; @endphp
            @endif
            <option value="{{$item[0]->pid}}">{{$item[0]->name}}</option>
            @endforeach
        </select>
        <label>Product</label>
    </div>
    @foreach($result as $productName => $item)
    <div class="input-field modelDiv" id="mdl_ddl_{{$item[0]->pid}}" @if(!$loop->first)style="display: none;"@endif>
        <select name="part[{{$item[0]->pid}}]" class="modelSelect">
            @foreach($item as $value)
            <option value="{{$value->id}}" data-max="{!! $value->max !!}">{{$value->part}}</option>
            @endforeach
        </select>
        <label>{{$productName}} - Part</label>
    </div>
    @endforeach
    <div class="input-field ">
        <input id="title" type="text" name="title" value="" required="">
        <label for="title">Title</label>
    </div>
    <div class="input-field ">
        <input id="title1" type="text" name="desc" value="">
        <label for="title">Price</label>
    </div>
    <div class="input-field ">
        <input id="currency" type="text" name="currency" value="" readonly required="">
        <label for="title">Currency</label>
    </div>
    <div class="input-field ">
        <input id="title" type="number" name="sequence" value="{!! $result[$keyName][0]->max + 1 !!}" required="">
        <label for="title">Sequence</label>
    </div>
    <p><button class="btn waves-effect waves-light" id="sub" name="action">Add Part Price</button></p>
</form>