@extends('layouts.admin')
@section('title','Update Part Details')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>Update Part Details</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <input value="{!! $part->pid !!}" type="hidden" name="pid" />
        <input value="{!! $part->prid !!}" type="hidden" name="prid" />
        <div class="input-field ">
            <select disabled="">
                @foreach($products as $item)
                <option value="{{$item->id}}" @if($part->pid == $item->id) selected @endif>{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        <div class="input-field ">
            <select disabled="">
                @foreach($result as $productName => $item)
                <optgroup label="{{$productName}}">
                    @foreach($item as $value)
                    <option value="{{$value->id}}" @if($part->prid == $value->id) selected @endif>{{$value->part}}</option>
                    @endforeach
                </optgroup>
                @endforeach
            </select>
            <label>Part</label>
        </div>
        
        <div id ="partprice">
            <div class="input-field ">
                <input id="title" type="text" name="title" value="{{$part->title}}" required="">
                <label for="title">Title</label>
            </div>
            <div class="input-field ">
                <input id="title" type="text" name="desc" value="{{$part->desc}}">
                <label for="title">Price</label>
            </div>
            <div class="input-field ">
                <input id="title" type="number" name="sequence" value="{{$part->sequence}}" required="">
                <label for="title">Sequence</label>
            </div>
        </div>
        <p><button class="btn waves-effect waves-light" type="submit" name="action">Update Part Price</button></p>
    </form>
</div>
@endsection
@section("js")
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

<!-- <script>
    $(function() {
        $("#country").change(function() {
            var country_id = $('select[name=country_id]').val()
            if (country_id) {
                data = {
                    _token: "{{csrf_token()}}",
                    'country_id': country_id
                };
                url = "{{ url('getupdatedata') }}";
                $.post(url, data, function(response) {

                    $("#partprice").html(response.data)
                })
            }

        });
    });
</script> -->