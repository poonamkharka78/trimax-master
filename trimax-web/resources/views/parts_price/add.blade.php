@extends('layouts.admin')
@section('title','New Part Price')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>New Part Price</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" id="partdetails" method="post" enctype="multipart/form-data">
        @csrf
        <!-- <div id="check1" class="check">
            <label class="checkbox-label"><input type="radio"  id="check" name="country_id" style="opacity: 13;position: inherit;" value="1">Australia</label>
            <label class="checkbox-label"><input type="radio"  id="check" name="country_id" style="opacity: 13;position: inherit;" value="2">New Zealand</label>
            <label class="checkbox-label"><input type="radio"  id="check" name="country_id" style="opacity: 13;position: inherit;" value="3">United Kingdom</label>
            <label class="checkbox-label"><input type="radio"  id="check" name="country_id" style="opacity: 13;position: inherit;" value="4">United States</label>
        </div> -->
        <div class="input-field ">
            <select name="pid" id="productSelect">
                @foreach($result as $productName => $item)
                @if($loop->first)
                @php $keyName = $productName; @endphp
                @endif
                <option value="{{$item[0]->pid}}">{{$item[0]->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        @foreach($result as $productName => $item)
        <div class="input-field modelDiv" id="mdl_ddl_{{$item[0]->pid}}" @if(!$loop->first)style="display: none;"@endif>
            <select name="part[{{$item[0]->pid}}]" class="modelSelect">
                @foreach($item as $value)
                <option value="{{$value->id}}" data-max="{!! $value->max !!}">{{$value->part}}</option>
                @endforeach
            </select>
            <label>{{$productName}} - Part</label>
        </div>
        @endforeach
        <div class="input-field ">
            <select name="country_id[] " multiple="multiple" class="js-example-basic-multiple">
                <option value="1">Australia</option>
                <option value="2">New Zealand</option>
                <option value="3">United Kingdom</option>
                <option value="4">United States</option>
            </select>
            <label>Country</label>
        </div>
        <div class="input-field ">
            <input id="title" type="text" name="title" value="" required="">
            <label for="title">Title</label>
        </div>
        <div class="input-field ">
            <input id="title1" type="text" name="desc" value="">
            <label for="title">Price</label>
        </div>
        <!-- <div class="input-field ">
            <input id="currency" type="text" name="currency" value="" required="">
            <label for="title">Currency</label>
        </div> -->
        <div class="input-field ">
            <input id="title" type="number" name="sequence" value="{!! $result[$keyName][0]->max + 1 !!}" required="">
            <label for="title">Sequence</label>
        </div>
        <p><button class="btn waves-effect waves-light" id="sub" name="action">Add Part Price</button></p>
    </form>
</div>
</div>
@endsection
@push('footer')
@section("js")
<script src="{{asset('js/admin.js')}}"></script>
<script>
    $("input[name='country']").click(function() {
        var country_id = $("input[name='country']:checked").val();
        if (country_id) {
            data = {
                _token: "{{csrf_token()}}",
                'country_id': country_id
            };
            url = "{{ url('addcity') }}";
            $.post(url, data, function(response) {

                $("#partdetails").html(response.data)
            })
        }
    });

    $("#productSelect").on('change', function() {
        $(".modelDiv").hide();
        let pid = $(this).val();
        let selector = "#mdl_ddl_" + pid;
        $(selector).show();
        setSequence(selector + " select");
    });
    $(".modelSelect").on('change', function() {
        setSequence($(this));
    });

    function setSequence(selector) {
        let value = $(selector).find(':selected').data('max');
        $("input[name='sequence']").val(value + 1);

    }
</script>

@endpush