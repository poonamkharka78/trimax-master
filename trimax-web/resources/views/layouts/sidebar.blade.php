@php
$cRouteName = Route::currentRouteName();
@endphp
<!--<li class="@if($cRouteName == 'aboutus') active red lighten-1 @endif"><a class="waves-effect @if($cRouteName == 'aboutus') white-text @endif" href="{!! route('aboutus') !!}">About US</a></li>-->
<li class="@if($cRouteName == 'products') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'products') white-text @endif" href="{!! route('products') !!}">Products</a>
</li>
<li class="@if($cRouteName == 'partsorders') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'partsorders') white-text @endif" href="{!! route('partsorders') !!}">Product Sequence</a>
</li>
<li class="@if($cRouteName == 'generals') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'generals') white-text @endif" href="{!! route('generals') !!}">Generals</a>
</li>
<li class="@if($cRouteName == 'general-images') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'general-images') white-text @endif" href="{!! route('general-images') !!}">General Images</a>
</li>
<li class="@if($cRouteName == 'features') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'features') white-text @endif" href="{!! route('features') !!}">Features</a>
</li>
<li class="@if($cRouteName == 'options') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'options') white-text @endif" href="{!! route('options') !!}">Options</a>
</li>
<li class="@if($cRouteName == 'specifications') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'specifications') white-text @endif" href="{!! route('specifications') !!}">Specifications</a>
</li>
<li class="@if($cRouteName == 'specifications-details') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'specifications-details') white-text @endif" href="{!! route('specifications-details') !!}">Specification Details</a>
</li>
<li class="@if($cRouteName == 'parts') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'parts') white-text @endif" href="{!! route('parts') !!}">Parts</a>
</li>
<li class="@if($cRouteName == 'partsdetails') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'partsdetails') white-text @endif" href="{!! route('partsdetails') !!}">Parts Details</a>
</li>
<li class="@if($cRouteName == 'partsprice') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'partsprice') white-text @endif" href="{!! route('partsprice') !!}">Price and Availabilities</a>
</li>
<li class="@if($cRouteName == 'library') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'library') white-text @endif" href="{!! route('library') !!}">Library</a>
</li>
<li class="@if($cRouteName == 'tips-demo') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'tips-demo') white-text @endif" href="{!! route('tips-demo') !!}">Demo Tips</a>
</li>
<li class="@if($cRouteName == 'tips-sales') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'tips-sales') white-text @endif" href="{!! route('tips-sales') !!}">Sales Tips</a>
</li>
<li class="@if($cRouteName == 'service') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'service') white-text @endif" href="{!! route('service') !!}">Service</a>
</li>
<li class="@if($cRouteName == 'option') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'option') white-text @endif" href="{!! route('option') !!}">Manage Info</a>
</li>
<hr>
<li class="@if($cRouteName == 'contact-us') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'contact-us') white-text @endif" href="{!! route('contact-us') !!}">Contact Us</a>
</li>

<li class="@if($cRouteName == 'aboutus') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'aboutus') white-text @endif" href="{!! route('aboutus') !!}">About Us</a>
</li>

<li class="@if($cRouteName == 'apiadmin-users') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'apiadmin-users') white-text @endif" href="{!! route('apiadmin-users') !!}">Users</a>
</li>

<li class="@if($cRouteName == 'apiadmin-registrationRequests') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'apiadmin-registrationRequests') white-text @endif" href="{!! route('apiadmin-registrationRequests') !!}">Registration Requests</a>
</li>
<li class="@if($cRouteName == 'apiadmin-contactpersons') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'apiadmin-contactpersons') white-text @endif" href="{!! route('apiadmin-contactpersons') !!}">Contact Persons</a>
</li>
<li class="@if($cRouteName == 'apiadmin-welcomeText') active red lighten-1 @endif">
    <a class="waves-effect @if($cRouteName == 'apiadmin-welcomeText') white-text @endif" href="{!! route('apiadmin-welcomeText') !!}">Welcome Text</a>
</li>
<li>
    <a class="waves-effect" href="{!! url('logout') !!}">Logout</a>
</li>
