<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('title') :: Trimax App Backend</title>
    <style>
        header,
        main,
        footer {
            padding-left: 300px;
        }

        @media only screen and (max-width : 992px) {

            header,
            main,
            footer {
                padding-left: 0;
            }
        }

        .border {
            border: 1px solid lightgray;
        }

        label.checkbox-label input[type=checkbox] {
            position: relative;
            vertical-align: middle;
            bottom: 1px;
            opacity: 1;
            pointer-events: auto;
            position: relative;
            height: 82px;
            width: 20px;
        }

        label.checkbox-label {
            font-size: 1.8rem;
            padding: 21px;
        }
        label.buttoncheck{
            font-size: 1.2rem;
            padding: 11px;
        }

    </style>
    @stack('head')
</head>

<body>
    <header>
        <nav class="red lighten-1" role="navigation">
            <div class="nav-wrapper container center-align">
                <a id="logo-container" href="#" style="font-size: 2em;">Trimax App Backend</a>
                <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            </div>
        </nav>
        <ul id="slide-out" class="sidenav sidenav-fixed grey lighten-5">
            <li>
                <img src="https://trimaxmowers.co.uk/wp-content/uploads/2017/02/Trimax_logo_header.png" class="responsive-img" />
            </li>
            <li>
                <div class="divider"></div>
            </li>
            @include('layouts.sidebar')
        </ul>
    </header>
    <main>
        @if(session('success'))<div class="card-panel green white-text darken-4" style="padding: 15px;margin: 0">{!! session('success') !!}</div>@endif
        @if(session('error'))<div class="card-panel red white-text darken-4" style="padding: 15px;margin: 0">{!! session('error') !!}</div>@endif
        @if ($errors->any())
        <div class="card-panel red white-text lighten-1" style="padding: 15px;margin: 0">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @yield('content')
    </main>
    <!-- Compiled and minified JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.sidenav').sidenav();
            $('.materialboxed').materialbox();
            $('select').formSelect();
            $('.modal').modal();
            $('.fixed-action-btn').floatingActionButton();
            // hide country dropdown after selected li  
            $('.multiple-select-dropdown li').click(function(){
                $(this).hasClass('selected');
                    $(this).parents('.multiple-select-dropdown').hide();           
                });
        });
    </script>
    @stack('footer')
</body>

</html>