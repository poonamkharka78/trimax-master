@extends('layouts.admin')
@section('title','About US')
@section('content')

<ul class="tabs tabs-fixed-width tab-demo z-depth-1 teal lighten-1" style="margin-bottom:  .5em">
    <li class="tab"><a class="active waves-effect white-text" href="#s1">Section 1</a></li>
    <li class="tab"><a class="white-text waves-effect" href="#s2">Section 2</a></li>
    <li class="tab"><a class="white-text waves-effect" href="#s3">Section 3</a></li>
    <li class="tab"><a class="white-text waves-effect" href="#s4">Section 4</a></li>
</ul>
@foreach($result as $key => $value)

@if($value->id == 1)
<div id="s1" >
    <form  action="{!! route('aboutus-update')!!}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <input type="hidden" name="id" value="{{$value->id}}">
            <div class="input-field col s12">
                <input id="title" type="text" name="title" value="{{$value->title}}">
                <label for="title">Title</label>
            </div>
            <div class="input-field col s12">
                <textarea id="textarea1" name="description" class="materialize-textarea">{{$value->description}}</textarea>
                <label for="textarea1">Description</label>
            </div>
            <div class="file-field input-field col s6">
                <div class="btn">
                    <span>Image</span>
                    <input type="file" name="image" accept="image/*">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path" type="text">
                </div>
                <p><button class="btn waves-effect waves-light" type="submit" name="action">Update Details</button></p>
            </div>
            <div class="col s6">
                <img src="data:image/png;base64,{{$value->image}}" class="responsive-img"/>
            </div>
        </div>
    </form>
</div>

@elseif ($value->id == 2)
<div id="s2">
    <form  action="{!! route('aboutus-update')!!}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <input type="hidden" name="id" value="{{$value->id}}">
            <div class="input-field col s12">
                <input id="title" type="text" name="title" value="{{$value->title}}">
                <label for="title">Title</label>
            </div>
            <div class="input-field col s12">
                <textarea id="textarea1" name="description" class="materialize-textarea">{{$value->description}}</textarea>
                <label for="textarea1">Description</label>
            </div>
            <div class="file-field input-field col s6">
                <div class="btn">
                    <span>Image</span>
                    <input type="file" name="image" accept="image/*">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path" type="text">
                </div>
                <p><button class="btn waves-effect waves-light" type="submit" name="action">Update Details</button></p>
            </div>
            <div class="col s6">
                <img src="data:image/png;base64,{{$value->image}}" class="responsive-img"/>
            </div>
        </div>
    </form>
</div>
@elseif ($value->id == 3)
<div id="s3">
    <form  action="{!! route('aboutus-update')!!}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <input type="hidden" name="id" value="{{$value->id}}">
            <div class="input-field col s12">
                <input id="title" type="text" name="title" value="{{$value->title}}">
                <label for="title">Title</label>
            </div>
            <div class="input-field col s12">
                <textarea id="textarea1" name="description" class="materialize-textarea">{{$value->description}}</textarea>
                <label for="textarea1">Description</label>
            </div>
            <div class="file-field input-field col s6">
                <div class="btn">
                    <span>Image</span>
                    <input type="file" name="image" accept="image/*">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path" type="text">
                </div>
                <p><button class="btn waves-effect waves-light" type="submit" name="action">Update Details</button></p>
            </div>
            <div class="col s6">
                <img src="data:image/png;base64,{{$value->image}}" class="responsive-img"/>
            </div>
        </div>
    </form>
</div>
@elseif ($value->id == 4)
<div id="s4">
    <form  action="{!! route('aboutus-update')!!}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <input type="hidden" name="id" value="{{$value->id}}">
            <div class="input-field col s12">
                <input id="title" type="text" name="title" value="{{$value->title}}">
                <label for="title">Title</label>
            </div>
            <div class="input-field col s12">
                <textarea id="textarea1" name="description" class="materialize-textarea">{{$value->description}}</textarea>
                <label for="textarea1">Description</label>
            </div>
            <div class="file-field input-field col s6">
                <div class="btn">
                    <span>Image</span>
                    <input type="file" name="image" accept="image/*">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path" type="text">
                </div>
                <p><button class="btn waves-effect waves-light" type="submit" name="action">Update Details</button></p>
            </div>
            <div class="col s6">
                <img src="data:image/png;base64,{{$value->image}}" class="responsive-img"/>
            </div>
        </div>
    </form>
</div>
@endif
@endforeach
@endsection
@push('footer')
<script>
    $(document).ready(function () {
        $('.tabs').tabs();
    });
</script>
@endpush