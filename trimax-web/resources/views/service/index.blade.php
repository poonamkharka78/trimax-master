@extends('layouts.admin')
@section('title','Service')
@push('head')
<style>
    .tabs.products .tab a {
        color: #fff !important;
    }

    .tabs .tab a:hover,
    .tabs .tab a.active {
        color: #000 !important;
    }
    .tabs .tab a {
    color: #fff !important;
    margin: -7px 0px;
}
</style>
@endpush
@section('content')
<div class="fixed-action-btn">
    <a class="btn-floating btn-large red" href="{!! route('service-add',['isVideo' => $isVideo,'product_id' => $product_id]) !!}">
        <i class="large material-icons">add</i>
    </a>
</div>
<div>
<div class="row">
    <div class="col s12 teal">
        <ul class="tabs teal products1">
            @foreach($products as $row)
            <li class="tab">
                <a target="_self"  id="{{$row->id}}" class="{!! $row->id == $product_id ? 'active' : null !!}" href="{{route('service',['product_id' => $row->id])}}">{{$row->name}}</a>
            </li>
            @endforeach
        </ul>
    </div>
   
    <div class="col s12" >
        <div class="row" id="selectid">
            <ul class="tabs idvideo">
                <li class="tab col s6 border" >
                    <a target="_self"  value ="1" id ='1' class="{!! $isVideo == 1 ? 'active' : null !!}" href="{{route('service',['product_id' => $product_id,'isVideo' => 1])}}" style="color: #000 !important;">Video</a>
                </li>
                <li class="tab col s6 border">
                    <a target="_self" value ="0" id ='0' class="{!! $isVideo == 0 ? 'active' : null !!}" href="{{route('service',['product_id' => $product_id,'isVideo' => 0])}}" style="color: #000 !important;">Image</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="check1">
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="1">Australia</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="2">New Zealand</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="3">United Kingdom</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="4">United States</label>
</div>
<div class="row" id="librarytable" >
    <div class="col s12" >
        <div style="overflow: auto">
            <table class="striped highlight" >
                <thead>
                    <tr>
                        <th class="border center-align">#</th>
                        @if($isVideo == 1)
                        <th class="border center-align">Thumbnail Image</th>
                        <th class="border center-align">Video</th>
                        @else
                        <th class="border center-align">Image</th>
                        @endif
                        <th class="border center-align">Sequence</th>
                        <th class="border center-align">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($result as $index => $row)
                    <tr>
                        <td class="border center-align">{!! $index + 1 !!}</td>
                        <td class="border center-align">
                            <img src="{{$row->img_name}}?rand=<?php echo rand(); ?>" class="materialboxed responsive-img" style="height: 200px;display: block;margin: auto" />
                        </td>
                        @if($isVideo == 1)
                        <td class="border center-align">
                            <video class="responsive-video" controls style="height: 200px;display: block;margin: auto">
                                <source src="{{$row->video_url}}?rand=<?php echo rand(); ?>"  class="" type="video/mp4">
                            </video>
                        </td>
                        @endif
                        
                        <td class="border center-align">{{$row->sequence}}</td>
                        <td class="border center-align">
                            <a class='dropdown-trigger btn' href='#' data-target="dropdown{!! $row->id !!}">Action</a>
                            <ul id="dropdown{!! $row->id !!}" class='dropdown-content'>
                                <li><a class=" waves-effect waves-light" href="{!! route('service-update',['service'=>$row->id]) !!}">Edit</a></li>
                                <li class="divider" tabindex="-1"></li>
                                <li><a class=" red-text lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!!$row->id !!}">Delete</a></li>
                            </ul>
                           
                            <div id="modal_{!! $row->id !!}" class="modal bottom-sheet">
                                <div class="modal-content">
                                    <h4 class="center-align">Delete?</h4>
                                    <img src="{{$row->img_name}}" class="responsive-img" style="height: 100px;;margin: auto;display: block" />
                                </div>
                                <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                    <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                    <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('service-delete',['library'=>$row->id]) !!}">Yes, Delete It</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@push('footer')
@endpush
@section("js")
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".tabs").click(function() {
                $('.nav-tabs .active').text();

            });
            $("#check1").click(function() {
                var country_id = [];
                var isVideo = $("ul.idvideo li a.active").attr('id');
                var product_id = $("ul.products1 li a.active").attr('id');
                $.each($("input[name='country']:checked"), function() {
                    country_id.push($(this).val());
                });
                data = {
                    _token: "{{csrf_token()}}",
                    'country_id': country_id,
                    'isVideo': isVideo,
                    'product_id': product_id,
                };
                url = "{{ url('getserviceimage') }}";
                $.post(url, data, function(response) {
                    $("#librarytable").html(response.data)

                })
            });
        });
        $(document).ready(function() {
            $('.dropdown-trigger').dropdown();
            $('.tabs').tabs();
        });
    </script>
