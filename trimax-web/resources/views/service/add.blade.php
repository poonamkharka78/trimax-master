@extends('layouts.admin')
@section('title','New Service Media')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>New Service Media</h3>
    </div>
    <div class="col s12" id="jsUploadProgressBarContainer" style="display: none">
        <div class="row">
            <div class="col s11">
                <div class="progress">
                    <div class="determinate green" id="jsUploadProgressBar" style="width: 0%"></div>
                </div>
            </div>
            <div class="col s1"><p class="flow-text" id="progressText" style="margin: 0;margin-top: -1em">4%</p></div>
        </div>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data" id="fom_file_uploading">
        @csrf
        <input name="pid" type="hidden" value="{{request('product_id')}}">
        <input name="isVideo" type="hidden" value="{{request('isVideo')}}">
       
        <div class="input-field ">
            <select>
                @foreach($products as $item)
                <option value="{{$item->id}}"> {{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>      
        <div class="input-field ">
            <select name="country_id[]" multiple="multiple" class="js-example-basic-multiple">
                <option value="1">Australia</option>
                <option value="2">New Zealand</option>
                <option value="3">United Kingdom</option>
                <option value="4">United States</option>
            </select>
            <label>Country</label>
        </div>
        <div class="input-field ">
            <input id="title" type="text" name="sequence" value="{{$max+1}}" required="">
            <label for="title">Sequence</label>
        </div>
        <div class="file-field input-field">
            <div class="btn">
                <span>Thumbnail / Image</span>
                <input type="file" name="img_name" accept="image/*" required="">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text">
            </div>
        </div>
        @if(request('isVideo') == 1)
        <div class="file-field input-field">
            <div class="btn">
                <span>Video</span>
                <input type="file" name="video" accept=".mp4" required="">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text">
            </div>
        </div>
        @endif
        <p><button class="btn waves-effect waves-light" id="js_mdl_file_submit_btn" type="submit" name="action">Add Library Media</button></p>
    </form>
</div>
@endsection
@push('footer')
<script>
   
</script>
@endpush