@extends('layouts.admin')
@section('title','New Part')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>New Part</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input-field ">
            <select name="pid">
                @foreach($products as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        <div class="input-field ">
            <select name="country_id[] " multiple="multiple" class="js-example-basic-multiple">
                <option value="1">Australia</option>
                <option value="2">New Zealand</option>
                <option value="3">United Kingdom</option>
                <option value="4">United States</option>
            </select>
            <label>Country</label>
        </div>
        <div class="input-field ">
            <input id="title" type="text" name="part" value="" required="">
            <label for="title">Part</label>
        </div>
        <p><button class="btn waves-effect waves-light" type="submit" name="action">Add Part</button></p>
    </form>
</div>
@endsection
<script>
$(document).ready(function() {
        $(".js-example-basic-multiple").select2();

    });

</script>