@extends('layouts.admin')
@section('title','New Demo Tip')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>New Demo Tip</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input-field ">
            <select name="pid" class="pids">
                @foreach($products as $item)
                <option value="{{$item->id}}" class="attr-pid" data-attr="{{$item['demoTips']['video_url']}}">{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        <div class="input-field ">
            <select name="country_id[] " multiple="multiple" class="js-example-basic-multiple">
                <option value="1">Australia</option>
                <option value="2">New Zealand</option>
                <option value="3">United Kingdom</option>
                <option value="4">United States</option>
            </select>
            <label>Country</label>
        </div>
        <div class="input-field ">
            <textarea id="textarea1" name="Desc" required="" class="materialize-textarea"></textarea>
            <label for="textarea1">Tip</label>
        </div>
        <div class="file-field input-field videoShowHide" style="display: none">
            <div class="btn">
                <span>Video</span>
                <input type="file" name="video" accept=".mp4">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text">
            </div>
        </div>
        <div class="file-field input-field">
            <div class="btn">
                <span>Thumbnail / Image</span>
                <input type="file" name="img_name" accept="image/*">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text">
            </div>
        </div>
        <p><button class="btn waves-effect waves-light" type="submit" name="action">Add Demo Tip</button></p>
    </form>
</div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.pids').on('change',function (){
         var ids = $('option:selected', this).attr('data-attr')
         if (ids == "") {
            $('.videoShowHide').css("display", "block");
        } else {
                $('.videoShowHide').css("display", "none");
        }
        });
    })
    
</script>
<script>
    $("#fom_file_uploading").submit(function(e) {
        e.preventDefault();
        var data = new FormData(document.getElementById('fom_file_uploading')); // create formdata
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);

                        if (percentComplete === 100) {
                            // use before and complete method for better ui
                        } else {
                            $('#jsUploadProgressBar').css('width', percentComplete + '%');
                            $('#progressText').text(percentComplete + '%');
                        }

                    }
                }, false);
                return xhr;
            },
            url: '{!! url()->current() !!}',
            processData: false, // set this false
            contentType: false, // set this false
            type: "POST",
            data: data,
            beforeSend: function(xhr) {
                $('#jsUploadProgressBarContainer').show();
                $("#js_mdl_file_submit_btn").text('Uploading...');
                $('#jsUploadProgressBar').removeClass('red');
                $('#jsUploadProgressBar').removeClass('green');
                $('#jsUploadProgressBar').addClass('green');
                $('#progressText').text('0%');
            },
            dataType: "json",
            success: function(result) {
                window.location.href = result.url;
            },
            complete: function(jqXHR, textStatus) {
                $('#jsUploadProgressBar').css('width', '100%');
                $('#jsUploadProgressBar').text('100%');
                $("#js_mdl_file_submit_btn").text('Add General Details');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#jsUploadProgressBar').css('width', '100%');
                $('#jsUploadProgressBar').text('100%');
                $('#jsUploadProgressBar').addClass('red');
                $('#jsUploadProgressBar').removeClass('green');
                $("#js_mdl_file_submit_btn").text('Add General Details');
                alert(jqXHR.responseText);
            }
        });
    });
</script>