@extends('layouts.admin')
@section('title','Update Sales Tip')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>Update Sales Tip</h3>
    </div>
    <form class="col s12" action="{!! route('tips-sales-update',['part'=>$part['Id']]) !!}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" value="{!! $part->pid !!}" name="pid" />
        <div class="input-field ">
            <select disabled name="pid" class="pids">
               <option value="{{$part->id}}" class="attr-pid" data-attr="{{$part['video_name']}}">{{$part['product']['name']}}</option>
            </select>
            <label>Product</label>
        </div>
         <input type="hidden" name="pid" value="{{$part['pid']}}" >
        <div class="input-field ">
            <textarea id="textarea1" name="Desc" required="" class="materialize-textarea">{{$part->Desc}}</textarea>
            <label for="textarea1">Tip</label>
        </div>
        <div class="input-field ">
            <input id="title" type="text" name="sequence" value="{{$part->sequence}}" required="">
            <label for="textarea1">Sequence</label>
        </div>
        <!-- <div class="input-field ">
            <select name="country_id">
                <option value="1" @if($part->cid == 1) selected @endif>Australia</option>
                <option value="2" @if($part->cid == 2) selected @endif>New Zealand</option>
                <option value="3" @if($part->cid == 3) selected @endif>United Kingdom</option>
                <option value="4" @if($part->cid == 4) selected @endif>United States</option>

            </select>
            <label>Country</label>
        </div> -->
        
        <div class="file-field input-field videoShowHide">
            <div class="btn">
                <span>Video</span>
                <input type="file" name="video" accept=".mp4" >
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text" id="video_name" name="video_name" value="{{$part->video_name}}">
            </div>
        </div>
       
        <div class="file-field input-field">
            <div class="btn">
                <span>Thumbnail / Image</span>
                <input type="file" name="img_name" accept="image/*">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text"  id="image" name="img_name" value="{{$part->img_name}}">
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <h4>Media</h4><br>
            </div>
            @if($part['video_url']!=null)
            <div class="col s6" id="video_div">
                <button id="del_video"><i class="material-icons dp48">close</i></button>
                <video class="responsive-video" controls style="height: 200px">
                    <source src="{{$part->video_url}}" class="" type="video/mp4" name="video_url">
                </video>
            </div>
             @endif
             @if($part['thumbnail_img'] != null && $part['thumbnail_img'] != " ")
            <div class="col s6" id="img_div">
                <button id="del_image" type="button"><i class="material-icons dp48">close</i></button>
                <img src="{{$part->thumbnail_img}}" class="responsive-img materialboxed" style="height: 200px"/>
            </div>
            @endif
        </div>
        <p><button class="btn waves-effect waves-light" type="submit" name="action">Update Sales Tip</button></p>
    </form>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.pids').on('change',function (){
         var ids = $('option:selected', this).attr('data-attr')
         if (ids == "") {
            $('.videoShowHide').css("display", "none");
        } else {
                $('.videoShowHide').css("display", "block");
        }
        });
    })

    $(document).ready(function () {
        $('#del_image').click( function(){
            $("#image").val("");
            $("#img_div").remove();
           
            return false;
        });

        $('#del_video').click( function(){
            $("#video_name").val("");
            $("#video_div").remove();
           
            return false;
        });
    });
    
</script>
@endsection
