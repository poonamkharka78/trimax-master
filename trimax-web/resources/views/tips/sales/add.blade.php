@extends('layouts.admin')
@section('title','New Sales Tip')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>New Sales Tip</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input-field">
             <select name="pid" class="pids">
                 @foreach($products as $item)
                <option value="{{$item->id}}" data-max="{!! $item->max !!}" class="attr-pid" data-attr="{{$item['salesTips']['video_name']}}">{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        <div class="input-field ">
            <select name="country_id[] " multiple="multiple" class="js-example-basic-multiple">
                <option value="1">Australia</option>
                <option value="2">New Zealand</option>
                <option value="3">United Kingdom</option>
                <option value="4">United States</option>
            </select>
            <label>Country</label>
        </div>
        <div class="input-field ">
            <textarea id="textarea1" name="Desc" required="" class="materialize-textarea"></textarea>
            <label for="textarea1">Tip</label>
        </div>
        <div class="input-field ">
            <input id="title" type="number" name="sequence" value="{!! $products[0]->max !!}" required="">
            <label for="textarea1">Sequence</label>
        </div>
        <div class="file-field input-field videoShowHide" style="display: none">
            <div class="btn">
                <span>Video</span>
                <input type="file" name="video" accept=".mp4">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text">
            </div>
        </div>
        <div class="file-field input-field">
            <div class="btn">
                <span>Thumbnail / Image</span>
                <input type="file" name="img_name" accept="image/*">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text">
            </div>
        </div>
        <p><button class="btn waves-effect waves-light" type="submit" name="action">Add Sales Tip</button></p>
    </form>
</div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.pids').on('change',function (){
         var ids = $('option:selected', this).attr('data-attr')
         if (ids == "") {
            $('.videoShowHide').css("display", "block");
        } else {
                $('.videoShowHide').css("display", "none");
        }
        });
    })
    
</script>
@push('footer')
<script>
    $("select[name='pid']").on('change', function () {
        let value = $(this).find(':selected').data('max');
        $("input[name='sequence']").val(value + 1);
    });
</script>
@endpush

