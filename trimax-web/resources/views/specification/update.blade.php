@extends('layouts.admin')
@section('title','Update Specification')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>Update Specification</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input-field ">
            <select name="pid">
                @foreach($products as $item)
                <option value="{{$item->id}}" @if($item->id == $part->pid) selected  @endif>{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
      
        <!-- <div class="input-field ">
            <select name="country_id">
            
                <option value="1" @if($part->cid == 1) selected @endif>Australia</option>
                <option value="2" @if($part->cid == 2) selected @endif>New Zealand</option>
                <option value="3" @if($part->cid == 3) selected @endif>United Kingdom</option>
                <option value="4" @if($part->cid == 4) selected @endif>United States</option>

            </select>
            <label>Country</label>
        </div>   -->
        <div class="input-field ">
            <input id="title" type="text" name="model" value="{{$part->model}}" required="">
            <label for="title">Model</label>
        </div>

        <p><button class="btn waves-effect waves-light" type="submit" name="action">Update Specification</button></p>
    </form>
</div>
@endsection