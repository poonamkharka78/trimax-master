@extends('layouts.admin')
@section('title','New General Details')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>Update Info</h3>
    </div>
    <div class="col s12" id="jsUploadProgressBarContainer" style="display: none">
        <div class="row">
            <div class="col s11">
                <div class="progress">
                    <div class="determinate green" id="jsUploadProgressBar" style="width: 0%"></div>
                </div>
            </div>
            <div class="col s1">
                <p class="flow-text" id="progressText" style="margin: 0;margin-top: -1em">4%</p>
            </div>
        </div>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" id="fom_file_uploading" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input-field ">
            @foreach($products as $item)
            <select disabled name="pid" id="proid">
              <option value="{{$item->id}}">{{$item['product']['name']}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        <div class="input-field ">
            <select required name="country_id[]"  multiple="multiple" class="js-example-basic-multiple">
                <option value="1" <?php if ($item['cid']==1) echo 'selected="selected"';?>>Australia</option>
                <option value="2" <?php if ($item['cid']==2) echo 'selected="selected"';?>>New Zealand</option>
                <option value="3" <?php if ($item['cid']==3) echo 'selected="selected"';?>>United Kingdom</option>
                <option value="4" <?php if ($item['cid']==4) echo 'selected="selected"';?>>United States</option>
                <select>
            </select>
            <label>Country</label>
        </div>
        <div id="check1">
            <label class="checkbox-label buttoncheck"><input type="checkbox" id="check" name="log_demo" value="1">LOG A DEMO</label>
            <label class="checkbox-label buttoncheck"><input type="checkbox" id="check" name="register_warranty" value="2">REGISTER WARRANTY</label>
            <label class="checkbox-label buttoncheck"><input type="checkbox" id="check" name="share_product" value="3">SHARE WARRANTY</label>
            <label class="checkbox-label buttoncheck"><input type="checkbox" id="check" name="sales_demo" value="4">SALES & DEMO TIPS</label>
            <label class="checkbox-label buttoncheck"><input type="checkbox" id="check" name="place_order" value="5">PLACE ORDER</label>
            <label class="checkbox-label buttoncheck"><input type="checkbox" id="check" name="product_feedback" value="6">PRODUCT FEEDBACK</label>
        </div>
        <p><button class="btn waves-effect waves-light" id="js_mdl_file_submit_btn" type="submit" name="action">Update Details</button></p>
    </form>
</div>
@endsection
@push('footer')


@endpush