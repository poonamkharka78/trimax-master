@extends('layouts.admin')
@section('title','Product Button')
@section('content')
<div class="fixed-action-btn">
    <a class="btn-floating btn-large red" href="{!! route('options-add') !!}">
        <i class="large material-icons">add</i>
    </a>
</div>
<div id="generalcheck">
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="1">Australia</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="2">New Zealand</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="3">United Kingdom</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="4">United States</label>
</div>
<div class="row" id="general_table">
    <div class="col s12 m9 l10 ">
        @foreach($result as $productName => $collection)
        <div id="{{snake_case($productName)}}" class="section scrollspy">
            <div style="overflow: auto">
                <table class="striped highlight">
                    <thead>
                        <tr>
                            <th class="border">Product</th>
                            <th class="border">Item</th>
                            <th class="border">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($collection as $index => $feature)
                        @if($feature['log_demo']==1)
                        <tr>
                            <td class="border">{{$feature['name']}}</td>
                            <td class="border" id="myInput">LOG DEMO</td>

                            <td class="border">
                                <a class='dropdown-trigger btn' href='#' data-target="dropdown{!! $feature['id'] !!}">Action</a>
                                <ul id="dropdown{!! $feature['id'] !!}" class='dropdown-content'>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" waves-effect waves-light" href="{!! route('options-update',['general'=>$feature['id']]) !!}">Edit</a></li>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" red-text lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!!$feature['id'] !!}">Delete</a></li>
                                </ul>
                                <div id="modal_{!! $feature['id'] !!}" class="modal bottom-sheet">
                                    <div class="modal-content">
                                        <h4 class="center-align">Delete {{$feature['name']}}?</h4>
                                    </div>
                                    <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                        <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                        <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('button-delete',['general'=>$feature['id'],'text' => 'log_demo'])!!}">Yes, Delete It</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endif
                        @if($feature['register_warranty']==1)
                        <tr>
                            <td class="border">{{$feature['name']}}</td>
                            <td class="border" id="myInput1">REGISTER WARRANTY</td>
                            <td class="border">
                                <a class='dropdown-trigger btn' href='#' data-target="dropdown{!! $feature['id'] !!}">Action</a>
                                <ul id="dropdown{!! $feature['id'] !!}" class='dropdown-content'>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" waves-effect waves-light" href="{!! route('options-update',['general'=>$feature['id']]) !!}">Edit</a></li>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" red-text lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!!$feature['id'] !!}">Delete</a></li>
                                </ul>
                                <div id="modal_{!! $feature['id'] !!}" class="modal bottom-sheet">
                                    <div class="modal-content">
                                        <h4 class="center-align">Delete {{$feature['name']}}?</h4>
                                    </div>
                                    <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                        <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                        <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('button-delete',['general'=>$feature['id'],'text' => 'register_warranty'])!!}">Yes, Delete It</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endif
                        @if($feature['share_product']==1)
                        <tr>
                            <td class="border">{{$feature['name']}}</td>
                            <td class="border" id="myInput2">SHARE PRODUCT</td>
                            <td class="border">
                                <a class='dropdown-trigger btn' href='#' data-target="dropdown{!! $feature['id'] !!}">Action</a>
                                <ul id="dropdown{!! $feature['id'] !!}" class='dropdown-content'>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" waves-effect waves-light" href="{!! route('options-update',['general'=>$feature['id']]) !!}">Edit</a></li>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" red-text lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!!$feature['id'] !!}">Delete</a></li>
                                </ul>
                                <div id="modal_{!! $feature['id'] !!}" class="modal bottom-sheet">
                                    <div class="modal-content">
                                        <h4 class="center-align">Delete {{$feature['name']}}?</h4>
                                    </div>
                                    <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                        <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                        <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('button-delete',['general'=>$feature['id'],'text' => 'share_product'])!!}">Yes, Delete It</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endif
                        @if($feature['sales_demo']==1)
                        <tr>
                            <td class="border">{{$feature['name']}}</td>
                            <td class="border" id="myInput3">SALES DEMO</td>
                            <td class="border">
                                <a class='dropdown-trigger btn' href='#' data-target="dropdown{!! $feature['id'] !!}">Action</a>
                                <ul id="dropdown{!! $feature['id'] !!}" class='dropdown-content'>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" waves-effect waves-light" href="{!! route('options-update',['general'=>$feature['id']]) !!}">Edit</a></li>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" red-text lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!!$feature['id'] !!}">Delete</a></li>
                                </ul>
                                <div id="modal_{!! $feature['id'] !!}" class="modal bottom-sheet">
                                    <div class="modal-content">
                                        <h4 class="center-align">Delete {{$feature['name']}}?</h4>
                                    </div>
                                    <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                        <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                        <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('button-delete',['general'=>$feature['id'],'text' => 'sales_demo'])!!}">Yes, Delete It</a>
                                    </div>
                                </div>
                            </td>

                        </tr>
                        @endif
                        @if($feature['place_order']==1)
                        <tr>
                            <td class="border">{{$feature['name']}}</td>
                            <td class="border" id="myInput4">PLACE ORDER</td>
                            <td class="border">
                                <a class='dropdown-trigger btn' href='#' data-target="dropdown{!! $feature['id'] !!}">Action</a>
                                <ul id="dropdown{!! $feature['id'] !!}" class='dropdown-content'>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" waves-effect waves-light" href="{!! route('options-update',['general'=>$feature['id']]) !!}">Edit</a></li>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" red-text lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!!$feature['id'] !!}">Delete</a></li>
                                </ul>
                                <div id="modal_{!! $feature['id'] !!}" class="modal bottom-sheet">
                                    <div class="modal-content">
                                        <h4 class="center-align">Delete {{$feature['name']}}?</h4>
                                    </div>
                                    <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                        <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                        <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('button-delete',['general'=>$feature['id'],'text' => 'place_order'])!!}">Yes, Delete It</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endif
                        @if($feature['product_feedback']==1)
                        <tr>
                            <td class="border">{{$feature['name']}}</td>
                            <td class="border">PRODUCT FEEDBACK</td>
                            <td class="border">
                                <a class='dropdown-trigger btn' href='#' data-target="dropdown{!! $feature['id'] !!}">Action</a>
                                <ul id="dropdown{!! $feature['id'] !!}" class='dropdown-content'>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" waves-effect waves-light" href="{!! route('options-update',['general'=>$feature['id']]) !!}">Edit</a></li>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" red-text lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!!$feature['id'] !!}">Delete</a></li>
                                </ul>
                                <div id="modal_{!! $feature['id'] !!}" class="modal bottom-sheet">
                                    <div class="modal-content">
                                        <h4 class="center-align">Delete {{$feature['name']}}?</h4>
                                    </div>
                                    <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                        <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                        <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('button-delete',['general'=>$feature['id'],'text' => 'product_feedback'])!!}">Yes, Delete It</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endforeach
    </div>
    <div class="col hide-on-small-only m3 l2">
        <ul class="section table-of-contents">
            @foreach($result as $productName => $collection)
            <li style="padding: 1em"><a href="#{{snake_case($productName)}}">{{$productName}}</a></li>
            @endforeach
        </ul>
    </div>
</div>
@endsection
@push('footer')
<script>
    var i = 1;
    $(document).ready(function() {
        $("#generalcheck").click(function() {
            var country_id = [];
            $.each($("input[name='country']:checked"), function() {
                country_id.push($(this).val());
            });
            data = {
                _token: "{{csrf_token()}}",
                'country_id': country_id
            };

            url = "{{ url('getproductbutton') }}";
            $.post(url, data, function(response) {
                $("#general_table").html(response.data)

            })
        });
        $('.dropdown-trigger').dropdown();
        $('.scrollspy').scrollSpy();
    });

    
</script>
@endpush