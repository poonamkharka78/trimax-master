@extends('layouts.admin')
@section('title','Update Product Sequence')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>Update Product Sequence</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <input name="pid" type="hidden" value="{!! $part->pid !!}"  />
        <div class="input-field ">
            <select disabled="">
                @foreach($products as $item)
                <option value="{{$item->id}}" @if($item->id == $part->pid) selected  @endif>{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        <div class="input-field ">
            <input id="title" type="text" name="index" readonly value={{$item['category'] == 1 ? 'Rotary Mowers' : 'Flail Mowers'}} required="">
            <label for="title">Order</label>
        </div> 
         <div class="input-field ">
            <select name="country_id">
                <option value="1" @if($part->cid == 1) selected @endif>Australia</option>
                <option value="2" @if($part->cid == 2) selected @endif>New Zealand</option>
                <option value="3" @if($part->cid == 3) selected @endif>United Kingdom</option>
                <option value="4" @if($part->cid == 4) selected @endif>United States</option>

            </select>
            <label>Country</label>
        </div> 
        <div class="input-field ">
            <input id="title" type="text" name="index" value="{{$part->index}}" required="">
            <label for="title">Order</label>
        </div>

        <p><button class="btn waves-effect waves-light" type="submit" name="action">Update Product Sequence</button></p>
    </form>
</div>
@endsection