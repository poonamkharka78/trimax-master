@extends('layouts.admin')
@section('title','New Product Sequence')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>New Product Sequence</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input-field ">
            <select name="pid">
                @foreach($products as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        <div class="input-field ">
            <input id="title" type="text" name="index" value="{!! $max + 1 !!}" required="">
            <label for="title">Order</label>
        </div>
        <div class="input-field ">
            <select name="country_id[] " multiple="multiple" class="js-example-basic-multiple">
                <option value="1">Australia</option>
                <option value="2">New Zealand</option>
                <option value="3">United Kingdom</option>
                <option value="4">United States</option>
            </select>
            <label>Country</label>
        </div>
        <p><button class="btn waves-effect waves-light" type="submit" name="action">Add Product Sequence</button></p>
    </form>
</div>
@endsection