@extends('layouts.ajax')
@if(count($result)!=0)
<div id="producttable">
        <div class="col s12 m9 l10">
            <table class="striped highlight">
                <thead>
                    <tr>
                        <!--<th class="border">#</th>-->
                        <th class="border center-align">Sequence</th>
                        <th class="border center-align">Product</th>
                        <th class="border center-align">Category</th>
                        <th class="border center-align">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($result as $index => $feature)
                    <tr>
                        <!--<td class="border">{!! $index + 1 !!}</td>-->
                        <td class="border center-align">{!! $index + 1 !!}</td>
                        <td class="border  center-align">{{$feature['name']}}</td>
                        <td class="border  center-align">{{$feature['category'] == 1 ? 'Rotary Mowers' : 'Flail Mowers'}}</td>
                        <td class="border  center-align">
                            <a class='dropdown-trigger btn' href='#' data-target="dropdown{!! $feature['id'] !!}">Action</a>
                            <ul id="dropdown{!! $feature['id'] !!}" class='dropdown-content'>
                                <li><a class=" waves-effect waves-light" href="{!! route('partsorders-update',['part'=>$feature['id']]) !!}">Edit</a></li>
                                <li class="divider" tabindex="-1"></li>
                                <li><a class=" red-text lighten-1 waves-effect waves-light modal-trigger" value="11" href="#modal_{!!$feature['id'] !!}">Delete</a></li>
                            </ul>
                            <div id="modal_{!! $feature['id'] !!}" class="modal bottom-sheet">
                                <div class="modal-content">
                                    <h4 class="center-align">Delete {{$feature['name']}} - {{$feature['index']}}?</h4>
                                </div>
                                <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                    <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                    <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('partsorders-delete',['part'=>$feature['id']]) !!}">Yes, Delete It</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
<tr>
    <td class="center-align" style="font-size: 2.92rem;">No Records Found</td>
</tr>
@endif
<script>
    $(document).ready(function() {
        $('.modal').modal({
            dismissible: true
        });
        $('.dropdown-trigger').dropdown();
        $('.scrollspy').scrollSpy();
    });
</script>