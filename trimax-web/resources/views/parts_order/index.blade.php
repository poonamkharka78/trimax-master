@extends('layouts.admin')
@section('title','Parts Order')
@section('content')
<div class="fixed-action-btn">
    <a class="btn-floating btn-large red" href="{!! route('partsorders-add') !!}">
        <i class="large material-icons">add</i>
    </a>
</div>
<div id="check1">
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="1">Australia</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="2">New Zealand</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="3">United Kingdom</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="4">United States</label>
</div>
<div class="row">

    <div class="col s12 m9 l10">
        <h3>Product Sequence</h3>
    </div>
    <div id="producttable">
        <div class="col s12 m9 l10">
            <table class="striped highlight">
                <thead>
                    <tr>
                        <!--<th class="border">#</th>-->
                        <th class="border center-align">Sequence</th>
                        <th class="border center-align">Product</th>
                        <th class="border center-align">Category</th>
                        <th class="border center-align">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($result as $index => $feature)
                    <tr>
                        <!--<td class="border">{!! $index + 1 !!}</td>-->
                        <td class="border center-align">{!! $index + 1 !!}</td>
                        <td class="border  center-align">{{$feature['name']}}</td>
                        <td class="border  center-align">{{$feature['category'] == 1 ? 'Rotary Mowers' : 'Flail Mowers'}}</td>
                        <td class="border  center-align">
                            <a class='dropdown-trigger btn' href='#' data-target="dropdown{!! $feature['id'] !!}">Action</a>
                            <ul id="dropdown{!! $feature['id'] !!}" class='dropdown-content'>
                                <li><a class=" waves-effect waves-light" href="{!! route('partsorders-update',['part'=>$feature['id']]) !!}">Edit</a></li>
                                <li class="divider" tabindex="-1"></li>
                                <li><a class=" red-text lighten-1 waves-effect waves-light modal-trigger" value="11" href="#modal_{!!$feature['id'] !!}">Delete</a></li>
                            </ul>
                            <div id="modal_{!! $feature['id'] !!}" class="modal bottom-sheet">
                                <div class="modal-content">
                                    <h4 class="center-align">Delete {{$feature['name']}} - {{$feature['index']}}?</h4>
                                </div>
                                <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                    <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                    <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('partsorders-delete',['part'=>$feature['id']]) !!}">Yes, Delete It</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@push('footer')
<script>
    $(document).ready(function() {
        $('.dropdown-trigger').dropdown();
        $('.scrollspy').scrollSpy();

        $("#check1").click(function() {
            var country_id = [];
            $.each($("input[name='country']:checked"), function() {
                country_id.push($(this).val());
            });
            data = {
                _token: "{{csrf_token()}}",
                'country_id': country_id
            };
            url = "{{ url('productsequence') }}";
            $.post(url, data, function(response) {
                $("#producttable").html(response.data)

            })
        });
    });
</script>
@endpush