@extends('layouts.ajax')
<div class="row" id="table">
    @if(count($result)!=0)
    <div class="col s12 m9 l10">
        @foreach($result as $name => $product1)
        <div class="col s12">
            <h1>{{$name}}</h1>
        </div>
        <div class="row scrollspy" id="{{snake_case($name)}}">
            @foreach($product1 as $item)
            <div class="col m4">
                <div class="card hoverable">
                    <div class="card-image">
                        <img src="{{$item->img_name}}" class="materialboxed" style="height: 200px">
                    </div>
                    <div class="card-stacked">
                        <div class="card-content">
                            <p class="flow-text" style="padding-bottom: .5em">{{$item->name}}</p>
                        </div>
                        <div class="card-action" style="padding-top: 0;border-top:0">
                            <a class="btn red lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!! $item->id !!}">Delete</a>
                            <div id="modal_{!! $item->id !!}" class="modal">
                                <div class="modal-content">
                                    <h4 class="center-align">Delete Image?</h4>
                                    <img src="{{ $item->img_name }}" class="materialboxed" style="max-height: 200px;margin: auto;display: block">
                                </div>
                                <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                    <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                    <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('general-image-delete',['id'=>$item->id,'img'=>$item->img]) !!}">Yes, Delete It</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
    </div>
    @else
    <tr>
        <td class="center-align" style="font-size: 50px;">No Records Found</td>
    </tr>
    @endif
    <div class="col hide-on-small-only m3 l2">
        <ul class="section table-of-contents">
            @foreach($result as $name => $product1)
            <li style="padding: 1em"><a href="#{{snake_case($name)}}">{{$name}}</a></li>
            @endforeach
        </ul>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.scrollspy').scrollSpy();
         $('.modal').modal();


    });
</script>