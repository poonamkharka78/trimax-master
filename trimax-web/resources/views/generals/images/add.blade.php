@extends('layouts.admin')
@section('title','New General Image')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>New General Image</h3>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input-field ">
            <select name="pid">
                @foreach($products as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        <div class="input-field ">
            <select name="country_id[] " multiple="multiple" class="js-example-basic-multiple">
                <option value="1">Australia</option>
                <option value="2">New Zealand</option>
                <option value="3">United Kingdom</option>
                <option value="4">United States</option>
            </select>
            <label>Country</label>
        </div>
        
        <div class="file-field input-field">
            <div class="btn">
                <span>Image</span>
                <input type="file" name="img" accept="image/*" required="">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text">
            </div>
            <p><button class="btn waves-effect waves-light" type="submit" name="action">Add General Image</button></p>
        </div>
    </form>
</div>
@endsection
<script>
$(document).ready(function() {
        $(".js-example-basic-multiple").select2();

    });
</script>