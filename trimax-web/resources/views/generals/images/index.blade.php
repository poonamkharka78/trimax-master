@extends('layouts.admin')
@section('title','General Images')
@section('content')
<div class="fixed-action-btn">
    <a class="btn-floating btn-large red" href="{!! route('general-image-add') !!}">
        <i class="large material-icons">add</i>
    </a>
</div>
<div id="generalcheckimage">
        <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="1">Australia</label>
        <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="2">New Zealand</label>
        <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="3">United Kingdom</label>
        <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="4">United States</label>
</div>
<div class="row" id="table">
    <div class="col s12 m9 l10">
        @foreach($result as $name => $product1)
        <div class="col s12">
            <h1>{{$name}}</h1>
        </div>
        <div class="row scrollspy" id="{{snake_case($name)}}">
            @foreach($product1 as $item)
            <div class="col m4">
                <div class="card hoverable">
                    <div class="card-image">
                        <img src="{{$item->img_name}}?rand=<?php echo rand(); ?>"  class="materialboxed" style="height: 200px">
                    </div>
                    <div class="card-stacked">
                        <div class="card-content">
                            <p class="flow-text" style="padding-bottom: .5em">{{$item->name}}</p>
                        </div>
                        <div class="card-action" style="padding-top: 0;border-top:0">
                            <a class="btn red lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!! $item->id !!}">Delete</a>
                            <div id="modal_{!! $item->id !!}" class="modal">
                                <div class="modal-content">
                                    <h4 class="center-align">Delete Image?</h4>
                                    <img src="{{ $item->img_name }}" class="materialboxed" style="max-height: 200px;margin: auto;display: block">
                                </div>
                                <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                    <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                    <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('general-image-delete',['id'=>$item->id,'img'=>$item->img]) !!}">Yes, Delete It</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
    </div>
    <div class="col hide-on-small-only m3 l2">
        <ul class="section table-of-contents">
            @foreach($result as $name => $product1)
            <li style="padding: 1em"><a href="#{{snake_case($name)}}">{{$name}}</a></li>
            @endforeach
        </ul>
    </div>
</div>
@endsection
@push('footer')

<script>
    $(document).ready(function() {
        $('.scrollspy').scrollSpy();

        $("#generalcheckimage").click(function() {
            var country_id = [];
            $.each($("input[name='country']:checked"), function() {
                country_id.push($(this).val());
            });
            data = {
                _token: "{{csrf_token()}}",
                'country_id': country_id
            };
            url = "{{ url('generalimg') }}";
            $.post(url, data, function(response) {
                $("#table").html(response.data)

            })
        });

         $('.modal').modal();
    });
</script>
@endpush