@extends('layouts.admin')
@section('title','Generals')
@section('content')
<div class="fixed-action-btn">
    <a class="btn-floating btn-large red" href="{!! route('general-add') !!}">
        <i class="large material-icons">add</i>
    </a>
</div>
<div id="generalcheck">
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="1">Australia</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="2">New Zealand</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="3">United Kingdom</label>
    <label class="checkbox-label"><input type="checkbox" id="check" name="country" value="4">United States</label>
</div>
<div class="row" id="general_table">

    <div class="col s12 m9 l10 ">
        @foreach($result as $productName => $collection)
        <div id="{{snake_case($productName)}}" class="section scrollspy">
            <div style="overflow: auto">
                <table class="striped highlight">
                    <thead>
                        <tr>
                            <!--<th class="border">#</th>-->
                            <th class="border">Product</th>
                            <th class="border">Thumbnail</th>
                            <!--<th class="border">Image Title</th>-->
                            <th class="border">Description</th>
                            <!--<th class="border">Description Title</th>-->
                            <th class="border">Video</th>
                            <th class="border">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($collection as $index => $feature)
                        <tr>
                            <!--<td class="border">{!! $index + 1 !!}</td>-->
                            <td class="border">{{$feature['name']}}</td>
                            <td class="border" style="width: 25%">
                                <img src="{{$feature['images']}}?rand=<?php echo rand(); ?>" class="materialboxed responsive-img" />
                            </td>
                            <!--<td class="border">{{$feature['image_title']}}</td>-->
                            <td class="border" style="white-space:pre-line">{{$feature['desc']}}</td>
                            <!--<td class="border">{{$feature['desc_title']}}</td>-->
                            <td class="border" style="width: 25%">
                                <video class="responsive-video" controls>
                                    <source src="{{$feature['video_url']}}" type="video/mp4">
                                </video>
                            </td>
                            <td class="border">
                                <a class='dropdown-trigger btn' href='#' data-target="dropdown{!! $feature['id'] !!}">Action</a>
                                <ul id="dropdown{!! $feature['id'] !!}" class='dropdown-content'>
                                    <li><a class=" waves-effect waves-light" href="{!! route('general-update',['general'=>$feature['id']]) !!}">Edit</a></li>
                                    <li class="divider" tabindex="-1"></li>
                                    <li><a class=" red-text lighten-1 waves-effect waves-light modal-trigger" href="#modal_{!!$feature['id'] !!}">Delete</a></li>
                                </ul>
                                <div id="modal_{!! $feature['id'] !!}" class="modal bottom-sheet">
                                    <div class="modal-content">
                                        <h4 class="center-align">Delete {{$feature['name']}} - {{$feature['image_title']}}?</h4>
                                    </div>
                                    <div class="modal-footer" style="text-align:center;padding-bottom: 5em;">
                                        <a class="modal-close btn waves-effect waves-green">No, Don't Delete</a>
                                        <a class="btn red lighten-1 waves-effect waves-light" href="{!! route('general-delete',['general'=>$feature['id']]) !!}">Yes, Delete It</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endforeach
    </div>
    <div class="col hide-on-small-only m3 l2">
        <ul class="section table-of-contents">
            @foreach($result as $productName => $collection)
            <li style="padding: 1em"><a href="#{{snake_case($productName)}}">{{$productName}}</a></li>
            @endforeach
        </ul>
    </div>
</div>
@endsection
@push('footer')
<script>
    $(document).ready(function() {
        $("#generalcheck").click(function() {
            var country_id = [];
            $.each($("input[name='country']:checked"), function() {
                country_id.push($(this).val());
            });
            data = {
                _token: "{{csrf_token()}}",
                'country_id': country_id
            };
            url = "{{ url('getgenerals') }}";
            $.post(url, data, function(response) {
                $("#general_table").html(response.data)

            })
        });
    });

    $(document).ready(function() {
        $('.dropdown-trigger').dropdown();
        $('.scrollspy').scrollSpy();
    });
</script>
@endpush