@extends('layouts.admin')
@section('title','New General Details')
@section('content')
<div class="row">
    <div class="col s12">
        <h3>New General Details</h3>
    </div>
    <div class="col s12" id="jsUploadProgressBarContainer" style="display: none">
        <div class="row">
            <div class="col s11">
                <div class="progress">
                    <div class="determinate green" id="jsUploadProgressBar" style="width: 0%"></div>
                </div>
            </div>
            <div class="col s1">
                <p class="flow-text" id="progressText" style="margin: 0;margin-top: -1em">4%</p>
            </div>
        </div>
    </div>
    <form class="col s12" action="{!! url()->current() !!}" id="fom_file_uploading" method="post" enctype="multipart/form-data">
        @csrf
        
        @foreach($products as $item)
        <input type="hidden" name="product_name_collection[{{$item->id}}]" value="{{$item->name}}" />
        @endforeach
        <div class="input-field ">
            <select name="pid">
                @foreach($products as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
            <label>Product</label>
        </div>
        <!-- <div class="input-field ">
            <select name="country_id">
                <option value="1" @if($item->country_id == 1) selected @endif>Australia</option>
                <option value="2" @if($item->country_id == 2) selected @endif>New Zealand</option>
                <option value="3" @if($item->country_id == 3) selected @endif>United Kingdom</option>
                <option value="4" @if($item->country_id == 4) selected @endif>United States</option>
            </select>
            <label>Country</label>
        </div> -->
        <div class="input-field ">
            <select name="country_id[] " multiple="multiple" class="js-example-basic-multiple">
                <option value="1">Australia</option>
                <option value="2">New Zealand</option>
                <option value="3">United Kingdom</option>
                <option value="4">United States</option>
            </select>
            <label>Country</label>
        </div>
        <div class="file-field input-field">
            <div class="btn">
                <span>Video</span>
                <input type="file" name="video" accept=".mp4" required="">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text">
            </div>
        </div>
        <div class="file-field input-field">
            <div class="btn">
                <span>Thumbnail Image</span>
                <input type="file" name="thumb" accept="image/*" required="">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path" type="text">
            </div>
        </div>
        <div class="input-field">
            <textarea id="textarea1" name="desc" class="materialize-textarea" required=""></textarea>
            <label for="textarea1">Description</label>
        </div>
        <p><button class="btn waves-effect waves-light" id="js_mdl_file_submit_btn" type="submit" name="action">Add General Details</button></p>
    </form>
</div>
@endsection
@push('footer')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script>
    $("#fom_file_uploading").submit(function(e) {
        e.preventDefault();
        var data = new FormData(document.getElementById('fom_file_uploading')); // create formdata
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);

                        if (percentComplete === 100) {
                            // use before and complete method for better ui
                        } else {
                            $('#jsUploadProgressBar').css('width', percentComplete + '%');
                            $('#progressText').text(percentComplete + '%');
                        }

                    }
                }, false);
                return xhr;
            },
            url: '{!! url()->current() !!}',
            processData: false, // set this false
            contentType: false, // set this false
            type: "POST",
            data: data,
            beforeSend: function(xhr) {
                $('#jsUploadProgressBarContainer').show();
                $("#js_mdl_file_submit_btn").text('Uploading...');
                $('#jsUploadProgressBar').removeClass('red');
                $('#jsUploadProgressBar').removeClass('green');
                $('#jsUploadProgressBar').addClass('green');
                $('#progressText').text('0%');
            },
            dataType: "json",
            success: function(result) {
                window.location.href = result.url;
            },
            complete: function(jqXHR, textStatus) {
                $('#jsUploadProgressBar').css('width', '100%');
                $('#jsUploadProgressBar').text('100%');
                $("#js_mdl_file_submit_btn").text('Add General Details');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#jsUploadProgressBar').css('width', '100%');
                $('#jsUploadProgressBar').text('100%');
                $('#jsUploadProgressBar').addClass('red');
                $('#jsUploadProgressBar').removeClass('green');
                $("#js_mdl_file_submit_btn").text('Add General Details');
                alert(jqXHR.responseText);
            }
        });
    });
    

</script>
@endpush